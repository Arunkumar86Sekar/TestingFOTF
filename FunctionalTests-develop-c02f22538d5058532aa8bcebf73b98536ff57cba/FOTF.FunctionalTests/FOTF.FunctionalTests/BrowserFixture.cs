﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Text;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class BrowserFixture : IDisposable
    {
        public IWebDriver driver;
        public string baseURL;
        public StringBuilder verificationErrors;

        public BrowserFixture()
        {
            driver = new FirefoxDriver();
            baseURL =  Environment.GetEnvironmentVariable("TEST_SERVER_HOST") ?? "http://www.focusonthefamily.com";
            verificationErrors = new StringBuilder();
        }

        public void Dispose()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.Equal("", verificationErrors.ToString());
        }
    }
}
