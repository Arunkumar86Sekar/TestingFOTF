﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class BaseTestSuite : IClassFixture<BrowserFixture>
    {
        protected BrowserFixture fixture;
        protected bool acceptNextAlert = true;
        protected IWebDriver driver;
        protected string baseURL;

        public BaseTestSuite(BrowserFixture fixture)
        {
            this.fixture = fixture;
            this.driver = fixture.driver;
            this.baseURL = fixture.baseURL;
        }

        protected bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        protected bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        protected string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
