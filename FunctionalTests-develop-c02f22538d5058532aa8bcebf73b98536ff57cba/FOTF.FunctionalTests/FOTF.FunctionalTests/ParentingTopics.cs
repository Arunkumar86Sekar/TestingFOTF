﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class ParentingTopics : BaseTestSuite
    {
        public ParentingTopics(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheParentingTopicsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/parenting");
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Babies, Toddlers and Preschoolers"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Babies, Toddlers and Preschoolers")).Click();
            Assert.Equal("Babies, Toddlers and Preschoolers | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("School-Age Children"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("School-Age Children")).Click();
            Assert.Equal("School-Age Children | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Teens"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Teens")).Click();
            Assert.Equal("Teens | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Your Child's Emotions"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Your Child's Emotions")).Click();
            Assert.Equal("Your Child's Emotions | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Parenting Roles"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Parenting Roles")).Click();
            Assert.Equal("Parenting Roles | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Parenting Challenges"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Parenting Challenges")).Click();
            Assert.Equal("Parenting Challenges | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Effective Biblical Discipline"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Effective Biblical Discipline")).Click();
            Assert.Equal("Effective Biblical Discipline | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Spiritual Growth for Kids"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Spiritual Growth for Kids")).Click();
            Assert.Equal("Spiritual Growth for Kids | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Sexuality"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Sexuality")).Click();
            Assert.Equal("Sexuality | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Protecting Your Family"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Protecting Your Family")).Click();
            Assert.Equal("Protecting Your Family | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Schooling"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Schooling")).Click();
            Assert.Equal("Schooling | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Building Relationships"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Building Relationships")).Click();
            Assert.Equal("Building Relationships | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Single Parenting"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Single Parenting")).Click();
            Assert.Equal("Single Parenting | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Adoptive Families"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Adoptive Families")).Click();
            Assert.Equal("Adoptive Families | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Holidays"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Holidays")).Click();
            Assert.Equal("Holidays | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Parenting")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
        }
    }
}
