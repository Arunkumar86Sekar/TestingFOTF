﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class LifeChallengesTopics : BaseTestSuite
    {
        public LifeChallengesTopics(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheLifeChallengesTopicsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/lifechallenges");
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Abuse and Addiction"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Abuse and Addiction")).Click();
            Assert.Equal("Abuse and Addiction | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Emotional Health"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Emotional Health")).Click();
            Assert.Equal("Emotional Health | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Life Transitions"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Life Transitions")).Click();
            Assert.Equal("Life Transitions | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Love and Sex"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Love and Sex")).Click();
            Assert.Equal("Love and Sex | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Managing Money"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Managing Money")).Click();
            Assert.Equal("Managing Money | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Relationship Challenges"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Relationship Challenges")).Click();
            Assert.Equal("Relationship Challenges | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Life Challenges")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Understanding Homosexuality"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Understanding Homosexuality")).Click();
            Assert.Equal("Understanding Homosexuality | Focus on the Family", driver.Title);
        }
    }
}
