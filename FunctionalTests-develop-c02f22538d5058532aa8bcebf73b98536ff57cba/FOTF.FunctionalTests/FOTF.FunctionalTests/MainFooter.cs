using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class MainFooter : BaseTestSuite
    {
        public MainFooter(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheFotfMainPageFooterTest()
        {
            // //Set base URL variables
            String baseURL = "/";
            String baseTitle = "Focus on the Family: Helping Families Thrive";
            // //Open Page
            driver.Navigate().GoToUrl(baseURL + baseURL);
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (baseTitle == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal(baseTitle, driver.Title);
            // //Footer-ad
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/div[2]/div"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/div[2]/div")));
            // //Footer Container
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer")));
            // //Footer - top
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div")));
            // //Footer - top -- image
            // //Footer - top --image ---Small Image
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[1]/a/i[1]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[1]/a/i[1]")));
            // //Footer - top --image ---Large Image
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[1]/a/i[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[1]/a/i[2]")));
            // //Footer - top --image ---Link
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[1]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family: Helping Families Thrive" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family: Helping Families Thrive", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -top -- Social Media
            // //Footer -top --Social Media ----Facebook
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[1]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[1]")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[1]/img")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[1]")));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[2]/a[1]")).Click();
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -top --Social Media ----Twitter
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[2]")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[2]/img")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[2]")));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[2]/a[2]")).Click();
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -top --Social Media ----Google +
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[3]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[3]")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[3]/img")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[3]")));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[2]/a[3]")).Click();
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -top --Social Media ----Email
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[4]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[4]")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[4]/img")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[4]")));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[2]/a[4]")).Click();
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -top --Social Media ----YouTube
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[5]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[5]")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[5]/img")));
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/div[2]/a[5]")));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/div/div[2]/a[5]")).Click();
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer - top -- newsletter signup
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form")));
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/label"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Sign up for our Newsletters", driver.FindElement(By.XPath("//html/body/footer/div[1]/div/form/label")).Text);
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/input[1]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/input[1]")));
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/input[1]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/button"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/div/form/button")));
            Assert.Equal("Sign Up", driver.FindElement(By.XPath("//html/body/footer/div[1]/div/form/button")).Text);
            // //Footer - nav
            // //Footer - nav --links ---donate
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/a")));
            Assert.Equal("Donate", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/a")).Text);
            // //Footer - nav --links ---donate ----Make a Donation
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[1]/a")));
            Assert.Equal("Make a Donation", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[1]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[1]/a")).Click();
            // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | title=Donate to Focus on the Family | ]]
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Donate to Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Donate to Focus on the Family", driver.Title);
            driver.Close();
            // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | title=${baseTitle} | ]]
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (baseTitle == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal(baseTitle, driver.Title);
            // //Footer - nav --links ---donate ----Why Donate?
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[2]/a")));
            Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[2]/a")).Text, "^Why Donate[\\s\\S]$"));
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (Regex.IsMatch(driver.Title, "^Why Invest in Families[\\s\\S]$")) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(Regex.IsMatch(driver.Title, "^Why Invest in Families[\\s\\S]$"));
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer - nav --links ---donate ----How Funds Are Used
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[3]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[3]/a")));
            Assert.Equal("How Funds Are Used", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[3]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[3]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Financial Accountability" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Financial Accountability", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer - nav --links ---donate ----Estate Planning
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[4]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[4]/a")));
            Assert.Equal("Estate Planning", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[4]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[1]/ul/li[4]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Planned Giving Home" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Planned Giving Home", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Shop
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/a")));
            Assert.Equal("Shop", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/a")).Text);
            // //Footer -nav --links ---Shop ----Magazines
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[1]/a")));
            Assert.Equal("Magazines", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[1]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[1]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Magazines - Christianbook.com" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Magazines - Christianbook.com", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Shop ----All products
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[2]/a")));
            Assert.Equal("All Products", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[2]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[2]/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("All Products - Christianbook.com Search" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("All Products - Christianbook.com Search", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/a")));
            Assert.Equal("Media", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/a")).Text);
            // //Footer -nav --links ---Media ----Daily Broadcast
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[1]/a")));
            Assert.Equal("Daily Broadcast", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[1]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[1]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Daily Broadcast | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Daily Broadcast | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----Adventures in Odyssey
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[2]/a")));
            Assert.Equal("Adventures in Odyssey", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[2]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Adventures in Odyssey | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Adventures in Odyssey | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----Plugged In
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[3]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[3]/a")));
            Assert.Equal("Plugged In", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[3]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[3]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Plugged In Entertainment Reviews | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Plugged In Entertainment Reviews | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----Radio Theatre
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[4]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[4]/a")));
            Assert.Equal("Radio Theatre", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[4]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[4]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Radio Theatre | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Radio Theatre | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----The Boundless Show
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[5]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[5]/a")));
            Assert.Equal("The Boundless Show", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[5]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[5]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("The Boundless Show | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("The Boundless Show | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----Weekend
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[6]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[6]/a")));
            Assert.Equal("Weekend", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[6]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[6]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Weekend | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Weekend | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Media ----All Shows
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[7]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[7]/a")));
            Assert.Equal("All Shows", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[7]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[3]/ul/li[7]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Shows - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Shows - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/a")));
            Assert.Equal("About", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/a")).Text);
            // //Footer -nav --links ---About ----AboutUs
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[1]/a")));
            Assert.Equal("About Us", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[1]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[1]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("About Us - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("About Us - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ---- Global Outreach
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[2]/a")));
            Assert.Equal("Global Outreach", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[2]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Global Outreach - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Global Outreach - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ----Contact US
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[3]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[3]/a")));
            Assert.Equal("Contact Us", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[3]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[3]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (Regex.IsMatch(driver.Title, "^How do I contact Focus on the Family[\\s\\S]$")) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(Regex.IsMatch(driver.Title, "^How do I contact Focus on the Family[\\s\\S]$"));
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ----Visit Us
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[4]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[4]/a")));
            Assert.Equal("Visit Us", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[4]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[4]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Welcome Center - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Welcome Center - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ----On-Campus Bookstore
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[5]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[5]/a")));
            Assert.Equal("On-Campus Bookstore", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[5]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[5]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Bookstore - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Bookstore - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ----Jobs & Volunteering
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[6]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[6]/a")));
            Assert.Equal("Jobs & Volunteering", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[6]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[6]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Focus on the Family Careers" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Focus on the Family Careers", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---About ----Advertise With Us
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[7]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[7]/a")));
            Assert.Equal("Advertise With Us", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[7]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[4]/ul/li[7]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Advertise With Focus on the Family - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Advertise With Focus on the Family - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/a")));
            Assert.Equal("Popular Websites", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/a")).Text);
            // //Footer -nav --links ---Popular Websites ----Plugged In
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[1]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[1]/a")));
            Assert.Equal("Plugged In", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[1]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[1]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Plugged In" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Plugged In", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----Boundless
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[2]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[2]/a")));
            Assert.Equal("Boundless", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[2]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Boundless.org | A Website for Christian Singles and Young Adults" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Boundless.org | A Website for Christian Singles and Young Adults", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----Kids' Websites
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[3]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[3]/a")));
            Assert.Equal("Kids' Websites", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[3]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[3]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("We've Got Your Kids Covered | Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("We've Got Your Kids Covered | Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----The Truth Project
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[4]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[4]/a")));
            Assert.Equal("The Truth Project", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[4]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[4]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("The Truth Project" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("The Truth Project", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----National Institute of Marriage
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[5]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[5]/a")));
            Assert.Equal("National Institute of Marriage", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[5]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[5]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Christian Marriage Counseling - Focus on the Family's National Institute of Marriage" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Christian Marriage Counseling - Focus on the Family's National Institute of Marriage", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----That the World May Know
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[6]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[6]/a")));
            Assert.Equal("That the World May Know", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[6]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[6]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("That the World May Know | Welcome" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("That the World May Know | Welcome", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer -nav --links ---Popular Websites ----All Websites and Ministries
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[7]/a"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[7]/a")));
            Assert.Equal("All Websites and Ministries", driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[7]/a")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[1]/nav/ul/li[5]/ul/li[7]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Websites and Ministries - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Websites and Ministries - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer--bottom
            // //Footer--bottom--container
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]")));
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div")));
            // //Footer--bottom---container----nav
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav")));
            // //Footer --bottom ---container ----nav -----nav_list
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul")));
            // //Footer --bottom ---container ----nav -----nav_list ------nav_items -------FAQs
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[1]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[1]")));
            Assert.Equal("FAQs", driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[1]")).Text);
            // //Footer --bottom ---container ----nav -----nav_list ------nav_items -------Privacy & Terms
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[2]")));
            Assert.Equal("Privacy & Terms", driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[2]")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[2]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("FOCUS ON THE FAMILY POLICIES - Focus on the Family" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("FOCUS ON THE FAMILY POLICIES - Focus on the Family", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer --bottom ---container ----nav -----nav_list ------nav_items -------En Espanol
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[3]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[3]")));
            Assert.Equal("En Español", driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[3]")).Text);
            driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[3]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if ("Enfoque a la Familia" == driver.Title) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.Equal("Enfoque a la Familia", driver.Title);
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer --bottom ---container ----nav -----nav_list ------nav_items -------Need Help?
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[4]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[4]")));
            Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[4]")).Text, "^Need Help[\\s\\S] Call 1-800-A-FAMILY \\(232-6459\\)$"));
            driver.FindElement(By.XPath("//html/body/footer/div[2]/div/nav/ul/li[4]/a")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (Regex.IsMatch(driver.Title, "^How do I contact Focus on the Family[\\s\\S]$")) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(Regex.IsMatch(driver.Title, "^How do I contact Focus on the Family[\\s\\S]$"));
            driver.Navigate().GoToUrl(baseURL + baseURL);
            // //Footer --bottom ---container ----copyright
            for (int second = 0;; second++) {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.XPath("//html/body/footer/div[2]/div/p[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            Assert.IsTrue(IsElementPresent(By.XPath("//html/body/footer/div[2]/div/p[2]")));
            Assert.Equal("© 1997–2016 Focus on the Family", driver.FindElement(By.XPath("//html/body/footer/div[2]/div/p[2]")).Text);
        }
    }
}
