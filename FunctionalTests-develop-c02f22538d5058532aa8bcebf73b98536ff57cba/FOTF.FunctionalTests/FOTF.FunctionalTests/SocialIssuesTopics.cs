﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class SocialIssuesTopics : BaseTestSuite
    {
        public SocialIssuesTopics(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheSocialIssuesTopicsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/faith/christian-singles");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Christian Singles"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            Assert.Equal("Christian Singles | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Life Issues"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Life Issues")).Click();
            Assert.Equal("Life Issues | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Family"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Family")).Click();
            Assert.Equal("Family | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Religious Freedom"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Religious Freedom")).Click();
            Assert.Equal("Religious Freedom | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            Assert.Equal("Social Issues | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Sexuality"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Sexuality")).Click();
            Assert.Equal("Sexuality | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Education"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Education")).Click();
            Assert.Equal("Education | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Citizen Magazine"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Citizen Magazine")).Click();
            Assert.Equal("Citizen Magazine | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("How to Get Involved"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("How to Get Involved")).Click();
            Assert.Equal("How to Get Involved | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Social Issues")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Understanding the Issues"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Understanding the Issues")).Click();
            Assert.Equal("Understanding the Issues | Focus on the Family", driver.Title);
        }
    }
}
