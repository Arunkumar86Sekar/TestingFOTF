﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class MarriageTopics : BaseTestSuite
    {
        public MarriageTopics(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheMarriageTopicsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/marriage");
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Dating Your Spouse"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Dating Your Spouse")).Click();
            Assert.Equal("Dating Your Spouse | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("God's Design for Marriage"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("God's Design for Marriage")).Click();
            Assert.Equal("God's Design for Marriage | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Preparing for Marriage"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Preparing for Marriage")).Click();
            Assert.Equal("Preparing for Marriage | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("The Early Years"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("The Early Years")).Click();
            Assert.Equal("The Early Years | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Daily Living"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Daily Living")).Click();
            Assert.Equal("Daily Living | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Communication and Conflict"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Communication and Conflict")).Click();
            Assert.Equal("Communication and Conflict | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Sex and Intimacy"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Sex and Intimacy")).Click();
            Assert.Equal("Sex and Intimacy | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Money and Finances"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Money and Finances")).Click();
            Assert.Equal("Money and Finances | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Strengthening Your Marriage"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Strengthening Your Marriage")).Click();
            Assert.Equal("Strengthening Your Marriage | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Marriage Challenges"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Marriage Challenges")).Click();
            Assert.Equal("Marriage Challenges | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Facing Crisis"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Facing Crisis")).Click();
            Assert.Equal("Facing Crisis | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Divorce and Infidelity"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Divorce and Infidelity")).Click();
            Assert.Equal("Divorce and Infidelity | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Marriage")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Marriage and the Military"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Marriage and the Military")).Click();
            Assert.Equal("Marriage and the Military | Focus on the Family", driver.Title);
        }
    }
}
