﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace FOTF.FunctionalTests
{
    public class FaithTopics : BaseTestSuite
    {
        public FaithTopics(BrowserFixture fixture) : base(fixture) { }

        [Fact]
        public void TheFaithTopicsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            Assert.Equal("Focus on the Family: Helping Families Thrive", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Becoming a Christian"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Becoming a Christian")).Click();
            Assert.Equal("Becoming a Christian | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Christian Worldview"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Christian Worldview")).Click();
            Assert.Equal("Christian Worldview | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Spiritual Development"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Spiritual Development")).Click();
            Assert.Equal("Spiritual Development | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("The Study of God"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("The Study of God")).Click();
            Assert.Equal("The Study of God | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Faith In Life"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Faith In Life")).Click();
            Assert.Equal("Faith In Life | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Faith-based Family Finances"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Faith-based Family Finances")).Click();
            Assert.Equal("Faith-based Family Finances | Focus on the Family", driver.Title);
            driver.FindElement(By.LinkText("Faith")).Click();
            Assert.Equal("Faith | Focus on the Family", driver.Title);
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("All Topics"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("All Topics")).Click();
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.True(false, "timeout");
                try
                {
                    if (IsElementPresent(By.LinkText("Christian Singles"))) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
            driver.FindElement(By.LinkText("Christian Singles")).Click();
            Assert.Equal("Christian Singles | Focus on the Family", driver.Title);
        }
    }
}
