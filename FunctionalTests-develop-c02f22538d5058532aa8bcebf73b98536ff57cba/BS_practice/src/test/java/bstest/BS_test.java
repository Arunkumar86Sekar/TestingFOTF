package bstest;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.*;
import java.net.URL;
import org.testng.annotations.*;

public class BS_test {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private WebDriver driver;
  private String username = System.getenv("BROWSERSTACK_USER");
  private String accessKey = System.getenv("BROWSERSTACK_ACCESSKEY");
  private String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
  private String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
  private String url = "https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub";

  @BeforeClass(alwaysRun = true)
  public void setup() throws Exception {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("os", "Windows");
    capabilities.setCapability("browser", "chrome");
    capabilities.setCapability("browserstack.local", browserstackLocal);
    capabilities.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
    driver = new RemoteWebDriver(new URL("https://" + username + ":" + accessKey + "@hub.browserstack.com/wd/hub"), capabilities);
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
  driver.quit();
}

@Test
public void test(){
  driver.get("https://www.google.com/");
  WebElement element = driver.findElement(By.name("q"));

  element.sendKeys("BrowserStack");
  element.submit();

  System.out.println(driver.getTitle());
  }
}
