smokeTestREADME.md


//Channel Smoke Test Guidelines
//This file defines what needs to be included in a comprehensive smoke test.

//Instructions for Running Smoketests in Browserstack Automate
	1. 	Download and install BrowserStackLocal-darwin-x64.zip from the BrowserStack Directions page (https://www.browserstack.			com/local-testing)
	2. 	To initiate Browserstack local after you install it, (in Terminal on Mac) type ./browserstacklocal [your Browserstack key]. 	This will allow Browserstack to access sites within your LAN.
	3. There are three environment variables you need to declare in order for the smoketest to run in Browserstack Automate.
		a. The site you want to test against, for example, export TEST_SERVER_HOST=http://test.focusonthefamily.com
		b. Your Browserstack user name: export BROWSERSTACK_USER=[your Browserstack username]
		c. Your Browserstack key: export BROWSERSTACK_ACCESSKEY=[Your Browserstack access key]
		(Your Browserstack username and access key are found at https://www.browserstack.com/accounts/settings.)
	4. Once you've set values for these variables, you are ready to initiate the tests with the following command: 'mvn clean test

	--------------------------------------'

//Header:
	All header links function correctly:
		Primary Nav links
			Marriage
			Parenting
			Life Challenges
			Faith
			Social Issues
			Pro Life
		Logo
		Today's Broadcast Link
		Family Help Link
		About Link
		Store Link
		Search Field
		Donate button
---------------------------------
//Home Page
	Slide Show
	Channel Tabs
		Marriage
		Parenting
		Life Challenges
		Social Issues
	Family Store
	Jim Daly Blog object
	Right Sidebar Donation Item
	Today's Broadcast Object
	Family Resource of the Month
	Contact Links
	Pre-footer ad
---------------------------------
//Footer Links
	Logo
	Social Sharing Buttons
	Newsletter Signup (Aprimo link)
	Donate Links
		Make a Donation
		Why Donate?
		How Funds Are Used
		Estate Planning
	Shop
		Magazines
		All Products
	Media
		Daily Broadcast
		Adventures in Odyssey
		Plugged In
		Radio Thatre
		Boundless Show
		Weekend
		All Shows
	About
		About Us
		Global Outreach
		Contact Us
		Visit Us
		On-Campus Bookstore
		Jobs & Voluteering
		Advertise With Us

	Popular Websites
		Plugged In
		Boundless
		Kids' Websites
		The Truth Project
		Hope Restored
		That the World May Know
		All Websites and Ministries

//Post footer
	FAQs
	Privacy and Terms
	En Español
	Need Help
--------------------------------

//Channel Pages
	//All Channel page tests follow the same pattern

	Channel Header
		All Topic Links
		Featued Item
			Featured Item Image
			Related Items Links
			Recent Related Article Object
			Recent Related Episode Object
			Initiative Object(s)
			Channel Page Article Objects
				Article Image
				Article Title
				Article Button Link
			Channel Page PreFooter
				More Channel Resources Objects
				Topic links
				Preefooter Ad

//Media Center
	Daily Broacast Header
	Search Filter
	Latest Episode
	Upcoming Episodes
	Related Content Objects
	Bonus Page

//Banner Test
	Campaign--Banner Object Visible

//Site APIs
	Broadcast Mobile Feed
	Broadcast Podcast Feed
	Channel Sitemaps
	Media Center Sitemaps
	General Sitemap
