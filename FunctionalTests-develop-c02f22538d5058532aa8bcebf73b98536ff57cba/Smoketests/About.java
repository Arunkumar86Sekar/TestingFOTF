package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class About {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.focusonthefamily.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAbout() throws Exception {
    driver.get(baseUrl + "/about");
    driver.findElement(By.linkText("Our Vision")).click();
    driver.findElement(By.linkText("Our Mission")).click();
    driver.findElement(By.linkText("Our Values")).click();
    driver.findElement(By.linkText("Our Beliefs")).click();
    driver.findElement(By.cssSelector("img[alt=\"Marriage\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Parenting\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Life Challenges\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Faith\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Social Issues\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Pro Life\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on the Family Daily Broadcast\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Adventures in Odyssey\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Plugged In Entertainment Reviews\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Boundless\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on Marriage Podcast\"]")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on Parenting Podcast\"]")).click();
    driver.findElement(By.linkText("More")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on the Family Commentary\"]")).click();
    driver.findElement(By.linkText("More")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on the Family Minute\"]")).click();
    driver.findElement(By.linkText("More")).click();
    driver.findElement(By.cssSelector("img[alt=\"Focus on the Family Weekend\"]")).click();
    driver.findElement(By.linkText("More")).click();
    driver.findElement(By.cssSelector("img[alt=\"Radio Theatre\"]")).click();
    driver.findElement(By.linkText("More")).click();
    driver.findElement(By.xpath("//div[2]/div[5]/figure/a/img")).click();
    driver.findElement(By.linkText("More About Our History")).click();
    driver.findElement(By.linkText("More About Jim Daly")).click();
    driver.findElement(By.xpath("//em[2]")).click();
    driver.findElement(By.xpath("//div[3]/a/em[2]")).click();
    driver.findElement(By.xpath("//div[4]/a/em[2]")).click();
    driver.findElement(By.xpath("//div[5]/a/em[2]")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
