//model
package smoketests;

// import org.testng.annotations.AfterMethod;
// import org.testng.annotations.Test;
// import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
// import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class mediaCenterTestPage{
  private WebDriver driver;
  private WebDriverWait wait;
  private String subUrl = "/media/daily-broadcast";

  public mediaCenterTestPage(WebDriver _driver) throws Exception {

    driver = _driver;
     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
     wait = new WebDriverWait(driver, 30);

  }

  public void quit() throws Exception{
      System.out.println("Quitting tests");
      driver.quit();
  }

  public String pageTitle() {
    return driver.getTitle();
  }

  public void openPage(String url) {
    driver.get(url + subUrl);
  }

  public WebElement findElement(String xpath) {
    return driver.findElement(By.xpath(xpath));
  }

  public boolean doesElementExist(String xpath) {
    boolean present;
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    present =  driver.findElements(By.xpath(xpath) ).size() != 0;
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    return present;

  }

  public boolean isElementPresent(String xpath, String name) {
    try {
      driver.findElement(By.xpath(xpath));
      System.out.println("Element -->" + name + "<-- found");
      return true;
    } catch (NoSuchElementException e) {
      System.out.println("Element -->" + name + "<-- not found");
      return false;
    }
  }

  public String getHeader() {
    String xpath = "//h1[@class='show_header--title']";
    return driver.findElement(By.xpath(xpath)).getText();
  }

  public Link[] getLinks(){
    Link[] links = new Link[3];
    links[0] = new Link();
    links[1] = new Link();
    links[2] = new Link();
    links[0].name = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[1]")).getText();
    links[0].link = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[1]/a")).getAttribute("href");
    links[1].name = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[2]")).getText();
    links[1].link = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[2]/a")).getAttribute("href");
    links[2].name = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[3]")).getText();
    links[2].link = driver.findElement(By.xpath("//ul[@class='additional_content--links_list']/li[3]/a")).getAttribute("href");

    return links;
  }

  public Result[] search(String searchString){
    driver.findElement(By.xpath("(//input[@type='search'])[2]")).clear();
    driver.findElement(By.xpath("(//input[@type='search'])[2]")).sendKeys(searchString);
    String xpath = "//div[@id='loading-spinner']";
    System.out.print("Loading");

    for(int i = 0; doesElementExist(xpath); i++){
      System.out.print(">");
      try {
      // thread to sleep for 500 milliseconds
      Thread.sleep(250);
    } catch (Exception e) {
      System.out.println(e);
    }
    if (i >= 120) {
      break;
    }
    }

    Result[] results = new Result[10];

    for (int i = 0; i < 5; i++) {
      results[i] = new Result();
      results[i].load(i+1,driver);
    }
    for (int i = 6; i <= 10; i++) {
      results[i-1] = new Result();
      results[i-1].load(i+1,driver);
    }

    return results;
  }

  public boolean isElementPresent(String xpath) {
    // display("Is element present");
    // display("by = " + by);
    try {
      driver.findElement(By.xpath(xpath));
      // display("element found");
      return true;
    } catch (NoSuchElementException e) {
      // display("element not found");
      return false;
    }
  }

  public String getText(String xpath) {
      return driver.findElement(By.xpath(xpath)).getText();
  }

  public String getAttribute(String xpath, String attr) {
      return driver.findElement(By.xpath(xpath)).getAttribute(attr);
  }

  public void checkImage (String xpath) {
    WebElement ImageFile = driver.findElement(By.xpath(xpath));

    Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver)
    .executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
    if (!ImagePresent)
    {
         System.out.println("Image not displayed.");
    }
    else
    {
        System.out.println("Image displayed.");
    }
  }

  public boolean isTextPresent(String xpath) {
    //check for any text
    WebElement element = driver.findElement(By.xpath(xpath));
    String text = element.getText();
    if (text.length() > 0){
      System.out.println("Text present -->\n" + text);
      return true;
    } else {
      System.out.println("Text not found");
      return false;
    }
  }

  public String buildXpath(String element, String type, String path) {
    return "//" + element + "[@" + type + "='" + path + "']";
  }

  public void open(String url) {
    driver.get(url);
  }

  public int getResponseCode(String urlString) throws MalformedURLException, IOException {
    URL u = new URL(urlString);
    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
    huc.setRequestMethod("GET");
    huc.connect();
    return huc.getResponseCode();
 }

 public int getNumberElements(String xpath) {
   int numElements = driver.findElements(By.xpath(xpath)).size();
   return numElements;
 }
}
