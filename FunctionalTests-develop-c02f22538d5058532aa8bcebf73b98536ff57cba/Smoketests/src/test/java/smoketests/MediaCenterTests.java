//view
package smoketests;

import org.testng.Assert;
import org.testng.AssertJUnit;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;


public class MediaCenterTests {
  private String baseTitle = "Focus on the Family Daily Broadcast | Focus on the Family";
  private mediaCenterTestPage testPage;
  private String _baseUrl;
  String fotfBaseTitle = " | Focus on the Family";

public MediaCenterTests(String baseUrl, WebDriver _driver) throws Exception{
    System.out.println("Creating URLS");
    testPage = new mediaCenterTestPage(_driver);
    // broadcastTestPage = new broadcastTestPage(BSUrl);
    _baseUrl = baseUrl;
}

public void quitTest() throws Exception{
  testPage.quit();
 }

public void openHomePage() throws Exception{
   String siteName = "Daily Broadcast";
   System.out.println("----------> Opening  " + siteName);
   testPage.openPage(_baseUrl);
 }

 public void testTitle() {
   String actual = testPage.pageTitle();
   String expected = baseTitle;

   System.out.println("Expected ->" + expected + "\nActual --->" + actual);
   assertEquals(expected, actual);
 }

 public void testHeaderText() {
   testPage.openPage(_baseUrl);
   String name = "Header text";
   System.out.println("Testing -->" + name + "<--");

   String actual = testPage.getHeader();
   String expected = "Focus on the Family Daily Broadcast";

   System.out.println("Expected ->" + expected + "\nActual --->" + actual);

   assertEquals(expected, actual);
 }

 public void testHeaderLinks() {
   testPage.openPage(_baseUrl);
   String name = "Header links";
   System.out.println("Testing -->" + name + "<--");

   Link[] expectedLinks = new Link[3];

   expectedLinks[0] = new Link();
   expectedLinks[1] = new Link();
   expectedLinks[2] = new Link();

   expectedLinks[0].name = "Find a Station";
   expectedLinks[0].link = "http://www.focusonthefamily.com/media/fotf/media-center/shows/daily-broadcast/find-a-station";
   expectedLinks[1].name = "Mobile App";
   expectedLinks[1].link = "http://www.focusonthefamily.com/about/programs#mobile";
   expectedLinks[2].name = "iTunes";
   expectedLinks[2].link = "https://geo.itunes.apple.com/us/podcast/focus-on-family-daily-broadcast/id290803395?mt=2";

   int numLinks = expectedLinks.length;

   Link[] actualLinks = testPage.getLinks();

   for (int i =0; i < numLinks; i++){
     System.out.println("Expected Name->" + expectedLinks[i].name + "\nActual Name--->" + expectedLinks[i].name);
     System.out.println("Expected Name->" + expectedLinks[i].link + "\nActual Name--->" + expectedLinks[i].link);
     assertEquals(expectedLinks[i].name, actualLinks[i].name);
     assertEquals(expectedLinks[i].link, actualLinks[i].link);
   }
 }

 public void testElementPresent(String name, String xpath) {
   System.out.println("Testing -->" + name + "<--");
   assertTrue(testPage.isElementPresent(xpath, name));
 }

 public void testEpisodeSearch(){
   System.out.println("<-------------Testing Episode Search------------->");

   testPage.openPage(_baseUrl);
   String searchValue = "Andy";
   searchValue = searchValue.toLowerCase();
   Result[] actualGuestResults = testPage.search(searchValue);
  //  Result[] actualTitleResults = testPage.search(searchValue);
   Result[] actualTitleResults = actualGuestResults;


   System.out.println("\nResults contain value ->" + searchValue + " <-");
   for (int i = 0; i < 10; i++) {
     boolean guestValue = actualGuestResults[i].guest.toLowerCase().contains(searchValue);
     boolean titleValue = actualTitleResults[i].title.toLowerCase().contains(searchValue);

     System.out.println("Actual Guest--->" + actualGuestResults[i].guest);
     System.out.println("Actual Title--->" + actualTitleResults[i].title);

    assertTrue(guestValue || titleValue);
   }
 }

 public void testLastestEpisodePresent() {
   System.out.println("<-------------Testing Latest Episode------------->");

   testPage.openPage(_baseUrl);

   testElementPresent("Latest Episode", "//*[@id='latest-episode']/div/h2");
   testElementPresent("Aired", "//*[@id='latest-episode']/div/p");
   testElementPresent("Aired", "//*[@id='latest-episode']/div/p");
   testElementPresent("Episode Link", "//*[@id='latest-episode']/h3");
   testElementPresent("Listen Now", "//*[@id='latest-episode']/a");

   String episodeExpectedTitle = testPage.getText("//*[@id='latest-episode']/h3/a") + fotfBaseTitle;
   testPage.openPage(testPage.getAttribute("//*[@id='latest-episode']/a", "href"));
   String episodeActualTitle = testPage.pageTitle();
   System.out.println("Testing titles: \nExpected ->" + episodeExpectedTitle + "\nActual --->" + episodeActualTitle);

   assertEquals(episodeExpectedTitle, episodeActualTitle);
 }

 public void testUpcomingEpisodesPresent() {
   System.out.println("<-------------Testing Upcoming Episodes------------->");

   String localXpath = testPage.buildXpath("*", "class", "recent_episodes--header");

   testPage.openPage(_baseUrl + "/media/daily-broadcast/encouragement-for-remarried-couples-pt1");
   testElementPresent("Upcoming Episodes", localXpath);
 }

 public void testAside() {
   System.out.println("<-------------Testing Aside------------->");
   testPage.openPage(_baseUrl);
   int numHosts = 2;

   for(int i = 1; i <= numHosts; i++) {
     String localXpath = "//div[" + i + "][@class='show_host--container']";
     testElementPresent("Show Host -" + i, localXpath);
     //check image
     testElementPresent("Checking Host image-" + i, localXpath + "/div");
     testPage.checkImage(localXpath + "/div/img");

     //check title
     testElementPresent("Checking Host Title-" + i, localXpath + "/h3");
     testPage.isTextPresent(localXpath + "/h3");

     //check text
     testElementPresent("Checking Host Bio-" + i, localXpath + "/p");
     testPage.isTextPresent(localXpath + "/p");
   }

 }

 public void broadcastPage() throws Exception {
   String localXpath = testPage.buildXpath("*", "class", "episode_header--title");

   //test title
   testPage.open(_baseUrl + "/media/daily-broadcast/encouragement-for-remarried-couples-pt1");
   testElementPresent("Title", localXpath);

   // test media player
   testElementPresent("Checking Media Player -", testPage.buildXpath("*", "id", "mediaplayer"));

   //test episode_summary
   testElementPresent("Checking episode_summary ", testPage.buildXpath("div", "class", "episode_summary"));
   testPage.isTextPresent(testPage.buildXpath("div", "class", "episode_summary") + "/div/p");

   //test episode_summary link
   testElementPresent("Checking episode_summary link ", testPage.buildXpath("div", "class", "episode_summary") + "/div/p[2]");

   assertTrue(testPage.getResponseCode(testPage.getAttribute(testPage.buildXpath("div", "class", "episode_summary") + "/div/p[2]/a", "href")) != 404);

 }

 public void bonusPage() throws Exception {
   String url = "/media/daily-broadcast/gods-word-on-divorce-and-remarriage-pt1/bonus";
   testPage.open(_baseUrl + url);

   String expected = "God's Word on Divorce and Remarriage (Part 1 of 2): More Resources | Focus on the Family";

   String actual = testPage.pageTitle();

   System.out.println("Expected ->" + expected + "\nActual --->" + actual);
   assertEquals(expected, actual);
 }
}
