package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Sitecore_Tests {
  private FOTF_UtilityFunctions testPage;
  private String baseUrl;
  private String baseTitle;

  public FOTF_Sitecore_Tests(String _baseUrl, WebDriver _driver ) throws Exception {
    baseTitle = " | Focus on the Family";
    baseUrl = _baseUrl;
    testPage = new FOTF_UtilityFunctions(_driver);
  }

  public void quitTest() throws Exception{
    testPage.quit();
 }

 public void loginAndTestTile() {
    System.out.println("<---- Log into Sitecore ---->");
    testPage.logintoSitecore(baseUrl + "/sitecore/login", "admin", "b");
    System.out.println("<---- Testing Sitecore Title ---->");
    String expected_A = "Sitecore Launchpad";
    String expected_B = "Desktop";
    assertTrue(testPage.testTitle(expected_A) || testPage.testTitle(expected_B));
 }
}
