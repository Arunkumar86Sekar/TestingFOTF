//This test checks for a functioning sitecore website.  It only checks for a
 // title, main page and six channel pages.
package smoketests;

import org.testng.annotations.*;

@Listeners(ScreenShotOnFailure.class)

public class FOTF_Environment_SmokeTest {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private FOTF_Environment_Tests test;
  private Browserstack browserstack;

@BeforeClass(alwaysRun = true)
@Parameters(value={"browser","browser_version","platform", "os", "os_version", "device"})
public void setup(String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception {

  System.out.println("\n<---------------FOTF_Environment_SmokeTest--------------->");
  System.out.println(baseUrl);

  browserstack = new Browserstack(browser, browser_version, platform, os, os_version, device);

  test = new FOTF_Environment_Tests(baseUrl, browserstack.driver);
}

@AfterClass(alwaysRun = true)
public void tearDown() throws Exception {
  browserstack.driver.quit();
}

@Test
public void testMainPage() {
  test.testMainPageTitle();
}

@Test
public void testChannels() {
  test.testChannelTitles();
}

@Test
public void testDailyBroadcast() {
  test.testDailyBroadcastTitle();
}

@Test
public void testAboutUs() {
  test.testAboutUsTitle();
}
}
