package smoketests;

public class URLString {
  public String url;
  public String refcd;
  public String visitorid;


public URLString (String url) {
  loadURLValues(url);
}

public void loadURLValues(String _url) {
  url = _url;

  if (_url.contains("visitorid=")) {
  visitorid = _url.substring(_url.indexOf("visitorid="));
  if (visitorid.contains("&")) {
    visitorid = visitorid.substring(0, visitorid.indexOf("&"));
    }

  } else visitorid = null;

  if (_url.contains("refcd=")) {
    refcd = _url.substring(_url.indexOf("refcd="));
  if (refcd.contains("&")) {
    refcd = refcd.substring(0, refcd.indexOf("&"));
    }
  } else refcd = null;

}

}
