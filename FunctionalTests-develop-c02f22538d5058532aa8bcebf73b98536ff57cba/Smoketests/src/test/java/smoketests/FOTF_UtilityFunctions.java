package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.testng.annotations.*;
import org.testng.ITestResult;
import org.testng.Reporter;

public class FOTF_UtilityFunctions {
  private WebDriver driver;
  private WebDriverWait wait;

  public FOTF_UtilityFunctions(WebDriver _driver) throws Exception {
    driver = _driver;
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    wait = new WebDriverWait(driver, 30);
  }

  public void quit() throws Exception{
      System.out.println("Quitting tests");
      driver.quit();
  }

  public void open(String url) {
    driver.get(url);
  }
  public void open(String name, String url) {
    System.out.println(" Opening -> " + name);
    driver.get(url);
  }

  public String getPageTitle() {
    return driver.getTitle();
  }

  public List<WebElement> getAllElements(String xpath) {
    return driver.findElements(By.xpath(xpath));
  }

  public WebElement getElement (String xpath) {
    return driver.findElement(By.xpath(xpath));
  }
  public WebElement getChildElement(WebElement eleParent, String childXpath) {
    WebElement eleChild = eleParent.findElement(By.xpath(childXpath));
    return eleChild;
  }

  public int getNumElements(String element, String xpath) {
    int numElements = driver.findElements(By.xpath(xpath)).size();
    System.out.println("Found " + numElements + " " + element + "s");
    return numElements;
  }

  public List getNavBarLinks(String xpath, String skipLink) {
    List<WebElement> elementList = new ArrayList();
    elementList.addAll(driver.findElements(By.xpath(xpath)));
    List<String> finalList = new ArrayList();
    for (WebElement element : elementList) {
      String link = element.getAttribute("href");
      if (!link.contains(skipLink)){
        finalList.add(link);
      }
    }
    return finalList;
}

  public List getNavBarLinks(String xpath) {
    List<WebElement> elementList = new ArrayList();
    elementList.addAll(driver.findElements(By.xpath(xpath)));
    List<String> finalList = new ArrayList();
    for (WebElement element : elementList) {
      String link = element.getAttribute("href");
      finalList.add(link);
    }
    return finalList;
  }

  public List findAllLinks(){
  List<WebElement> elementList = new ArrayList();
	  elementList = driver.findElements(By.tagName("a"));
	  elementList.addAll(driver.findElements(By.tagName("img")));
	  List<WebElement> finalList = new ArrayList();
	  for (WebElement element : elementList)
	  {
		  if(element.getAttribute("href") != null  && !element.getAttribute("href").contains("javascript") && !element.getAttribute("href").contains("mailto:"))
		  {
			  finalList.add(element);
		  }
	  }
	  return finalList;
  }

//find all links in an element
  public List findAllLinks(WebElement pageObject){
  List<WebElement> elementList = new ArrayList();
	  elementList = pageObject.findElements(By.tagName("a"));
	  elementList.addAll(pageObject.findElements(By.tagName("img")));
	  List<WebElement> finalList = new ArrayList();
	  for (WebElement element : elementList)
	  {
		  if(element.getAttribute("href") != null  && !element.getAttribute("href").contains("javascript") && !element.getAttribute("href").contains("mailto:"))
		  {
			  finalList.add(element);
		  }
	  }
	  return finalList;
  }

  public String getAttribute(String xpath, String attr) {
    return driver.findElement(By.xpath(xpath)).getAttribute(attr);
  }
  public String getAttribute(WebElement element, String attr) {
    return element.getAttribute(attr);
  }

  public String getText(WebElement element) {
    return element.getText();
  }
  public String getText(String xpath) {
    WebElement element = driver.findElement(By.xpath(xpath));
    return element.getText();
  }

  public int getResponseCode(String urlString) throws MalformedURLException, IOException {
    URL u = new URL(urlString);
    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
    huc.setRequestMethod("GET");
    huc.connect();
    return huc.getResponseCode();
 }

 public int getImageResponseCode(WebElement image) throws Exception{

   HttpResponse response = new DefaultHttpClient().execute(new HttpGet(image.getAttribute("src")));


      // HttpClient client = HttpClientBuilder.create().build();
      // HttpGet request = new HttpGet(image.getAttribute("src"));
      // HttpResponse response = client.execute(request);
      int statusCode = response.getStatusLine().getStatusCode();

      return statusCode;
  }

 public List getImages(String xpath) {
   WebElement element = driver.findElement(By.xpath(xpath));
   List<WebElement> imagesList = element.findElements(By.tagName("img"));
   return imagesList;
 }

 public boolean testImage (String xpath) {
   WebElement ImageFile = driver.findElement(By.xpath(xpath));

   Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver)
   .executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
   if (!ImagePresent)
   {

        return false;
   }
   else
   {
       return true;
   }
 }

 public boolean testImage (WebElement element) {
   WebElement ImageFile = element;

   Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver)
   .executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
   if (!ImagePresent)
   {
        return false;
   }
   else
   {
       return true;
   }
 }

 public boolean isTextPresent(WebElement element) {
   //check for any text
   String text = element.getText();
   if (text.length() > 0){
     display("Text is displayed --> '" + text + "'");
     return true;
   } else {
     display("Text not found");
     return false;
   }
 }
 public boolean isTextPresent(String text) {
   //check for any text
   if (text.length() > 0){
     display("Text is displayed --> '" + text + "'");
     return true;
   } else {
     display("Text not found");
     return false;
   }
 }

 public boolean isElementPresent(By by) {
   try {
     driver.findElement(by);
     return true;
   } catch (NoSuchElementException e) {
     return false;
   }
 }
 public boolean isElementPresent(String xpath) {
   try {
     driver.findElement(By.xpath(xpath));
     return true;
   } catch (NoSuchElementException e) {
     return false;
   }
 }

 public void display(String content) {
   System.out.println(content);
 }

 public void display(int content) {
   System.out.println(content);
 }

 public List getLinks(String xpath){
     List<WebElement> elementList = new ArrayList();
     List<Link> finalList = new ArrayList();
     elementList = getAllElements(xpath);

     for (WebElement element : elementList) {
       Link links = new Link();
       links.name = getText(element);
       if (isTextPresent(links.name) == false) {
         display("text hidden, getting inner Text");
         links.name = element.getAttribute("innerText").trim();
       }
       links.link = getAttribute(element,"href");
       System.out.println("Adding -> " + links.name + " @ " + links.link);
       finalList.add(links);
     }
     return finalList;
 }

 public List getArticles(String xpath, String headerPath, String linkPath, String textPath) {
   List<WebElement> elementList = new ArrayList();
   List<Article> finalList = new ArrayList();
   elementList = getAllElements(xpath);

   for (WebElement element : elementList) {
     Article article = new Article();
     article.header = getText(getChildElement(element, headerPath));
     article.link = getAttribute(getChildElement(element, linkPath),"href");
     article.text = getText(getChildElement(element, textPath));
     System.out.println("Adding -> " + article.header + " @ " + article.link + "\nText = " + article.text);
     finalList.add(article);
   }
   return finalList;
 }

 public boolean testTrue(String expected, String actual) {
   System.out.println("Expected ->" + expected + "\nActual --->" + actual);
   return expected.equals(actual);
 }

 public boolean testContains(String str1, String str2) {
   System.out.print("Does " + str1 + " contain -->" + str2 + "? -->");
   boolean val = str1.contains(str2);
   System.out.println(val);
   return val;
   }

public String simplify(String string) {
  // convert string to lowercase, remove -'s, and spaces"
  return string.toLowerCase().replace(" ", "");
}

public void logintoSitecore(String url, String usernameText, String passwordText) {
  driver.get(url);
  driver.manage().window().maximize();

  WebElement username = driver.findElement(By.id("UserName"));
  WebElement password = driver.findElement(By.id("Password"));
  WebElement button = driver.findElement(By.xpath("//input[@value='Log in']"));

  username.clear();
  password.clear();

  username.sendKeys(usernameText);
  password.sendKeys(passwordText);

  button.click();
  // clickButton("//input[@class='btn btn-primary btn-block']");
}

public boolean testTitle(String expected) {
  String actual = getPageTitle();
  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  return actual.equals(expected);
}

public void takeScreenshot(ITestResult result) {
  System.out.println("<---- Taking Screenshot ---->");
  Reporter.log("<---- Taking Screenshot ---->");
  driver = new Augmenter().augment(driver);
  Calendar calendar = Calendar.getInstance();
  SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
  String methodName = result.getName();
  File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
  String destFile = "target/surefire-reports/screenshots/"+methodName+"_"+ formater.format(calendar.getTime())+".png";
  try {
    FileUtils.copyFile(srcFile, new File(destFile));
    Reporter.log("Saved <a href=../screenshots/" + destFile + ">Screenshot</a>");
    // FileUtils.copyFile(scrFile, new File((String) PathConverter.convert("failure_screenshots/"+methodName+"_"+formater.format(calendar.getTime())+".png")));
    // FileUtils.copyFile(scrFile, new File((String) PathConverter.convert("failure_screenshots/"+methodName+".png")));
  } catch (IOException e) {
    e.printStackTrace();
  }
}

public void report(String string) {
  Reporter.log(string);
}

}
