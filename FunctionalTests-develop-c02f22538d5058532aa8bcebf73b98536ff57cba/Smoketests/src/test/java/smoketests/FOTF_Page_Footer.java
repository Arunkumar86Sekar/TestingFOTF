package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Page_Footer {
  public String title;
  private FOTF_UtilityFunctions testPage;
  private String _baseUrl;
  private WebDriver driver;
  private WebDriverWait wait;
  public List linkList;
  public Nav newsletter;

  public FOTF_Page_Footer (WebDriver _driver, String baseUrl) throws Exception {
    _baseUrl = baseUrl;
    driver = _driver;
    testPage = new FOTF_UtilityFunctions(driver);
    linkList = getFooterLinks();
    newsletter = getNewsletter();
  }

  public List getFooterLinks() {
    List<Nav> finalList = new ArrayList();
    List<WebElement> elementList = new ArrayList();
    testPage.open(_baseUrl);
    String footerXpath = "//footer[@class='site--footer']";
    //get secondary nav bar values
    elementList = testPage.getAllElements(footerXpath + "//a");

    for (WebElement element : elementList) {
      Nav nav = new Nav();
      nav.name = testPage.getText(element);
      nav.url = testPage.getAttribute(element,"href");
      finalList.add(nav);
      }

      return finalList;
    }

    public Nav getNewsletter() {
      Nav nav = new Nav();
      String xpath = "//form[@class='newsletter--form']";
      nav.url = testPage.getAttribute(xpath, "action");
      return nav;
    }


}
