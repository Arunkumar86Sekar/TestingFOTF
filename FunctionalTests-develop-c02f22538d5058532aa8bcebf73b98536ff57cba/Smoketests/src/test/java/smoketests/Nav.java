package smoketests;

import org.openqa.selenium.*;

public class Nav {
  public String name;
  public String url;
  public WebElement element;


  public WebElement loadElement(String xpath, WebDriver driver) {
    return driver.findElement(By.xpath(xpath));
  }
}
