//controller
package smoketests;

import org.testng.annotations.*;

@Listeners(ScreenShotOnFailure.class)

public class FOTF_MediaCenterTest {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private MediaCenterTests test;
  private Browserstack browserstack;


  @BeforeClass(alwaysRun = true)
  @Parameters(value={"browser","browser_version","platform", "os", "os_version", "device"})
  public void setup(String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception {
    System.out.println("Setting up Environment");

    // while (baseUrl.substring(baseUrl.length() -1).equals("/")) {
    //     System.out.println("Removing --> / " + baseUrl);
    //     baseUrl = baseUrl.substring(0 , baseUrl.length() -1);
    //   }
    // baseUrl = baseUrl + "/";

    System.out.println("\n<---------------Media Center Tests--------------->");
    System.out.println(baseUrl);

    browserstack = new Browserstack(browser, browser_version, platform, os, os_version, device);

    test = new MediaCenterTests(baseUrl,browserstack.driver);
  }

  @AfterClass
  public void tearDown() throws Exception {
    browserstack.driver.quit();
  }

  @Test(priority = 0)
  public void openDailyBroadcast() throws Exception{
    test.openHomePage();
    test.testTitle();
  }

  @Test
  public void testDailyBroadcastHeader() throws Exception{
    test.testHeaderText();
    test.testHeaderLinks();
  }

  @Test
  public void testSearch() throws Exception {
    test.testEpisodeSearch();
  }

  @Test
  public void testLastestEpisode() throws Exception {
    test.testLastestEpisodePresent();
  }

  @Test
  public void testUpcomingEpisodes() throws Exception {
    test.testUpcomingEpisodesPresent();
  }

  @Test
  public void testDailyBroadcastAside() throws Exception {
    test.testAside();
  }
  @Test
  public void testBroadcastPage() throws Exception {
    test.broadcastPage();
  }
  @Test
  public void testBonusPage() throws Exception {
    test.bonusPage();
  }


  // @Test
  // public void testMediaPlayer() throws Exception {
  //   String xpath = "//div[@class='episode--media_player']";
  //   test.testElementPresent("MediaPlayer",xpath);
  // }

}
