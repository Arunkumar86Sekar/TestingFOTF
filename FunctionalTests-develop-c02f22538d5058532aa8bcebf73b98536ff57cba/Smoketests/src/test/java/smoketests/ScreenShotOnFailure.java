package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.testng.annotations.*;
import org.testng.ITestResult;
import org.testng.Reporter;
import java.text.SimpleDateFormat;
import org.testng.TestListenerAdapter;
import org.testng.ITestResult;

import smoketests.Browserstack;

public class ScreenShotOnFailure extends TestListenerAdapter{
  WebDriver driver;


  @Override
  public void onTestFailure(ITestResult result) {
    String methodName = result.getName();
    takeScreenshot(methodName);
  }

  public void takeScreenshot(String methodName) {
    driver = Browserstack.getDriver();
    System.out.println("<---- Taking Screenshot of:" + methodName + " ---->");

    driver = new Augmenter().augment(driver);
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
    File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    String nameFile = methodName+"_"+ formater.format(calendar.getTime())+".png";
    String destFile = "target/surefire-reports/screenshots/";
    try {
      FileUtils.copyFile(srcFile, new File(destFile + nameFile));
      Reporter.log("<br> <img src=screenshots/" + nameFile + "/> <br>");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
