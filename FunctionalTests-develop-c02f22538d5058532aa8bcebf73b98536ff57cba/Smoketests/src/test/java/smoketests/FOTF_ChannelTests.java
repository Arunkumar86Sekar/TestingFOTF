package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_ChannelTests {
  private FOTF_UtilityFunctions testPage;
  private FOTF_Page_Channel channel;
  private String _baseUrl;
  private String baseTitle;
  // private Browserstack browserstack;
  private List<Channel> channels;
  private FOTF_Page_Header header;
  private FOTF_Page_Footer footer;

  public FOTF_ChannelTests(String baseUrl, WebDriver _driver ) throws Exception {
    baseTitle = " | Focus on the Family";
    _baseUrl = baseUrl;
    testPage = new FOTF_UtilityFunctions(_driver);
    channel = new FOTF_Page_Channel(_driver, _baseUrl);
    header = new FOTF_Page_Header(_driver, _baseUrl);
    footer = new FOTF_Page_Footer(_driver, _baseUrl);
    channels = channel.channelList;

  }

  public void quitTest() throws Exception{
    testPage.quit();
 }

 public void testTitles() {
   System.out.println("<---- Testing Channel Titles ---->");
   String[] expectedTitles = {"Marriage" +baseTitle, "Parenting" +baseTitle,"Life Challenges" +baseTitle, "Faith"+ baseTitle, "Social Issues" +baseTitle, "Pro-Life" + baseTitle};

  for (int i = 0; i < expectedTitles.length; i++) {
    String actualTitle = channels.get(i).title;
    System.out.println("Expected ->" + expectedTitles[i].toLowerCase() + "\nActual --->" + actualTitle.toLowerCase());
    assertEquals(expectedTitles[i].toLowerCase(), actualTitle.toLowerCase());
  }

}

public void testFeaturedArticle() {
  System.out.println("<---Testing: Featured Articles---> ");

  for (Channel channel : channels) {
    System.out.println("Opening " + channel.title + ": ->" +  channel.url);
    testPage.open(channel.url);
    System.out.println("Testing " + channel.title +  " -> Header Text");
    assertTrue(testPage.isTextPresent(channel.featured.header));
    // System.out.print("Testing " + channel.title +  " -> Image Present = ");
    // assertTrue(channel.featured.imagePresent);
    System.out.println(channel.featured.imagePresent);
    System.out.println("Testing " + channel.title +  " -> Text");
    assertTrue(testPage.isTextPresent(channel.featured.text));
    System.out.println("Testing " + channel.title +  " -> Link");
    testPage.open(channel.featured.link);


  }
}

public void testRelatedArticles() throws Exception {
  System.out.println("<---Testing: Related Articles---> ");

  for (Channel channel : channels) {
    List<Link> relatedArticles = channel.related;
    for(Link article : relatedArticles) {
      String url = article.link;
      int statusCode = testPage.getResponseCode(url);
      System.out.println(url + " Status Code --> " + statusCode);
      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + url);
      }
      assertTrue(statusCode != 404);
    }
  }
}

public void testRecentArticles() throws Exception {
  System.out.println("<---Testing: Recent Articles---> ");
  for (Channel channel : channels) {
    System.out.println("\n<----Testing " + channel.url + " Channel: Recent Articles-->");
    List<Article> recentArticles = channel.recent;
    for (Article article : recentArticles) {
      assertTrue(testPage.isTextPresent(article.header));
      // assertTrue(testPage.isTextPresent(article.text));
      // testPage.open(article.link);
      String url = article.link;
      int statusCode = testPage.getResponseCode(url);
      System.out.println(article.header + ": Link Status Code --> " + statusCode);
      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + url);
      }
      assertTrue(statusCode != 404);
    }
  }
}

public void testChannelList() throws Exception {
  System.out.println("<---Testing: Channel List--->");
  for (Channel channel: channels) {
    System.out.println("\n<----Testing " + channel.url + " Channel: Channel List-->");
    List<Article> list = channel.list;
    for (Article article : list) {
      assertTrue(testPage.isTextPresent(article.header));
      assertTrue(testPage.isTextPresent(article.text));
      // testPage.open(article.link);
      String url = article.link;
      int statusCode = testPage.getResponseCode(url);
      System.out.println(article.header + ": Link Status Code --> " + statusCode);
      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + url);
      }
      assertTrue(statusCode != 404);

    }
  }
}

public void testTopics() throws Exception {
  System.out.println("<---Testing: Channel Topics---> ");

  //toggles -  navigate to topic and check title
  boolean testTitles = false;

  for (Channel channel : channels) {
    System.out.println("\n<----Testing " + channel.url + " Channel: Topics-->");
    List<Link> topics = channel.topics;
    for(Link topic : topics) {
      String title = topic.name;
      String url = topic.link;
      int statusCode = testPage.getResponseCode(url);
      System.out.println(url + " Status Code --> " + statusCode);
      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + url);
        }
      assertTrue(statusCode != 404);
      //open page and test header
      if (testTitles) {
        testPage.open(url);
        String actual = testPage.getText("//header[@class='channel_header']/div/h1");
        System.out.println("Expected Topic Title ->" + title + "\nActual Topic Title --->" + actual);
        assertEquals(actual, title);
      }
    }
  }
}

public void testHeaderSecondaryNav() throws Exception{
  testPage.open(_baseUrl);
  System.out.println("<---- Testing: secondary Navbar ---->");
  List<Nav> navList = header.secondaryNavList;
  String[] expectedNames = {"Today's Broadcast", "Family Help", "About", "Store"};
  int i = 0;

  //test link names
  for (Nav nav : navList) {
    String expectedName = expectedNames[i];
    System.out.println("Expected ->" + expectedName.toLowerCase() + "\nActual --->" + nav.name.toLowerCase());
    assertEquals(expectedName.toLowerCase(), nav.name.toLowerCase());

    //test link
    int statusCode = testPage.getResponseCode(nav.url);
    System.out.println(nav.url + " Status Code --> " + statusCode);
    if (statusCode == 404) {
      System.out.println("Broken of Link--> " + nav.url);
      }
    assertTrue(statusCode != 404);

    i++;
  }
}

public void testHeaderLogos() {
  //tests fotf logo and sub title
  System.out.println("<----Testing: Header Logos---->");
  String expectedLogoName = "Focus on the Family";
  String actualLogoName = header.logo.name;
  String expectedLogoUrl = "http://www.focusonthefamily.com";
  String actualLogoUrl = header.logo.url;
  String expectedSubTitleName = "Helping Families Thrive™";

  assertTrue(testPage.testTrue(expectedLogoName, actualLogoName));
  assertTrue(testPage.testContains(actualLogoUrl, expectedLogoUrl));

  testPage.open(header.logo.url);
  assertTrue(testPage.testTrue("Focus on the Family: Helping Families Thrive", testPage.getPageTitle()));
}

// public void testHeaderDonate() {
//   System.out.println("<---- Testing: Header Donate Button ---->");
//   List<URLString> donateButtons = header.donate;
//   for (URLString button : donateButtons) {
//     System.out.println("visitorid = " + button.visitorid + " refcd = " + button.refcd);
//     assertTrue(button.refcd != null);
//     assertTrue(button.visitorid != null);
//   }
// }

public void testFooterLinks() throws Exception{
  System.out.println("<---- Testing: Footer Links");
  List<Nav> links = footer.linkList;
  for (Nav link : links) {
    //skip google plus
    if(!link.url.equals("https://plus.google.com/+focusonthefamily/videos")) {
      //test link
      int statusCode = testPage.getResponseCode(link.url);
      System.out.println(link.name + " Status Code --> " + statusCode);
      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + link.url);
        }
      assertTrue(statusCode != 404);
    }
  }
}

public void testFooterNewsletterLink() {
  System.out.println("<---- Testing: Newletter URL ---->");
  String expected = "https://connect.focusonthefamily.com/aprimo/AudienceUpdate.aspx?D=b9ca57b2fbe8cb42458807853387983f6a0f6be5ccdab113&T=1&A=83f2ca9b0cad5323&F=d0f05528368ce09a&M=b333208886639fd5&G=951c0aad4f451d07ded428ddc25201eb&FromSite=FOTF";
  String actual = footer.newsletter.url;

  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  assertEquals(expected, actual);
}

public void testCampaign() {
  testPage.open(_baseUrl);
  String campaignXpath = "//div[@class='campaign--banner campaign--banner-variation banner--actions_visible']";
  assertTrue(testPage.isElementPresent(campaignXpath));
}

}
