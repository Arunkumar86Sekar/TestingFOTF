package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_AboutUs_Tests {
  private FOTF_UtilityFunctions testPage;
  private FOTF_Page_AboutUs aboutUs;
  private String _baseUrl;
  private String baseTitle;
  private List<Link> items;
  private List imagesStatusList;

  public FOTF_AboutUs_Tests(String baseUrl, WebDriver _driver ) throws Exception {
    baseTitle = " | Focus on the Family";
    _baseUrl = baseUrl + "/about";
    testPage = new FOTF_UtilityFunctions(_driver);
    aboutUs = new FOTF_Page_AboutUs(_driver, _baseUrl);
    items = aboutUs.featuredItemsList;
    imagesStatusList = aboutUs.images;

  }

  public void testFeaturedItems() throws Exception {
    System.out.println("<---Testing: Featured Items---> ");
    String[] expectedText = {"Our Vision", "Our Mission", "Our Values", "Our Beliefs"};

    for (int i = 0; i < expectedText.length; i++) {
      String actualText = items.get(i).name;
      System.out.println("Expected ->" + expectedText[i].toLowerCase() + "\nActual --->" + actualText.toLowerCase());

      int statusCode = testPage.getResponseCode(items.get(i).link);
      System.out.println(items.get(i).link + " Status Code --> " + statusCode);

      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + items.get(i).link);
      }
      assertTrue(statusCode != 404);
    }
  }

  public void testAllLinks() throws Exception {
    System.out.println("<---Testing: All Links--->");
    testPage.open(_baseUrl);
    List<WebElement> allLinks = aboutUs.getAllLinks();
    System.out.println("Total number of elements found -->" + allLinks.size());

    for(WebElement element : allLinks) {
      String localUrl = element.getAttribute("href");
      int statusCode = testPage.getResponseCode(localUrl);

      System.out.println(localUrl + " Status Code --> " + statusCode);


      if (statusCode == 404) {
        System.out.println("Broken of Link--> " + localUrl);
      }

      assertTrue(statusCode != 404);

  }
}

public void testAllImages() throws Exception {
  System.out.println("<---Testing: All Images--->");
  testPage.open(_baseUrl);
  List<WebElement> allImages = aboutUs.getAllImages();
  System.out.println("Total number of images found -->" + allImages.size());

  for(WebElement element : allImages) {
    boolean status = testPage.testImage(element);
    String statusCode = "ok";

    System.out.println("Image Status --> " + statusCode);
    if (!status) {
      statusCode = "failed";
      System.out.println("Image Status --> " + statusCode);
      System.out.println("<-----------------------Broken of Image----------------------->");
    }
    assertTrue(status);

  }

}

public void testTopics() throws Exception {
    System.out.println("\n<----Testing About Us: Topics-->");
    testPage.open(_baseUrl);
    List<Link> topics = aboutUs.topics;
    for(Link topic : topics) {
      String title = topic.name;
      String url = topic.link;

      //open page and test header and links
        testPage.open(url);
        String actual = testPage.getText("//header[@class='aboutus_header']/div/h1");
        System.out.println("Expected Topic Title ->" + title + "\nActual Topic Title --->" + actual);
        assertEquals(actual, title);

        System.out.println("<---Testing: All Links--->");
        List<WebElement> allLinks = aboutUs.getAllLinks();
        System.out.println("Total number of elements found -->" + allLinks.size());

        for(WebElement element : allLinks) {
          String localUrl = element.getAttribute("href");
          int statusCode = testPage.getResponseCode(localUrl);

          System.out.println(localUrl + " Status Code --> " + statusCode);


          if (statusCode == 404) {
            System.out.println("Broken of Link--> " + localUrl);
          }

          assertTrue(statusCode != 404);

      }

      System.out.println("<---Testing: All Images--->");
      List<WebElement> allImages = aboutUs.getAllImages();
      System.out.println("Total number of images found -->" + allImages.size());

      if (allImages.size() > 0){
        for(WebElement element : allImages) {
          boolean status = testPage.testImage(element);
          String statusCode = "ok";

          System.out.println("Image Status --> " + statusCode);
          if (!status) {
            statusCode = "failed";
            System.out.println("Image Status --> " + statusCode);
            System.out.println("<-----------------------Broken of Image----------------------->");
            }
            assertTrue(status);
          }
      }
    }
  }
}
