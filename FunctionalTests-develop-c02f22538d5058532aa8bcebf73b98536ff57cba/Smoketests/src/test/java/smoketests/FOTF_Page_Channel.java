package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Page_Channel {
  public String title;
  private FOTF_UtilityFunctions testPage;
  private String _baseUrl;
  private WebDriver driver;
  private WebDriverWait wait;
  public List channelList;
  private List navList;

  public FOTF_Page_Channel (WebDriver _driver, String baseUrl) throws Exception {
    _baseUrl = baseUrl;
    driver = _driver;
    testPage = new FOTF_UtilityFunctions(driver);
    channelList = getChannel();
  }

  public List getNavBar() {
  String xpath = "//nav[@id='main_nav']";
  String mainNavXpath = "/ul[2]";
  testPage.open(_baseUrl);
  List<WebElement> elementList = new ArrayList();
  List<Nav> finalList = new ArrayList();
  elementList = testPage.getAllElements(xpath + mainNavXpath + "/li/a");

  for (WebElement element : elementList) {
    Nav nav = new Nav();
    nav.name = testPage.getText(element);
    nav.url = testPage.getAttribute(element,"href");
    finalList.add(nav);
  }
  return finalList;
}

public List getChannel() {
  List<Channel> finalList = new ArrayList();
  List<Nav> navBar = getNavBar();
  String featuredXpath  = "//section[@class='channel_featured']";
  String relatedXpath = "//div[@class='channel_related']";
  String recentXpath = "//div[@class='channel_recent']/div";
  String channelListXpath = "//div[@class='channel_list']/div";
  String topicsXpath = "//*[@class='topic_list--item']/a";

  for (Nav page : navBar) {
    Channel channel = new Channel();
    channel.url = page.url;
    testPage.open(channel.url);
    channel.title = testPage.getPageTitle();
    System.out.println(page.name + ": Title = " + channel.title);
    channel.featured = new Article();
    channel.featured.header = testPage.getText(featuredXpath + "//header[@class='channel_featured--header']/h1/a");
    System.out.println(page.name + ": Featured Article Header = " + channel.featured.header);
    channel.featured.link = testPage.getAttribute(featuredXpath + "//header[@class='channel_featured--header']/h1/a", "href");
    System.out.println(page.name + ": Featured Article URL = " + channel.featured.link);
    channel.featured.text = testPage.getText(featuredXpath + "/p");
    System.out.println(page.name + ": Featured Article Text = " + channel.featured.text);
    // channel.featured.imagePresent = testPage.testImage(featuredXpath + "/div/a/img");
    channel.related = testPage.getLinks(relatedXpath + "/ul/li/a");
    channel.recent = testPage.getArticles(recentXpath, ".//h3/a", ".//a", ".//p[2]");
    channel.list = testPage.getArticles(channelListXpath, ".//h3/a", ".//a", ".//p[2]");
    channel.topics = testPage.getLinks(topicsXpath);

    finalList.add(channel);
  }
  return finalList;

}
}
