package smoketests;

import org.testng.annotations.*;

@Listeners(ScreenShotOnFailure.class)

public class FOTF_Channel_SmokeTest {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private FOTF_ChannelTests test;
  private boolean testCampaign = false;
  private Browserstack browserstack;


@BeforeClass(alwaysRun = true)
@Parameters(value={"browser","browser_version","platform", "os", "os_version", "device"})
public void setup(String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception {

  // while (baseUrl.substring(baseUrl.length() -1).equals("/")) {
  //     System.out.println("Removing --> / " + baseUrl);
  //     baseUrl = baseUrl.substring(0 , baseUrl.length() -1);
  //   }
  // baseUrl = baseUrl + "/";

  System.out.println("\n<---------------Channel Page Smoketest--------------->");
  System.out.println(baseUrl);

  browserstack = new Browserstack(browser, browser_version, platform, os, os_version, device);

  test = new FOTF_ChannelTests(baseUrl, browserstack.driver);
}


@AfterClass(alwaysRun = true)
public void tearDown() throws Exception {
  browserstack.driver.quit();
}

@Test
public void testTitles() throws Exception {
  test.testTitles();
}

@Test
public void testFeaturedArticle() throws Exception {
  test.testFeaturedArticle();
}

@Test
public void testRelatedArticles() throws Exception {
  test.testRelatedArticles();
}

@Test
public void testRecentArticles() throws Exception {
  test.testRecentArticles();
}

@Test
public void testChannelList() throws Exception {
  test.testChannelList();
}
@Test
public void testChannelTopics() throws Exception {
  test.testTopics();
}

@Test
public void testHeaderSecondaryNav() throws Exception {
  test.testHeaderSecondaryNav();
}
@Test
public void testHeaderLogos() throws Exception {
  test.testHeaderLogos();
  // test.testHeaderDonate();
}

@Test
public void testFooterLinks() throws Exception {
  test.testFooterLinks();
}

@Test
public void testFooterNewsletterLink() throws Exception {
  test.testFooterNewsletterLink();
}

@Test(priority = 0)
public void testCampaign() throws Exception {
  if (testCampaign) {
    System.out.println("\n<----------Test Campaign is set to true---------->\n");
    test.testCampaign();
  } else System.out.println("\n<----------Test Campaign is set to false---------->\n");
}

}
