package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Page_AboutUs {
  public String title;
  public List topics;
  public List images;
  public String featuredTitle;
  public String featuredText;
  public String ministriesTitle;
  public List ministries;
  public String programsTitle;
  public List programs;
  public List leadership;
  public String financials;
  public Link media;
  public List featuredItemsList;
  private FOTF_UtilityFunctions testPage;
  private String _baseUrl;
  private WebDriver driver;
  private WebDriverWait wait;

  public FOTF_Page_AboutUs (WebDriver _driver, String baseUrl) throws Exception {
    _baseUrl = baseUrl;
    driver = _driver;
    testPage = new FOTF_UtilityFunctions(driver);
    title = getTitle();
    topics = getTopics();
    images = getAllImages();
    featuredItemsList = getFeaturedItems();
  }

  private String getTitle(){
    testPage.open(_baseUrl);
    return testPage.getPageTitle();
  };

  private void getFeaturedTitle() {
    String xpath = "//div[@class='common--component_box']//h2";
  }

  public List getFeaturedItems() {
  String xpath = "//*[@class='aboutus_header_video_right--link_items']/li/a";
  testPage.open(_baseUrl);
  List<WebElement> elementList = new ArrayList();
  List<Link> finalList = new ArrayList();
  elementList = testPage.getAllElements(xpath);

  for (WebElement element : elementList) {
    Link item = new Link();
    item.name = testPage.getText(element);
    item.link = testPage.getAttribute(element,"href");
    finalList.add(item);
  }
  return finalList;
}

public List getAllLinks() {
  List<WebElement> linkList = new ArrayList();
  WebElement page = driver.findElement(By.xpath("//div[@class='site--main']"));
  linkList = testPage.findAllLinks(page);
  return linkList;
}

public List getAllImages() {
  String xpath = "//div[@class='site--main']";
  List<WebElement> imagesList = testPage.getImages(xpath);
  return imagesList;
}

public List getTopics() {
  String topicsXpath = "//*[@class='topic_list--item']/a";
  return testPage.getLinks(topicsXpath);
}

}
