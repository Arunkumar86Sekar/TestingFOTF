//This test checks for a functioning sitecore website.  It only checks for a
 // title, main page and six channel pages.
package smoketests;

import org.testng.annotations.*;
import org.testng.ITestResult;
import org.apache.commons.io.FileUtils;
import java.io.File;
import org.testng.Reporter;

@Listeners(ScreenShotOnFailure.class)

public class FOTF_Sitecore_SmokeTest {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private FOTF_Sitecore_Tests test;
  private Browserstack browserstack;
  private FOTF_UtilityFunctions testPage;

@BeforeClass(alwaysRun = true)
@Parameters(value={"browser","browser_version","platform", "os", "os_version", "device"})
public void setup(String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception {

  System.out.println("\n<---------------FOTF_Sitecore_SmokeTest--------------->");
  System.out.println(baseUrl);

  browserstack = new Browserstack(browser, browser_version, platform, os, os_version, device);

  test = new FOTF_Sitecore_Tests(baseUrl, browserstack.driver);
  testPage = new FOTF_UtilityFunctions(browserstack.driver);
}

@AfterClass(alwaysRun = true)
public void tearDown() throws Exception {
  browserstack.driver.quit();
}

@Test
public void login() {
  test.loginAndTestTile();
}
}
