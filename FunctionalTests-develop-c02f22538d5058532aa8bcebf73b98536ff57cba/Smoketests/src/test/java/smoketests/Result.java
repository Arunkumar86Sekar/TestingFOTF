package smoketests;

import org.openqa.selenium.*;

public class Result {
  public String title;
  public String guest;
  private String baseXpath = "//ol[@class='past_episodes--list']";


  public void load(int index, WebDriver driver){
    title = driver.findElement(By.xpath(baseXpath + "/li[" + index +"]/div/h3/a")).getText();
    guest = driver.findElement(By.xpath(baseXpath + "/li[" + index +"]/div/span")).getText();
  }
}
