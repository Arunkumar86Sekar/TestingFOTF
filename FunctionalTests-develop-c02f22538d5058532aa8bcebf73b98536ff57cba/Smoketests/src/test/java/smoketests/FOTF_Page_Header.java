package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Page_Header {
  public String title;
  private FOTF_UtilityFunctions testPage;
  private String _baseUrl;
  private WebDriver driver;
  private WebDriverWait wait;
  public List secondaryNavList;
  public Nav logo;
  public Nav subTitle;
  // public URLString donate;
  // public List donate;

  public FOTF_Page_Header (WebDriver _driver, String baseUrl) throws Exception {
    _baseUrl = baseUrl;
    driver = _driver;
    testPage = new FOTF_UtilityFunctions(driver);
    secondaryNavList = getHeader();
  }

  public List getHeader() {
    List<Nav> finalList = new ArrayList();
    List<WebElement> elementList = new ArrayList();
    String secondaryNavXpath = "//ul[@class='navigation--list secondary_nav--list']/li/a";
    String logoXpath = "//a[@class='site_header--title_link']";
    String donateXpath = "//a[@class='donation_message--button']";
    testPage.open(_baseUrl);
    //get secondary nav bar values
    elementList = testPage.getAllElements(secondaryNavXpath);

    for (WebElement element : elementList) {
      Nav nav = new Nav();
      nav.name = testPage.getText(element);
      nav.url = testPage.getAttribute(element,"href");
      finalList.add(nav);
      }

    logo = new Nav();
    logo.name  = testPage.getText(logoXpath);
    logo.url  = testPage.getAttribute(logoXpath, "href");

    // for (int i = 0; i < testPage.getNumElements("a tag", logoXpath); i++){
    //   URLString d = new URLString(testPage.getAttribute(donateXpath, "href"));
    //   System.out.println("Adding DonateButton -> visitorid = " + d.visitorid + " \n refcd = " + d.refcd);
    //   donate.add(d);
    // }

      return finalList;
    }


}
