package smoketests;

import org.openqa.selenium.*;

public class Article {
  public String header;
  public String link;
  public boolean imagePresent;
  public String text;
}
