package smoketests;

import org.testng.Assert;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;

public class FOTF_Environment_Tests {
  private FOTF_UtilityFunctions testPage;
  private String baseUrl;
  private String baseTitle;

  public FOTF_Environment_Tests(String _baseUrl, WebDriver _driver ) throws Exception {
    baseTitle = " | Focus on the Family";
    baseUrl = _baseUrl;
    testPage = new FOTF_UtilityFunctions(_driver);
  }

  public void quitTest() throws Exception{
    testPage.quit();
 }

 public void testChannelTitles() {
   System.out.println("<---- Testing Channel Titles ---->");
   String[] channels = {"Marriage", "Parenting","Life Challenges", "Faith", "Social Issues", "Pro-Life"};

  for (String channel:channels) {
    channel = testPage.simplify(channel);
    testPage.open(baseUrl + "/" + channel, baseUrl + "/" + channel);
    String expected  = testPage.simplify(channel + baseTitle);
    String actual = testPage.simplify(testPage.getPageTitle());
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    assertEquals(expected, actual);
  }
}
public void testMainPageTitle() {
  System.out.println("<---- Testing Main Page Title ---->");
  testPage.open(baseUrl, baseUrl);
  String expected = "Focus on the Family: Helping Families Thrive";
  String actual = testPage.getPageTitle();
  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  assertEquals(expected, actual);
}

public void testDailyBroadcastTitle() {
  System.out.println("<---- Testing Daily Broadcast ---->");
  testPage.open(baseUrl + "/media/daily-broadcast", baseUrl + "/media/daily-broadcast");
  String expected = "Focus on the Family Daily Broadcast | Focus on the Family";
  String actual = testPage.getPageTitle();
  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  assertEquals(expected, actual);
}

public void testAboutUsTitle() {
  System.out.println("<---- Testing About Us Title ---->");
  testPage.open(baseUrl + "/media/fotf/about", baseUrl + "/media/fotf/about");
  String expected = "About Focus on the Family | Focus on the Family";
  String actual = testPage.getPageTitle();
  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  assertEquals(expected, actual);
}
}
