package smoketests;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.*;
import java.net.URL;

public class Browserstack {
  public static WebDriver driver;
  private static final String username = System.getenv("BROWSERSTACK_USER");
  private static final String accessKey = System.getenv("BROWSERSTACK_ACCESSKEY");
  private String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
  private String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
  private static final String url = "https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub";
  String browser;
  String browser_version;
  String platform;
  String os;
  String os_version;
  String device;

  public Browserstack (String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception{
    this.browser = browser;
    this.browser_version = browser_version;
    this.platform = platform;
    this.os = os;
    this.os_version = os_version;
    this.device = device;

    System.out.println("Setting up Browserstack");

    //Sets browserStack local to true if it is null.  This allows the tests to be run on a local box.
    if (browserstackLocal == null) {
      browserstackLocal = "true";
    }
    System.out.println("Local testing = " + browserstackLocal);

    DesiredCapabilities capabilities = buildCapabilities();

    driver = new RemoteWebDriver(new URL(url),capabilities);

   }

   private DesiredCapabilities buildCapabilities() {
     DesiredCapabilities capability = new DesiredCapabilities();
     capability.setCapability("build", "FOTF_SmokeTest");
     capability.setCapability("browserstack.local", browserstackLocal);
     capability.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
     capability.setCapability("browserstack.debug", "true");
     capability.setCapability("platform",platform);
     capability.setCapability("os",os);
     capability.setCapability("os_version", os_version);
     capability.setCapability("browserName", browser);
     capability.setCapability("browserVersion", browser_version);
     capability.setCapability("device", device);

     return capability;
   }

   public static WebDriver getDriver() {
     return driver;
   }

}
