package smoketests;

import org.testng.annotations.*;

@Listeners(ScreenShotOnFailure.class)

public class FOTF_AboutUs_SmokeTest {
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private FOTF_AboutUs_Tests test;
  private Browserstack browserstack;

@BeforeClass(alwaysRun = true)
@Parameters(value={"browser","browser_version","platform", "os", "os_version", "device"})
public void setup(String browser, String browser_version, String platform, String os, String os_version, String device) throws Exception {

  // while (baseUrl.substring(baseUrl.length() -1).equals("/")) {
  //     System.out.println("Removing --> / " + baseUrl);
  //     baseUrl = baseUrl.substring(0 , baseUrl.length() -1);
  //   }
  // baseUrl = baseUrl + "/";

  System.out.println("\n<---------------FOTF_AboutUs_SmokeTest--------------->");
  System.out.println(baseUrl);

  browserstack = new Browserstack(browser, browser_version, platform, os, os_version, device);

  test = new FOTF_AboutUs_Tests(baseUrl, browserstack.driver);
}

@AfterClass(alwaysRun = true)
public void tearDown() throws Exception {
  browserstack.driver.quit();
}

@Test
public void testFeaturedItems() throws Exception {
  test.testFeaturedItems();
}

@Test
public void testAllLinks() throws Exception {
  test.testAllLinks();
}

@Test
public void testTopicPages() throws Exception {
  test.testTopics();
}
@Test
public void testImages() throws Exception {
  test.testAllImages();
}
}
