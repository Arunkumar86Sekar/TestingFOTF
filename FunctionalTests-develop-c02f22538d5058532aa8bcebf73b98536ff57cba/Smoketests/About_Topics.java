package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class AboutTopics {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.focusonthefamily.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAboutTopics() throws Exception {
    driver.get(baseUrl + "/about");
    driver.findElement(By.linkText("Foundational Values")).click();
    driver.findElement(By.linkText("Programs")).click();
    driver.findElement(By.linkText("Visit Us")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Focus Findings")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Newsroom")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Media")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Historical Timeline")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Contact Us")).click();
    driver.findElement(By.linkText("All Pages")).click();
    driver.findElement(By.linkText("Advertise With Us")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
