package smoketest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.*;

public class FotfTopicTest {
  private SiteTests test = new SiteTests();
  // public String defaultURL = "http://test.focusonthefamily.com";

  @BeforeMethod
@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
}

  @Test
  public void FotfTopicsTest() throws Exception {

    test.maximizeBrowser();
    test.closeBanner();

    String[] channelsList = {"Marriage", "Parenting", "Life Challenges", "Faith", "Social Issues", ""};

    for (String channel: channelsList) {

      String localXpath;

      //set Enviroment variables
      test.setEnvironmentVariables(channel);

      //start test
      String testType = "-Topic-";
      test.startTest(testType);

      // open page
      test.openHomepage();

      // close campaign banner
      test.closeBanner();

      //Preview list
      localXpath = "//div[@class='header_list--preview']/ul";

      // loop over topic elements and check links
      String buttonXpath = "//a[@class='channel--more_button js--expandable_trigger']";
      int numLinks = test.countElements(localXpath);

      test.testTopicLinks(localXpath, buttonXpath, numLinks);

      // loop over hidden elements and check links
      localXpath = "//*[@id='more_options']/ul";

      numLinks = test.countElements(localXpath);

      test.testTopicLinks(localXpath, buttonXpath, numLinks);

    }
}

  @AfterMethod
@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    System.out.println("Ending Topic Test");
    test.quitTest();
  }
}
