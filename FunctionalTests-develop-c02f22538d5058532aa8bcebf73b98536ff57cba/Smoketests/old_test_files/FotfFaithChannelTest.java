package smoketest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FotfFaithChannelTest {
  private WebDriver driver;
  private String baseUrl;
  private String channel;
  private String baseTitle;
  private String fotf;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeMethod
@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = System.getenv("TEST_SERVER_HOST");
    channel = "Faith";
    fotf = " | Focus on the Family";

    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testFotfChannelTest() throws Exception {
    driver.manage().window().maximize();
    String endChar = baseUrl.substring(baseUrl.length() -1);

    if (endChar.equals("/")) {
      System.out.println("Changing --> " + baseUrl);
      baseUrl = baseUrl.substring(0 , baseUrl.length() -1);
      System.out.println("to --------> " + baseUrl);
    }

    baseTitle = channel + fotf;

    String newString = channel.replaceAll(" ", "");
    newString = newString.toLowerCase();
    newString = "/" + newString;
    channel =  newString;

    String testType = "-Channel-";
    System.out.println("Starting " + testType + " test: " + baseUrl + ": " + baseTitle);

    // open page
    driver.get(baseUrl + channel);

    //Assert page title
    AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

    // Featured container
    System.out.println("<---Testing: Featured Article---> ");

    //Assert channel--lead element is present
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel--lead']")));

    //Assert channel featured element is present
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//section[@class='channel_featured']")));

    // Featured container -- link
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//header/h1/a")));
    String linkTitle = driver.findElement(By.xpath("//header/h1/a")).getText();

    // featured container --link ---check titles match
    driver.get(driver.findElement(By.xpath("//header/h1/a")).getAttribute("href"));
    System.out.println("Expected Title: " + linkTitle + fotf + " --- Actual Title: " + driver.getTitle());
    AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);
    //Return to home
    driver.get(baseUrl + channel);

    AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

    // Featured Related
    System.out.println("<---Testing: Featured -- Related Articles---> ");

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_related']")));
    AssertJUnit.assertEquals(driver.findElement(By.cssSelector("h3.channel_related--heading")).getText(), "Related");
    // Featured container -- Links
    // loop over elements and check links

    int numLinks = driver.findElements(By.xpath("//div[@class='channel_related']/ul/*")).size();

    System.out.println("This element has " + numLinks + "links");

    for (int i = 1; i <= numLinks; i++) {

      String localXpathLink = "//div[@class='channel_related']/ul/li[" + i + "]/a";
      String localXpath = "//div[@class='channel_related']/ul/li[" + i + "]";
      linkTitle = driver.findElement(By.xpath(localXpath)).getText();

      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

      // check for article type
      String articleType = driver.findElement(By.xpath(localXpath)).findElement(By.cssSelector("i")).getAttribute("title");
      String value = driver.findElement(By.xpath(localXpath)).getAttribute("innerHTML");
      String localLink = driver.findElement(By.xpath(localXpathLink)).getAttribute("href");

      boolean opensExternalLink = false;

      if (value.contains("target=")) {
        opensExternalLink = true;
      }

      if (opensExternalLink == false) {
        driver.get(localLink);
        // driver.findElement(By.xpath(localXpath)).click();
        System.out.println("Expected Title: " + linkTitle + fotf + " <---> Actual Title: " + driver.getTitle());
        AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);
        //Return
        driver.get(baseUrl + channel);
        AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

      } else System.out.println("Skipping external link: " + linkTitle);
    }
    // Recent Container
    System.out.println("<---Testing: Recent Articles ---> ");

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']")));
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[1]")));
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[2]")));

    // Recent Container --Left Box

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[1]/h3")));
    linkTitle = driver.findElement(By.xpath("//div[@class='channel_recent']/div[1]/h3")).getText();
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[1]/a")));
    driver.get(driver.findElement(By.xpath("//div[@class='channel_recent']/div[1]/a")).getAttribute("href"));
    System.out.println("Expected Title: " + linkTitle + fotf + " <---> Actual Title: " + driver.getTitle());
    AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);

    driver.get(baseUrl + channel);

    AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

    // Recent Container --Right Box

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[2]/h3")));
    linkTitle = driver.findElement(By.xpath("//div[@class='channel_recent']/div[2]/h3")).getText();
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_recent']/div[2]/a")));
    driver.get(driver.findElement(By.xpath("//div[@class='channel_recent']/div[2]/a")).getAttribute("href"));
    System.out.println("Expected Title: " + linkTitle + fotf + " <---> Actual Title: " + driver.getTitle());
    AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);

    driver.get(baseUrl + channel);

    driver.get(baseUrl + channel);

    AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

    // Middle Ad link not checked

    // Channel list
    System.out.println("<---Testing: Channel List ---> ");

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='channel_list']")));

    // loop over elements and check links
    numLinks = driver.findElements(By.xpath("//div[@class='channel_list']/*")).size();

    for (int i = 1; i <= numLinks; i++) {
      String localXpath = "//div[@class='channel_list']/div[" + i + "]/a";
      String localXpathArticleType = "//div[@class='channel_list']/div[" + i + "]/p[1]";
      linkTitle = driver.findElement(By.xpath("//div[@class='channel_list']/div[" + i + "]/h3")).getText();

      // check for article type
      String articleType = driver.findElement(By.xpath(localXpathArticleType)).getText();

      if (articleType.equals("ARTICLE")) {
        driver.get(driver.findElement(By.xpath(localXpath)).getAttribute("href"));
        System.out.println("Expected Title: " + linkTitle + fotf + " --- Actual Title: " + driver.getTitle());
        AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);

        driver.get(baseUrl + channel);

        AssertJUnit.assertEquals(driver.getTitle(), baseTitle);
      }
    }
    // //Pre-footer
    System.out.println("<---Testing: Pre-Footer ---> ");

    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='prefooter--container']")));

    // AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='prefooter--container']/div/h3[2]")));
    // //AssertJUnit.assertEquals(driver.findElement(By.xpath("//h3[2]")).getText(), "Topics");
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//div[@class='prefooter--container']/div/ul")));

    // loop over elements and check links
    numLinks = driver.findElements(By.xpath("//div[@class='prefooter--container']/div/ul/*")).size();

    for (int i = 1; i <= numLinks; i++) {

      String localXpath = "//div[@class='prefooter--container']/div/ul/li[" + i + "]/a";
      linkTitle = driver.findElement(By.xpath(localXpath)).getText();

      // check topic links
        AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
        driver.get(driver.findElement(By.xpath(localXpath)).getAttribute("href"));
        System.out.println("Expected Title: " + linkTitle + fotf + " <---> Actual Title: " + driver.getTitle());
        AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);
        AssertJUnit.assertEquals(driver.findElement(By.xpath("//article/header/div/h1")).getText(), linkTitle);
        driver.get(baseUrl + channel);

        AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

      }
      driver.close();
  }

  @AfterMethod
@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      Assert.fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
