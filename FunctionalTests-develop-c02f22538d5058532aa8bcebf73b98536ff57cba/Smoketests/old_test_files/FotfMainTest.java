package smoketest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.*;

public class FotfMainTest {
  private SiteTests test = new SiteTests();

  @BeforeMethod
@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    test.setWaits();
  }

  @Test
  public void testFotfMainTest() throws Exception {


    String testType = "<--Main Page-->";
    test.setEnvironmentVariables("Main Page");

    test.setMainPageVariables();

    test.startTest(testType);

    test.openMainPage();

    // test.checkDate("//div[@id='BlogSection']/div/div/div/div[2]/div[2]");

    test.testFooter("//*[@class='site--footer']");
    
    test.testHeader("//*[@class='site--header']");

    test.testSlideshow("//*[@id='SliderSection']");

    test.closeBanner();

    test.testChannelLinks();

    test.testTabbedPanel("//*[@id='ChannelSection']");

    test.testContactsBox("//div[@id='thrivingFamilyContainer']");

    test.testDonateBox("//div[@id='helpingFamily']");

    test.testTodaysBroadcast("//div[@id='todaysBroadcast']");

    test.testFreeResourceBox();

    test.testDalyBlog("//div[@id='BlogSection']");

    test.testFooterAd("//div[@id='footer-ad-container']/div/iframe");

    test.closeBrowser();
  }

  @AfterMethod
  @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
      test.quitTest();
    }
}
