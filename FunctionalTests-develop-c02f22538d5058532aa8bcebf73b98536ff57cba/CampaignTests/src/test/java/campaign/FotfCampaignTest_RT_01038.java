package campaign;

import org.testng.annotations.*;

//Verify query string has been developed to block the pop up from displaying for certain pages
public class FotfCampaignTest_RT_01038 {
  private CampaignTests test = new CampaignTests();
  String baseUrl;
  String username;
  String password;
  String testName;
  boolean query;
  String queryString = "?nosplash=1";
  String bannerPath = "//div[@class='banner--container']";


  @BeforeTest
  public void setUp() throws Exception {
    System.out.println("----------RT-01038: Verify query string has been developed to block the pop up from displaying for certain pages----------");
    baseUrl = test.setEnvironmentVariables();
    test.LogEntry("Testing -->" + baseUrl);
    username = "admin";
    password = "b";
    test.setWaits();
  }

  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @BeforeMethod
  public void methodSetup () {
    test.open(baseUrl);
  }


  @AfterMethod
  public void methodTearDown() {
    System.out.println("Removing Cookies-->");
    test.removeAllCookies();
  }

  @Test(priority=1)
  public void testBannerDisplays() throws Exception {
    query = test.doesElementExist(bannerPath);
    test.testTrue(query == true);
    System.out.println("Banner is not Displayed");
  }

  @Test
  public void testQueryString () throws Exception {
    test.open(baseUrl + queryString);
    query = test.doesElementExist(bannerPath);
    test.testTrue(query == false);
    System.out.println("Banner is not Displayed");
  }
}
