package campaign;

import org.testng.annotations.*;

//Verify VisitorID is passed to the Donation URL from the banner
public class FotfCampaignTest_RT_01056 {
  private CampaignTests test = new CampaignTests();

  @Test
  public void testFotfCampaign_RT_01056 () throws Exception {

    System.out.println("\n<----------RT-01056: Verify Visitor ID Cookie---------->");
    String baseUrl = test.setEnvironmentVariables();
    test.setWaits();
    test.open(baseUrl);
    test.removeAllCookies();
    test.open(baseUrl);

    String cookie = test.getVisitorIDCookieValue("SC_ANALYTICS_GLOBAL_COOKIE");
    test.clickButton("//div[@class='banner--actions_container']//button[1]");
    String visitor = test.getVisitorIDUrlValue();
    test.containsValue(visitor, cookie);

    test.quitTest();
  }
}
