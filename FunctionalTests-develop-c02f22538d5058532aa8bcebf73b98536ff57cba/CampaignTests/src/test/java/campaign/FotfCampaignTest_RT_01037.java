package campaign;

import org.testng.annotations.*;

// Regression Test 01037 --> Get banner values from Sitecore and check that they display correctly on the page
public class FotfCampaignTest_RT_01037 {
  private CampaignTests test = new CampaignTests();
  String baseUrl;
  String username;
  String password;
  int bannerValue;

  @AfterClass
  private void tearDown() throws Exception {
    test.quitTest();
  }

  @BeforeTest
  private void setUp() throws Exception {
    System.out.println("----------RT-01037: Verify Banner Logic and SiteCore Settings----------");
    test.setWaits();
    baseUrl = test.setEnvironmentVariables();
    username = "admin";
    password = "b";
    //Determine which banner is displayed
    bannerValue = test.whichBannerIsDisplayed(baseUrl);
    test.removeAllCookies();
  }

  @BeforeClass
  private void logIntoSiteCore() {
  test.openAndLogin(baseUrl + "/sitecore/login", username, password);

  //Navigate to SiteCore Campaign Settings
  test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
  test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
  test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
  test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
  test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]/../../img");
  test.openSitecoreFolder("Banner - " + bannerValue, "//a/span[contains(text(), '2016 Spring')]/../following-sibling::div/div[" + bannerValue + "]/a/span/img");
}

@Test
public void testCampiagn () throws Exception {
  //open SiteCore and login
  test.testCampiagnValues(baseUrl, bannerValue);
}

}
