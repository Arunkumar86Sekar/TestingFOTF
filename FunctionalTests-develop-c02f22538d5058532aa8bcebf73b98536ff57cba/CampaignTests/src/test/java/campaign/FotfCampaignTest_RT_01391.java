package campaign;

import org.testng.annotations.*;

public class FotfCampaignTest_RT_01391 {
  private CampaignTests test = new CampaignTests();

  @Test
  public void testFotfCampaign_RT_01391 () throws Exception {
    String username = "admin";
    String password = "b";

    System.out.println("\n<----------RT-01391: Verify all Start and End date of campaign settings function---------->");
    String baseUrl = test.setEnvironmentVariables();
    test.setWaits();

    //Navigate to SiteCore Campaign Settings
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]");

    //change date by -->
    int days = 4;

    String startDateXpath = "//*[contains(text(), 'Start Date:')]/following-sibling::div[2]//table[1]//input";
    String endDateXpath = "//*[contains(text(), 'End Date:')]/following-sibling::div[2]//table[1]//input";

    String orgStartDate = test.getAttribute(startDateXpath, "value");
    System.out.println("Original Start Date " + orgStartDate);

    String newStartDate = test.changeDate(-days);
    System.out.println("Changing Start Date to -->" + newStartDate);
    test.changeValue(startDateXpath, newStartDate);

    String orgEndDate = test.getAttribute(endDateXpath, "value");
    System.out.println("Original End Date " + orgEndDate);

    String newEndDate = test.changeDate(days);
    System.out.println("Changing End Date to --->" + newEndDate);
    test.changeValue(endDateXpath, newEndDate);

    test.siteCorePublishCampaign();

    test.open(baseUrl);

    System.out.println("Testing Banner-->");

    boolean bannerDisplays = test.doesElementExist("//div[@class = 'banner--container']");
    System.out.println("Banner Displays--?= " + bannerDisplays);

    //banner should display
    test.testTrue(bannerDisplays == true);

    //return to sitecore banner settings
    test.removeAllCookies();
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]");

    days = 10;

    newStartDate = test.changeDate(-days);
    System.out.println("Changing Start Date to -->" + newStartDate);
    test.changeValue(startDateXpath, newStartDate);

    days = -4;
    newEndDate = test.changeDate(days);
    System.out.println("Changing End Date to --->" + newEndDate);
    test.changeValue(endDateXpath, newEndDate);

    //publish changes
    test.siteCorePublishCampaign();

    test.open(baseUrl);

    bannerDisplays = test.doesElementExist("//div[@class = 'banner--container']");
    System.out.println("Banner Displays--?= " + bannerDisplays);

    //banner should not display
    test.testTrue(bannerDisplays == false);

    //return to sitecore banner settings
    test.removeAllCookies();
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]");

    System.out.println("Changing Start Date to -->" + orgStartDate);
    test.changeValue(startDateXpath, orgStartDate);

    System.out.println("Changing End Date to --->" + orgEndDate);
    test.changeValue(endDateXpath, orgEndDate);

    //publish changes
    test.siteCorePublishCampaign();

    test.quitTest();
  }
}
