package campaign;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import campaign.CampaignTests;


public class FotfSplashPageTest_Summer2017 {
  private CampaignTests test = new CampaignTests();
  FotfCampaignTest_Summer2016 splashData=new FotfCampaignTest_Summer2016();
  @BeforeClass
  public void setUp() throws Exception {
	 splashData.setUp();
	 splashData.loginToSiteCoreAndGetValues();
	 
  }

  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @AfterMethod
  private void methodSetup() {
    System.out.println("Removing Cookies-->");
    test.removeAllCookies();
  }

  //end Email tests

    //<----- Test Banner Display Text ----->
    @Test(priority = 1)
    private void testTextValues() {
      test.open(splashData.baseUrl);
      test.checkTextIngoreCaseAndBreaks("//div[@class='splash--content_container']//h2", splashData.shortDescription);
      test.checkTextIngoreCaseAndBreaks("//div[@class='splash--content_container']//h3[text()]", splashData.subtitle2);    
      test.checkTextIngoreCaseAndBreaks("//div[@class='splash--content_container']//p", splashData.subtitle3);
      test.checkTextIngoreCaseAndBreaks("//div[@class='splash--content_container']//a[text()]", splashData.donationButtonText);
      test.checkTextIngoreCaseAndBreaks("//div[@class='splash--content_container']//div[@class='splash--continue_container']/a[text()]", splashData.footerContinueText);
         }

   
    @Test(priority = 2)
    private void testSourceCode() throws Exception {
      System.out.println("Testing SourceCode");
      test.open(splashData.baseUrl);
      test.removeAllCookies();
      test.open(splashData.baseUrl);
      test.clickButton("//div[@class='splash--content_container']//a[text()]");
      test.urlContains(splashData.sourceCode);
    }

    @Test(priority = 3)
    private void testCampaignID() throws Exception {
      System.out.println("Testing CampaignID");
      test.open(splashData.baseUrl);
      test.removeAllCookies();
      test.open(splashData.baseUrl);
      test.clickButton("//div[@class='splash--content_container']//a[text()]");
      test.urlContains(splashData.campaignID.toLowerCase());
    }
    
    @Test(priority = 4)
    //logs into sitecore and gets required values
    private void testContinueToFocus() throws Exception {
  	  
    	 System.out.println("Testing Continue to the Focus On The Family");
         test.open(splashData.baseUrl);
         test.removeAllCookies();
         test.open(splashData.baseUrl);
         test.clickButton("//div[@class='splash--content_container']//div[@class='splash--continue_container']/a[text()]");
         test.urlContains(splashData.baseUrl);
         
    }
    // @Test(priority = 10)
    // private void testBannerLogic() throws Exception {
    //   //minimizedDonationAmounts
    //   String[] minimizedDonationNames = {"$50", "$100", "$150", "$300", "Other..."};
    //   String[] minimizedDonationAmounts = {"50", "100", "150", "300", "0"};
    //   for (int i = 0; i < minimizedDonationAmounts.length; i++) {
    //     int j = i + 1;
    //     System.out.println("Expected ->" + minimizedDonationAmounts[i]);
    //     String actualAmount = test.getAttribute("//div[@class='banner--minimized-column_secondary']//button["  + j + "]", "value");
    //     System.out.println("Actual --->" + actualAmount);
    //     // assertTrue(isElementPresent(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + i + "]")));
    //     test.testValues(actualAmount, minimizedDonationAmounts[i]);
    //     System.out.println("Expected ->" + minimizedDonationNames[i] + "\nActual --->" + test.getText("//div[@class='banner--minimized-column_secondary']//button["  + j + "]"));
    //     test.assertEquals(test.getText("//div[@class='banner--minimized-column_secondary']//button["  + j + "]"), minimizedDonationNames[i]);
    //   }
    // }

    



}
