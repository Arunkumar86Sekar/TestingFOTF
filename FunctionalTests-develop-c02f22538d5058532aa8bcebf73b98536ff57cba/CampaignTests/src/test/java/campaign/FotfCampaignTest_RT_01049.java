package campaign;

import org.testng.annotations.*;

// Regression Test 01049 --> Verify the 50/50 split traffic between banners
//Test fails if there is more than a 30% difference
public class FotfCampaignTest_RT_01049 {
  private CampaignTests test = new CampaignTests();

  @Test
  public void testFotfCampaign_RT_01049 () throws Exception {

    test.LogEntry("----------RT-01049: Verify the 50/50 split traffic calculations between banners----------");

    String baseUrl = test.setEnvironmentVariables();

    test.setWaits();

    int bannerValue;
    int bannerX = 0;
    int bannerY = 0;
    int count = 25;
    float percentageX;
    float percentageY;

    for (int i = 1; count >= i; i++) {
      test.removeAllCookies();
      bannerValue = test.whichBannerIsDisplayed(baseUrl);
      if (bannerValue == 1) {
        bannerX++;
        } else {
          if (bannerValue == 2)
            bannerY++;
          }
      int percent = (i * 100)/count;
      test.printProgBar(percent);
    }

    percentageX = (bannerX * 100.0f)/count;
    percentageY = (bannerY * 100.0f)/count;

    float diff = Math.abs(percentageX - percentageY);

    test.LogEntry("Percent Difference = " + diff + "%");

    test.testTrue(diff <= 30.0f);

    test.quitTest();
  }
}
