package campaign;

import org.testng.annotations.*;

public class FotfCampaignTest_RT_01030 {
  private CampaignTests test = new CampaignTests();
  String baseUrl;
  String username;
  String password;
  int initialBannerValue;
  String testName;

  String thankYouMessage;
  String remindMeCheckboxLabelText;
  String remindMeActionButtonText;
  String remindMeCheckboxValidationText;
  String emailAddressValidationText;
  String thankyouContinueText;
  String footerContinueText;
  String sourceCode;


  @BeforeTest
  public void setUp() throws Exception {
    testName= "----------RT-01030: Testing Campaign Banner----------";
    test.LogEntry(testName);
    baseUrl = test.setEnvironmentVariables();
    username = "admin";
    password = "b";
    test.setWaits();
    initialBannerValue = test.whichBannerIsDisplayed(baseUrl);
    test.removeAllCookies();
  }

  @AfterTest
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @AfterMethod
  private void methodSetup() {
    System.out.println("Removing Cookies-->");
    test.removeAllCookies();
  }

  @BeforeClass
  private void loginToSiteCoreAndGetValues() throws Exception {
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);
    //Navigate to SiteCore Campaign Settings
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]/../../img");
    test.openSitecoreFolder("Banner - " + initialBannerValue, "//a/span[contains(text(), '2016 Spring')]/../following-sibling::div/div[" + initialBannerValue + "]/a/span/img");
    //Get SiteCore Banner values
    thankYouMessage = test.getRTEvalue("Thank you Message:");
    remindMeCheckboxLabelText = test.getSiteCoreValue("Remind Me Checkbox Label Text:");
    remindMeActionButtonText = test.getSiteCoreValue("Remind Me Action Button Text:");
    remindMeCheckboxValidationText = test.getSiteCoreValue("Remind Me Checkbox Validation Text:");
    emailAddressValidationText = test.getSiteCoreValue("Email Address Validation Text:");
    thankyouContinueText = test.getSiteCoreValue("Thank you Continue Text:");
    footerContinueText = test.getSiteCoreValue("Footer Continue Text:");
    sourceCode = test.getSiteCoreValue("Source Code:");
    }

    @Test
    private void testEmailCase1() {
      test.LogEntry("invalid email, checked agreement");
      test.open(baseUrl);
      while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
        test.clearAndReload(baseUrl);
        }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.checkBox("//div[@class='banner--form-agree']/input");
      test.enterText("//input[@id='email']", "testing@internetcom");
      test.clickButton("//button[contains(text(),'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_email_validation_message']/span", emailAddressValidationText);
    }

    @Test
    private void testEmailCase2() {
      test.LogEntry("invalid email, unchecked agreement");
      test.open(baseUrl);
      while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
        test.clearAndReload(baseUrl);
      }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internetcom");
      test.uncheckBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_email_validation_message']/span", emailAddressValidationText);
    }

    @Test
    private void testEmailCase3() {
      test.LogEntry("valid email, unchecked agreement");
      test.open(baseUrl);
      while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
        test.clearAndReload(baseUrl);
      }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internet.com");
      test.uncheckBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_agree_validation_message']", remindMeCheckboxValidationText);
    }

    @Test
    private void testEmailCase4() {
      test.LogEntry("valid email, checked agreement");
      test.open(baseUrl);
      while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
        test.clearAndReload(baseUrl);
      }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internet.com");
      test.checkBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//p[@class='banner--confirmation_text']", thankYouMessage);
    }

    @Test
    private void testEmailCase5() {
      test.LogEntry("click YES");
      test.open(baseUrl);
      while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
        test.clearAndReload(baseUrl);
      }
      test.clickButton("//div[@class='banner--actions_container']//button[1]");
      test.urlContains("https://focusonthefamily.webconnex.com");
    }

}
