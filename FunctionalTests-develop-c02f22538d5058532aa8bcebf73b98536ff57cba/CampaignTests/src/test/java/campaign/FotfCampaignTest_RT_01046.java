package campaign;

import org.testng.annotations.*;

//Verify Source Code is passed to the Donation URL from the banner
public class FotfCampaignTest_RT_01046 {
  private CampaignTests test = new CampaignTests();
  String baseUrl;
  String username;
  String password;
  int bannerValue;
  String sourceCode;

  @BeforeTest
  private void setUp() throws Exception {
    System.out.println("<----------RT-01046: Donation Source Code---------->");
    test.setWaits();
    baseUrl = test.setEnvironmentVariables();
    username = "admin";
    password = "b";
    //Determine which banner is displayed
    bannerValue = test.whichBannerIsDisplayed(baseUrl);
    test.removeAllCookies();
  }

  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @BeforeClass
  private void loginToSiteCoreAndGetValues() throws Exception {
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);

    //Navigate to SiteCore Campaign Settings
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]/../../img");
    test.openSitecoreFolder("Banner - " + bannerValue, "//a/span[contains(text(), '2016 Spring')]/../following-sibling::div/div[" + bannerValue + "]/a/span/img");

    sourceCode = test.getSiteCoreValue("Source Code:");
  }
  @Test
  private void isTextBlank() {
    test.isNotBlank(sourceCode);
 }

  @Test
  private void testSourceCode() throws Exception {
    test.removeAllCookies();
    test.open(baseUrl);

    //reload until correct banner is displayed
    while (bannerValue != test.whichBannerIsDisplayed(baseUrl)) {
      test.clearAndReload(baseUrl);
    }

    //Test source code value
    test.clickButton("//div[@class='banner--actions_container']//button[1]");
    test.urlContains(sourceCode);
  }

}
