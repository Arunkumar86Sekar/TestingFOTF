package campaign;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import campaign.CampaignTests;


public class Donate_GiveMonthly {
	
	String buttonText;
	String buttonUrl;
	String sourceCode;
	String postBackID;
	String tagLine;
	  
  private CampaignTests test = new CampaignTests();
  
  FotfCampaignTest_Summer2016 splashData=new FotfCampaignTest_Summer2016();
  @BeforeClass
  public void setUp() throws Exception {
	  splashData.setUp();
	  System.out.println("--> Logging into SiteCore and getting values" );
	  
	 
  }

  public void getDonateGiveMonthly_Button(String donateType) throws Exception {
	  test.openAndLogin(splashData.baseUrl + "/sitecore/login", splashData.username, splashData.password);
	  test.searchItem(donateType);
	
	  buttonText=test.getTextBoxText("Button Text","text");
	  buttonUrl=test.getTextBoxText("Button URL","url");
	  sourceCode=test.getTextBoxText("Source Code","text");
	  postBackID=test.getTextBoxText("PostBack ID","text");
	  tagLine=test.getTextBoxText("Tagline","text");
	 
  }
  
  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @AfterMethod
  private void methodSetup() {
    System.out.println("Removing Cookies-->");
    test.removeAllCookies();
  }

  //end Email tests

    //<----- Test Banner Display Text ----->
    @Test(priority = 2)
    private void testGiveMonthly() throws Exception {
      getDonateGiveMonthly_Button("GiveMonthly");
      test.open(splashData.baseUrl);
      test.checkTextIngoreCaseAndBreaks("//a[@class='donation_message--button'][2]//span[@class='donation_message--title']", buttonText);
      test.UrlContains("//a[@class='donation_message--button'][2]", buttonUrl,"href");   
      test.UrlContains("//a[@class='donation_message--button'][2]", sourceCode,"href");
      test.UrlContains("//a[@class='donation_message--button'][2]", postBackID,"href");
      test.checkTextIngoreCaseAndBreaks("//a[@class='donation_message--button'][2]//span[@class='donation_message--text']", tagLine);
        }

    @Test(priority = 1)
    private void testDonateButton() throws Exception {
      getDonateGiveMonthly_Button("DonateButton");
      test.open(splashData.baseUrl);
      test.checkTextIngoreCaseAndBreaks("//a[@class='donation_message--button'][1]//span[@class='donation_message--title']", buttonText);
      test.UrlContains("//a[@class='donation_message--button'][1]", buttonUrl,"href");   
      test.UrlContains("//a[@class='donation_message--button'][1]", sourceCode,"href");
      test.UrlContains("//a[@class='donation_message--button'][1]", postBackID,"href");
      test.checkTextIngoreCaseAndBreaks("//a[@class='donation_message--button'][1]//span[@class='donation_message--text']", tagLine);
        }

}
