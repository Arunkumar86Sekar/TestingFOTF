package campaign;

import org.testng.annotations.*;

// RT-01048 --->Spring 2016 Campaign: Test Case: Days Hidden Cookie: Verify Cookie functionality for Campaign links
public class FotfCampaignTest_RT_01048 {
  private CampaignTests test = new CampaignTests();

  @Test
  public void testFotfCampaign_RT_01048 () throws Exception {
    String baseUrl = test.setEnvironmentVariables();
    String username = "admin";
    String password = "b";

    test.setWaits();

    System.out.println("\n<----------RT-01048: Days Hidden Cookie: Verify Cookie functionality for Campaign links---------->");

    test.openAndLogin(baseUrl + "/sitecore/login", username, password);

    //Navigate to SiteCore Campaign Settings
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]");

    //Get days hidden value in SiteCore
    int daysHiddenValue = Integer.parseInt(test.getSiteCoreValue("Days Hidden:"));
    System.out.println("Days Hidden Value = " + daysHiddenValue);

    test.open(baseUrl);

    //Determine which banner is displayed
    int bannerValue = test.whichBannerIsDisplayed(baseUrl);

    test.clickButton("//div[@class='banner--close-button js--banner_close']");
    test.open(baseUrl);

    //Cookie Name
    String defaultCookieName = "4bbd640b277b4ae8aa7c4b105216b4a6";
    String cookieName;

    if (bannerValue == 1) {
      cookieName = defaultCookieName + "BannerDisplay";
    }else {
      if (bannerValue == 2){
        cookieName = defaultCookieName + "BannerVariationDisplay";
      } else cookieName = null;
    }

    String cookie = test.getCookieAndConvertToString(cookieName);

    String actualcookieExpirationDate = test.getCookieAttribute(cookie,"expires=");
    actualcookieExpirationDate = actualcookieExpirationDate.substring(0, 16);
    String expectedCookieExpirationDate = test.changeDateAndFormat(daysHiddenValue);

    test.testValues(expectedCookieExpirationDate, actualcookieExpirationDate);

    test.quitTest();
  }
}
