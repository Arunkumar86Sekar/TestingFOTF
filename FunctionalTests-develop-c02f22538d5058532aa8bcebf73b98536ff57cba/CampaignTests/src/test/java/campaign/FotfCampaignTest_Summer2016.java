package campaign;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import campaign.CampaignTests;


public class FotfCampaignTest_Summer2016 {
  private CampaignTests test = new CampaignTests();
  
  String baseUrl;
  String sitecoreURL;
  String username;
  String password;
  int initialBannerValue;
  String testName;
  String campaignName = "2016 Summer - Banner";
  String campaignVersion = "Banner";
  String campaignID;//="BE8E838A-01F0-4DBD-961D-5F32E3413AE2";
  //ID of SiteCore item
  String itemID;// = "{3EA33A37-A382-4944-8FCD-9429A08E06AD}";

  //SiteCore values
  String title;
  String shortDescription;
  String subtitle2;
  String subtitle3;
  String subtitle3M;
  String thankYouMessage;
  String donationButtonText;
  String donationButtonSubText;

  String remindMeButtonText;
  String remindMeButtonSubText;
  String remindMeLabelText;

  String remindMeCheckboxLabelText;
  String remindMeActionButtonText;
  String remindMeCheckboxValidationText;

  String emailAddressValidationText;
  String thankyouContinueText;
  String footerContinueText;
  String sourceCode;
  
  String campaignImageUrl;
  String itemName;
  @BeforeClass
  public void setUp() throws Exception {
	  test.OpenBrowser();
    testName= "----------Testing " + campaignName + "----------";
    test.LogEntry(testName);
    baseUrl = test.setEnvironmentVariables();
    sitecoreURL = test.setSiteCoreVariables();
    username = test.getUsername();
    password = test.getPassword();
    campaignID=test.getCampaignID();
    itemID=test.getItemID();
    test.setWaits();
    // initialBannerValue = test.whichBannerIsDisplayed(baseUrl);
    test.removeAllCookies();
  }

  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

  @AfterMethod
  private void methodSetup() {
    System.out.println("Removing Cookies-->");
    test.removeAllCookies();
  }

  @Test(priority = 1)
  //logs into sitecore and gets required values
  public void loginToSiteCoreAndGetValues() throws Exception {
	  
    System.out.println("--> Logging into SiteCore and getting values");
    test.openAndLogin(baseUrl + "/sitecore/login", username, password);
    //Navigate to SiteCore Campaign Settings
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");

    test.enterText("//*[@id='TreeSearch']",itemID);
    test.clickButton("//*[@id='SearchPanel']/table[1]/tbody/tr/td[2]/div/div/a");
    test.clearText("//*[@id='TreeSearch']");

    // test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    // test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    // test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    // test.openSitecoreFolder(campaignName, "//a/span[contains(text(), '" + campaignName + "')]/../../img");
    // test.openSitecoreFolder(campaignName, "//a/span[contains(text(), '" + campaignName + "')]/../following-sibling::div/div[" + initialBannerValue + "]/a/span/img");
    //Get SiteCore Banner values
    title = test.getSiteCoreTitleValue("Title - 50-55 characters:");
    shortDescription = test.getRTEvalue("Short Description:");
    subtitle2 = test.getRTEvalue("Subtitle 2:");
    subtitle3 = test.getRTEvalue("Subtitle 3:");
    thankYouMessage = test.getRTEvalue("Thank you Message:");
    donationButtonText = test.getSiteCoreValue("Donation Button Text:");
    donationButtonSubText = test.getSiteCoreValue("Donation Button Sub Text:");
    remindMeButtonText = test.getSiteCoreValue("Remind Me Button Text:");
    remindMeButtonSubText = test.getSiteCoreValue("Reminder Me Button Sub Text:");
    remindMeLabelText = test.getSiteCoreValue("Remind Me Label Text:");
    remindMeCheckboxLabelText = test.getSiteCoreValue("Remind Me Checkbox Label Text:");
    remindMeActionButtonText = test.getSiteCoreValue("Remind Me Action Button Text:");
    remindMeCheckboxValidationText = test.getSiteCoreValue("Remind Me Checkbox Validation Text:");
    emailAddressValidationText = test.getSiteCoreValue("Email Address Validation Text:");
    thankyouContinueText = test.getSiteCoreValue("Thank you Continue Text:");
    footerContinueText = test.getSiteCoreValue("Footer Continue Text:");
    sourceCode = test.getSiteCoreValue("Source Code:");
    itemName=test.getText("//td[contains(text(),'Item name')]/following-sibling::td");
    
    test.enterText("//*[@id='TreeSearch']",campaignID);
    test.clickButton("//*[@id='SearchPanel']/table[1]/tbody/tr/td[2]/div/div/a");
    test.clearText("//*[@id='TreeSearch']");
    campaignImageUrl=test.getSiteCoreValue("Donation URL:");
    test.setCampaignVariation(itemName);
    }

    //Tests 2-5 checking email functionality
    @Test(priority = 2)
    private void testEmailCase1() {
      test.LogEntry("->Testing Email");
      test.LogEntry("invalid email, checked agreement");
      test.open(baseUrl);
      // while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
      //   test.clearAndReload(baseUrl);
      //   }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.checkBox("//div[@class='banner--form-agree']/input");
      test.enterText("//input[@id='email']", "testing@internetcom");
      test.clickButton("//button[contains(text(),'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_email_validation_message']/span", emailAddressValidationText);
    }

    @Test(priority = 3)
    private void testEmailCase2() {
      test.LogEntry("invalid email, unchecked agreement");
      test.open(baseUrl);
      // while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
      //   test.clearAndReload(baseUrl);
      // }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internetcom");
      test.uncheckBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_email_validation_message']/span", emailAddressValidationText);
    }

    @Test(priority = 4)
    private void testEmailCase3() {
      test.LogEntry("valid email, unchecked agreement");
      test.open(baseUrl);
      // while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
      //   test.clearAndReload(baseUrl);
      // }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internet.com");
      test.uncheckBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//div[@class='banner--validation-message js--banner_agree_validation_message']", remindMeCheckboxValidationText);
    }

    @Test(priority = 5)
    private void testEmailCase4() {
      test.LogEntry("valid email, checked agreement");
      test.open(baseUrl);
      // while (initialBannerValue != test.whichBannerIsDisplayed(baseUrl)) {
      //   test.clearAndReload(baseUrl);
      // }
      test.clickButton("//form//*[contains(text(), 'Remind')]");
      test.enterText("//input[@id='email']", "testing@internet.com");
      test.checkBox("//div[@class='banner--form-agree']/input");
      test.clickButton("//button[contains(.,'GO')]");
      test.checkText("//p[@class='banner--confirmation_text']", thankYouMessage);
    }

    //end Email tests

    //<----- Test Banner Display Text ----->
    @Test(priority = 6)
    private void testTextValues() {
      String actual;
      //Title
      test.checkTextIngoreCaseAndBreaks("//div[@class='banner--cause_container']/h2", title);
      // test.checkTextIngoreCaseAndBreaks("//div[@class='banner--cause_container']/p", shortDescription);

    }

    @Test(priority = 7)
    private void testSourceCode() throws Exception {
      System.out.println("Testing SourceCode");
      test.open(baseUrl);
      test.removeAllCookies();
      test.open(baseUrl);
      test.clickButton("//div[@class='banner--actions_container']//*[contains(text(), '$50')]");
      test.urlContains(sourceCode);
    }

    @Test(priority = 8)
    private void testCampaignID() throws Exception {
      System.out.println("Testing CampaignID");
      test.open(baseUrl);
      test.removeAllCookies();
      test.open(baseUrl);
      test.clickButton("//div[@class='banner--actions_container']//*[contains(text(), '$50')]");
      test.urlContains(campaignID);
    }
    
    @Test(priority = 9)
    //logs into sitecore and gets required values
    private void testCampaignImageUrl() throws Exception {
  	  
    	 System.out.println("Testing Campaign Image Url");
         test.open(baseUrl);
         test.removeAllCookies();
         test.open(baseUrl);
         test.UrlContains("//div[@class='banner--minimized-column_secondary']/form",campaignImageUrl,"action");
    }
    
    @Test(priority = 10)
    //Campaign Minimized functionality
    private void testCampaignMinimizeMore() throws Exception {
    	 String path1=campaignID.substring(1, campaignID.length()-1);
      	 campaignID=path1.replaceAll("-", "");
   	  	 campaignID=campaignID.toLowerCase();
    	 System.out.println("Testing Campaign Minimized and More functionality");
         test.open(baseUrl);
         test.removeAllCookies();
         test.open(baseUrl);
         test.clickButton("//div[@class='banner--close-button js--banner_close']");
         test.driver.get(baseUrl+"/marriage");
         if(test.isCookieEnabled(campaignID)){
        	 System.out.println("Test Failed");
        	 test.LogEntry("Cookie has not set for Minimizing the Campaign Banner");
         }else{
        	 
        	 test.LogEntry("Cookie has been set for Minimizing the Campaign Banner");
        	 System.out.println("Test Passed");
         }
         
         test.clickButton("//button[@class='banner--minimized-expand-button js--banner_expand']");
         test.driver.get(baseUrl+"/parenting");
         if(test.isCookieEnabled(campaignID)){
        	 test.LogEntry("Cookie has been removed for More button for the Campaign Banner");
        	 System.out.println("Test Passed");
         }else{
        	 System.out.println("Test Failed");
        	 test.LogEntry("Cookie has not removed for More button for the Campaign Banner");
         }
    }
    
    // @Test(priority = 10)
    // private void testBannerLogic() throws Exception {
    //   //minimizedDonationAmounts
    //   String[] minimizedDonationNames = {"$50", "$100", "$150", "$300", "Other..."};
    //   String[] minimizedDonationAmounts = {"50", "100", "150", "300", "0"};
    //   for (int i = 0; i < minimizedDonationAmounts.length; i++) {
    //     int j = i + 1;
    //     System.out.println("Expected ->" + minimizedDonationAmounts[i]);
    //     String actualAmount = test.getAttribute("//div[@class='banner--minimized-column_secondary']//button["  + j + "]", "value");
    //     System.out.println("Actual --->" + actualAmount);
    //     // assertTrue(isElementPresent(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + i + "]")));
    //     test.testValues(actualAmount, minimizedDonationAmounts[i]);
    //     System.out.println("Expected ->" + minimizedDonationNames[i] + "\nActual --->" + test.getText("//div[@class='banner--minimized-column_secondary']//button["  + j + "]"));
    //     test.assertEquals(test.getText("//div[@class='banner--minimized-column_secondary']//button["  + j + "]"), minimizedDonationNames[i]);
    //   }
    // }

    



}
