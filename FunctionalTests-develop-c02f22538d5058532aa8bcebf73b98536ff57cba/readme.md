# FOTF Functional Tests #
These tests are designed to be used as a way to maintain quality of our website. The \IDE folder is used to hold selenium tests, while the FOTF-MediaCenter is used to hold the Selenium Webkit tests.

## Smoketests ##
The \IDE folder also holds an HTML file that acts as a "Project file" for the Selenium tests. We currently have 1 project file, and it's called the SmokeTest.html. This file is what will be run as part of the automated tests to ensure that a certain level of quality is maintained throughout the development process.

## Configuring your machine to run Browserstack locally ##
### Environment Variables ###
You will need to setup a couple of environment variables to make sure that our scripts can get them. The environment variables are:

`BROWSERSTACK_ACCESSKEY=YourAccessKey`

`BROWSERSTACK_USER=YourUserName`

Both of these items can be found in your Account Settings from the BrowserStack website.

### Browserstack Local ###
You will also need to instal a small program from Browserstack called Browserstack local and it will have to be running in order for the scripts to work.

You will need to run this program from the location that you have BrowserStack local installed. For example:

`BrowserStackLocal --key YourAccessKey --enable-logging-for-api`

Again, *YourAccessKey* can be found in your Account Settings from the BrowserStack website.

### Running a Browserstack test locally ###
Finally, in order to run the tests, you will need to issue another command line command. Here's an example:

`mvn clean test -DsuiteXmlFile=YourTestFile`

Where *YourTestFile* is the name of an XML file that contains the tests that you want to run.


## Converting Selenium IDE tests to XUnit/WebDriver ##
Currently we haven't been able to find a reliable way to run Selenium IDE tests in Jenkins so we are converting them to XUnit/WebDriver for running in Jenkins. The Selenium IDE tool has an option to convert to NUnit tests. This gets a "close to working" .NET class that we can make a few tweaks to get working fully.

- Open the test case in Selenium IDE
- Select File -> Export Test Case As -> C# / NUnit / WebDriver
- Save the file somewhere on your computer
- Open the FOTF.FunctionalTests project and create a new class for your test case
- Make the class inherit BaseTestSuite
- Add a constructor of the following form to call the base class constructor: `public MyClass(BrowserFixture fixture) : base(fixture) { }`
- Copy the Test runner function (named "TheXXXXXXXTest" where XXXXXXX is the name of your test case in Selenium IDE) into the new class
- Add the attribute `[Fact]` above the function
- Change all calls to `Assert.AreEqual` to `Assert.Equal`
- Change all calls to `Assert.Fail("timeout")` to `Assert.True(false, "timeout")`
- Add the following namespace references
	using OpenQA.Selenium;
	using System;
	using System.Threading;
	using Xunit;
- Build and run all tests to confirm everything is working