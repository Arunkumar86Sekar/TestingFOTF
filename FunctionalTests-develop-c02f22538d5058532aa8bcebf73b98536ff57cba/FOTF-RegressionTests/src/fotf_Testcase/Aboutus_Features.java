package fotf_Testcase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class Aboutus_Features extends Basedriver{

    //SiteCore Login
	@BeforeMethod
	public static void LoginSitecore(){
		Driver.get(baseUrl);
	}    
	
	//Testcase for AboutUs_Article
	@Test
	public static void AboutUs_Article() throws InterruptedException{
			log.info("");
			log.info("__________________________________________________________________________________________________");
			log.info("*** TestCase - PBI: D-01210 - FOTF: About Us: Articles as Related Content Defect causing 404 ***");
			log.info("__________________________________________________________________________________________________");
			Navigate_About.AppendToNavigateUrl("/about/focus-findings/marriage/what-is-the-actual-divorce-rate");
			Operation.getSetLinkUrl(Driver);
			Opr_Sitecore_AboutUs_Assert.AssertAboutus("ArticleShareBar", "ContentEditor");
	}	
}
