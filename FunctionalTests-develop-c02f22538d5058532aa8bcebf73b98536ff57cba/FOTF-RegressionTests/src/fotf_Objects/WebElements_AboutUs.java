package fotf_Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_AboutUs {
	public WebElements_AboutUs(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	
	// Login Page for Sitecore Sandbox 
		@FindBy(xpath=".//*[@id='UserName']")
		public static WebElement aboutUs;
		
		@FindBy(xpath=".//*[@id='UserName']")
		public static WebElement aboutUs_Topic;
		
		@FindBy(xpath=".//*[@id='UserName']")
		public static WebElement aboutUs_Topic_Article;
}
