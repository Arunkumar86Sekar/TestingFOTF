package fotf_Datasource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_AboutUs_AssertElement;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class Opr_AboutUsDataSourceForChildItems_DataProvider extends Basedriver
{	
	static String[] searchArticle = { 
		dataSourceProvider.getProperty("SearchArticle1"), 
		dataSourceProvider.getProperty("SearchArticle2"), 
		dataSourceProvider.getProperty("SearchArticle3")
		};
	
	public static void SetDefaultValueToChildItemHashTable(Hashtable hashTable){
		hashTable.clear();		
		hashTable.put("Image", false);
		hashTable.put("Title", false);
		hashTable.put("ShortDescription", false);
		hashTable.put("MoreLink", false);
		hashTable.put("ItemList", false);
		hashTable.put("ImageShape", false);
	}	
	
	public static void CreateChildItemsWithFolder(String FolderName, String childLevel, String TemplateName) throws InterruptedException{
		Opr_ContentEditor_AboutUs.HomeFolderTemplates();							
		AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");			
		RM.SendKeys(dataSourceObj.childFolderName,dataSourceProvider.getProperty(FolderName));
		RM.clickAnElement(sitecoreObj.okButton);
		Thread.sleep(3000);
		Driver.switchTo().defaultContent();
		try{
			RM.clickAnElement(sitecoreObj.insertFromTemplate);
			RM.clickAnElement(sitecoreObj.insertFromTemplate);
			AboutusCommonClass.SwitchToFrame();	
			}catch(Exception e){AboutusCommonClass.SwitchToFrame();	}
		AboutusCommonClass.NavigateToCommonComponentTemplate("MetaData");
		if(TemplateName.equals("CarouselTemplate")){RM.clickAnElement(dataSourceObj.carouselItemTemplate);	}
		else if(TemplateName.equals("ImageAndLinkItem")){RM.clickAnElement(dataSourceObj.imageAndLinkItemTemplate);	}
		else{RM.clickAnElement(dataSourceObj.childItemTemplate);	}		
		sitecoreObj.itemName.clear();
		RM.SendKeys(sitecoreObj.itemName,dataSourceProvider.getProperty(childLevel)+"1");
		RM.clickAnElement(sitecoreObj.insertButton);
		Driver.switchTo().defaultContent();	
	}
	
	public static void ChildItemDataSource(String renderingDatasource, Hashtable childItemsHashTable) throws InterruptedException
	{
		RM.clickAnElement(dataSourceObj.commonComponentContentTab);
		if(childItemsHashTable.get("Image").equals(true)){
			RM.SendKeys(dataSourceObj.childItemImage,dataSourceProvider.getProperty("ChildItemImage"));
		}
		if(childItemsHashTable.get("Title").equals(true)){			
			RM.SendKeys(dataSourceObj.childItemTitle,"Child Item 1 - Title");		
		}
		if(childItemsHashTable.get("ShortDescription").equals(true)){
			try{
				AboutusCommonClass.UrlExternalLinkAndRichTextEditor_Click(dataSourceObj.commonComponentShowEditor);
				AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
				if(renderingDatasource.equals("4Items4ColumnsSquare")){					
					RM.SendKeys(dataSourceObj.commonComponentTextEditor,dataSourceProvider.getProperty("ChildItemShortDescription4Items4ColumnsSquare"));
				}else{ RM.SendKeys(dataSourceObj.commonComponentTextEditor,dataSourceProvider.getProperty("ChildItemShortDescription"));}
				
				RM.clickAnElement(dataSourceObj.itemTextAccept);
				Driver.switchTo().defaultContent();
			}catch(Exception e){
				Driver.switchTo().defaultContent();
				log.error("ChildItem Short Description - " + e.getLocalizedMessage());
			}
		}
		
		if(childItemsHashTable.get("MoreLink").equals(true)){
			try{				
				AboutusCommonClass.UrlExternalLinkAndRichTextEditor_Click(dataSourceObj.childComponentItemMoreLink);
				AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
				
				RM.SendKeys(dataSourceObj.moreInfoLinkDesc,dataSourceProvider.getProperty("ChildItemMoreInfoLinkDesc"));
				RM.SendKeys(dataSourceObj.moreInfoLinkURL,dataSourceProvider.getProperty("ChildItemMoreInfoLinkUrl"));
				RM.clickAnElement(dataSourceObj.moreInfoLinkOK);
				Driver.switchTo().defaultContent();
			}catch(Exception e){
				Driver.switchTo().defaultContent();
				log.error("ChildItem Datasource MoreLink - " + e.getLocalizedMessage());
			}
		}
		
		if(childItemsHashTable.get("ImageShape").equals(true)){
			ChildComponentItemImageShape(renderingDatasource, "DataSource", 0);
		}
		RM.clickAnElement(dataSourceObj.saveDataSource);
	}
	
	public static void ChildComponentItemImageShape(String renderingDatasource, String mode, int imageShape) 
			throws InterruptedException{		
		if(mode.equals("ContentEditor") || mode.equals("")){
			RM.clickAnElement("//span[text()='" + renderingDatasource +"_DataSource']/parent::a/following-sibling::div/div/div[2]/div/a/span[text()='ChildItems-1']");
		}
		WebElement dropDownList1 = dataSourceObj.childComponentItemImageShape;
		Select drp1 = drp1= new Select(dropDownList1);					
		drp1.selectByIndex(imageShape);
		if(mode.equals("ContentEditor") || mode.equals("")){
			Opr_ContentEditor_AboutUs.HomeSave();
		}
		if(mode.equals("ContentEditor")){			
			RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
			RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
			Opr_ContentEditor_AboutUs.PublishPreview();
			Driver.switchTo().window(Opr_Sitecore_AboutUs_Assert.GetChildWindow("CHILD"));
		}
	}
	
	public static void DuplicatingChildItemDataSource(String renderingDatasource) throws InterruptedException
	{
		int numOfChildItemsToDuplicate = 3;
		switch(renderingDatasource)
		{			
			case "LeftImageList":				
			case "RightImageList":				
			case "IFrameRightImageList": break;
			case "SideBySide":				
			case "SideBySideCentered": numOfChildItemsToDuplicate = 3; break;
			case "3Items3Columns": 					
			case "StackedImageLeft": 				
			case "3ColumnsLinksOverlaid": 
			case "ShowHideBasic":
			case "ShowHideImageLeft": 
			case "ShowHideArticleGroupByYear":
			case "HeaderImageRight":
			case "HeaderVideoRight": numOfChildItemsToDuplicate = 5; break;
			case "4Items4ColumnsRectangle": 
			case "4ItemsWithInset":				
			case "4Items2Columns": 
			case "4Items4ColumnsSquare":
			case "6Items2Columns": numOfChildItemsToDuplicate = 6; break;
			case "5Column": 
			case "2x3": 
			case "6Items6Columns": numOfChildItemsToDuplicate = 7; break;
			case "6Items6ColumnsWithText": 
			case "6Items3Columns":
			case "TimeLineBasic": numOfChildItemsToDuplicate = 8; break;
			default: break;
		}
		for(int i = 2; i <= numOfChildItemsToDuplicate; i++){				
			Actions ref = new Actions(Driver);
			ref.contextClick(dataSourceObj.childItems_1).build().perform();
			RM.clickAnElement(dataSourceObj.duplicateChildItem);
			AboutusCommonClass.SwitchToFrame();
			RM.SendKeys(dataSourceObj.childFolderName,dataSourceProvider.getProperty("ChildItem") + i);
			RM.clickAnElement(sitecoreObj.okButton);
			Driver.switchTo().defaultContent();
		}			
	}
	
	public static void SelectChildItem_RenderingDatasource(String renderingDatasource) throws InterruptedException
	{
		Opr_ContentEditor_AboutUs.DoubleClickItem(RM.FindAnElement("//span[contains(text(),'" + renderingDatasource+"_DataSource" +"')]"));
		RM.clickAnElement("//span[contains(text(),'" + renderingDatasource+"_DataSource" +"')]");
		RM.clickAnElement("//span[text()='" + renderingDatasource+"_DataSource" +"']");
		int childItemsToDisplay = 2;		
		switch(renderingDatasource)
		{			
			case "LeftImageList":				
			case "RightImageList":				
			case "IFrameRightImageList": break;
			case "SideBySide":				
			case "SideBySideCentered": 	childItemsToDisplay = 2; break;
			case "3Items3Columns": 					
			case "StackedImageLeft": 				
			case "3ColumnsLinksOverlaid": 
			case "ShowHideBasic": 
			case "ShowHideImageLeft":
			case "ShowHideArticleGroupByYear":
			case "HeaderImageRight": 
			case "HeaderVideoRight": childItemsToDisplay = 4; break;
			case "4Items4ColumnsRectangle": 
			case "4ItemsWithInset":				
			case "4Items2Columns": 
			case "4Items4ColumnsSquare": childItemsToDisplay = 5; break;
			case "5Column": 
			case "2x3": 
			case "6Items2Columns":
			case "6Items6Columns": childItemsToDisplay = 6; break;
			case "6Items6ColumnsWithText": 
			case "6Items3Columns": 
			case "TimeLineBasic": childItemsToDisplay = 7; break;
			default: break;
		}
		
		for(int i = 1; i <= childItemsToDisplay; i++){
			WebElement item = RM.FindAnElement("//option[text()='" + dataSourceProvider.getProperty("ChildItem")+Integer.toString(i) +"']");
			if(i==1){
			RM.clickAnElement(item);}
			RM.clickAnElement(dataSourceObj.childrenItemsNext);
		}
		RM.clickAnElement(dataSourceObj.saveDataSource);
		Thread.sleep(2000);
	}
	
	// Select date for all the Marriage Article under three components for AutoAboutUs 	
	public static void ClonedArticlePublicationDate(String renderingDatasource, int articleStartPos, int articleCount) throws InterruptedException 
	{	
		String clonedArticle="//span[text()='" + renderingDatasource + "_DataSource']/parent::a/following-sibling::div/div[*]";
		for(int i=1; i<= articleCount; i++){
			RM.clickAnElement(clonedArticle.replace("*", Integer.toString(i)));
			Thread.sleep(2000);
			if(i <= 3 && renderingDatasource.equals("marriage")){
				AboutusCommonClass.clonedRecentArticleTitle[i] = dataSourceObj.recentArticleTitleText.getAttribute("value");
			}
			RM.clickAnElement(dataSourceObj.clearPublicationDate);
			RM.SendKeys(dataSourceObj.publicationDate,AboutusCommonClass.PublicationDate(articleStartPos + i- 2));
			IsImageNull();
			Opr_ContentEditor_AboutUs.HomeSave();
		}
	}	
	
	public static void IsImageNull(){
		WebElement articleImgElement=RM.FindAnElement("//div[contains(text(),'Image')]/following-sibling::div[2]/input");
		try{
		String articleImg=articleImgElement.getAttribute("value");
		}catch(Exception e)
		{	RM.SendKeys(articleImgElement,dataSourceProvider.getProperty("Image"));	}
	}
}