package fotf_Datasource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_AboutUs_AssertElement;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class Opr_AboutUsDataSourceForGrandChildItems_DataProvider extends Basedriver
{		
	public static void GrandChildItemsDataProvider(String renderingDatasource) throws InterruptedException{
		switch(renderingDatasource)
		{			
			case "4Items2Columns": 
				CreateGrandChildItemsToCommonComponentItem(renderingDatasource);
				break;			
			default: break;
		}		
	}	
	
	public static void CreateGrandChildItemsToCommonComponentItem(String renderingDatasource) throws InterruptedException{
		RM.clickAnElement("//span[text()='" + dataSourceProvider.getProperty("ChildItem") +"1']");
		Opr_AboutUsDataSourceForChildItems_DataProvider.CreateChildItemsWithFolder("GrandChildFolderName", "GrandChildItem", "ImageAndLinkItem");
		ImageAndLinkItemDataSource(renderingDatasource);
		DuplicatingGrandChildItemDataSource(renderingDatasource);
		SelectGrandChildItem_RenderingDatasource(renderingDatasource);		
	}
	
	public static void ImageAndLinkItemDataSource(String renderingDatasource) throws InterruptedException
	{
		RM.clickAnElement(dataSourceObj.commonComponentContentTab);
		RM.SendKeys(dataSourceObj.imageAndLinkTemplateItemImage,dataSourceProvider.getProperty("GrandChildItemImage"));		
		try{				
			AboutusCommonClass.UrlExternalLinkAndRichTextEditor_Click(dataSourceObj.imageAndLinkTemplateItemLinkText);
			AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
			
			RM.SendKeys(dataSourceObj.moreInfoLinkDesc,dataSourceProvider.getProperty("GrandChildItemMoreInfoLinkDesc"));
			RM.SendKeys(dataSourceObj.moreInfoLinkURL,dataSourceProvider.getProperty("GrandChildItemMoreInfoLinkUrl"));
			RM.clickAnElement(dataSourceObj.moreInfoLinkOK);
			Driver.switchTo().defaultContent();
		}catch(Exception e){
			Driver.switchTo().defaultContent();
			log.error("Grand ChildItem Datasource MoreLink - " + e.getLocalizedMessage());
		}
		
		RM.clickAnElement(dataSourceObj.saveDataSource);
	}
	
	public static void DuplicatingGrandChildItemDataSource(String renderingDatasource) throws InterruptedException
	{
		int numOfChildItemsToDuplicate = 3;		
		for(int i = 2; i <= numOfChildItemsToDuplicate; i++){				
			Actions ref = new Actions(Driver);
			ref.contextClick(dataSourceObj.grandChildItems_1).build().perform();
			RM.clickAnElement(dataSourceObj.duplicateChildItem);
			AboutusCommonClass.SwitchToFrame();
			RM.SendKeys(dataSourceObj.childFolderName,dataSourceProvider.getProperty("GrandChildItem") + i);
			RM.clickAnElement(sitecoreObj.okButton);
			Driver.switchTo().defaultContent();
		}		
	}
	
	public static void SelectGrandChildItem_RenderingDatasource(String renderingDatasource) throws InterruptedException
	{		
		RM.clickAnElement("//span[text()='" + dataSourceProvider.getProperty("ChildItem") +"1']");
		for(int i = 1; i <= 2; i++){
			Actions ref = new Actions(Driver);
			WebElement item = RM.FindAnElement("//option[text()='" + dataSourceProvider.getProperty("GrandChildItem")+Integer.toString(i) +"']");
			ref.doubleClick(item).build().perform();
		}
		RM.clickAnElement(dataSourceObj.saveDataSource);
	}
}