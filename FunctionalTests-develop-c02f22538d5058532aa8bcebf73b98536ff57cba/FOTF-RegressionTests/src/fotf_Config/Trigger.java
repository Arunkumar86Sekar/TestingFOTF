package fotf_Config;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Response;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import fotf_DDF.XmlUtil;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class Trigger extends XmlUtil{
	public static int Size;
	public static String[] store;
	public static String[] testCasesList;
	public static int testCasesSize;
	public static String[] browserList;
	public static int browserSize;
	public static WebDriver driver;
	
	public static String[] getCheckBoxParameter(String parameterName){
		String[] getTestcase = {System.getenv(parameterName)};
		
		List<String> items = Arrays.asList(getTestcase[0].split("\\s*,\\s*"));
		Size=items.size();
		store = new String[Size];
		
		for(int i=0; i<=Size-1; i++){
			store[i]=items.get(i);
			System.out.println(store[i]);
		}
		return store;
	}
	public static int getCheckBoxParameterCount(String parameterName){
		String[] getTestcase = {System.getenv(parameterName)};
		List<String> items = Arrays.asList(getTestcase[0].split("\\s*,\\s*"));
		 return Size=items.size();
	}
	public static void main(String args[]) throws Exception {
		testCasesList=getCheckBoxParameter("TestCases");
		testCasesSize=getCheckBoxParameterCount("TestCases");
		
		browserList=getCheckBoxParameter("Browsers");
		browserSize=getCheckBoxParameterCount("Browsers");
		
		createXml();
	}
	

}
