package fotf_Config;

import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import fotf_Datasource.Obj_Sitecore_AboutusDatasource;
import fotf_Navigation.AboutUsNavigation;
import fotf_Objects.Obj_Sitecore_AboutUs;
import fotf_Objects.Obj_Sitecore_AboutUs_Article;
import fotf_Operations.AboutUs_Operation;
import fotf_Operations.Opr_Sitecore_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class Basedriver {
public static WebDriver Driver=null;
public static Obj_Sitecore_AboutUs sitecoreObj;
public static Obj_Sitecore_AboutUs_Article sitecoreObj_Article;
public static Opr_Sitecore_AboutUs objSitecoreOperation;
public static Opr_Sitecore_AboutUs_Assert objSitecoreOperationAssert;
public static Obj_Sitecore_AboutusDatasource dataSourceObj;
public static AboutUsNavigation Navigate_About;
public static AboutUs_Operation Operation;
public static ReusableMethods RM;
public static Logger log = Logger.getLogger(Basedriver.class.getName());
public static Logger processLog = Logger.getLogger(Basedriver.class.getName());
public static FileInputStream file=null;
public static Properties expectedData=null;
public static Properties dataSourceProvider=null;
public static Properties customInitiativeDataSourceProvider=null;
static String testProp = "test.property";
public static String server;
public static String oS;
public static String browser;
public static String baseUrl;
public static final String USERNAME = "arunkumars4";
public static final String AUTOMATE_KEY = "pLzuqaZQucpg7wsnysvE";
public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
		
		
		@BeforeSuite
		public void environmentSetup() throws MalformedURLException{
			
			server = System.getenv("TEST_SERVER_HOST");
			oS = System.getenv("OS");
			baseUrl="http://"+server;
		}
		
		@Parameters({ "browser" })
		@BeforeTest
		public void Configuration(String browser) throws Exception {	
			this.browser=browser;
			
			DesiredCapabilities caps = new DesiredCapabilities();
		    caps.setCapability("browser", browser);
		    //caps.setCapability("browser_version", "47.0");
		    caps.setCapability("os", oS);
		    caps.setCapability("os_version", "XP");
		    caps.setCapability("browserstack.debug", "true");
		    caps.setCapability("browserstack.local", "true");
		    
		    Driver = new RemoteWebDriver(new java.net.URL(URL), caps);
		    
			/*if(browser.equalsIgnoreCase("Firefox")){
			 Driver=new FirefoxDriver();
			 log.info("***** Opening Firefox Browser *****");
			}
			else if(browser.equalsIgnoreCase("Chrome")){
				System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
				 Driver=new ChromeDriver();
				 log.info("***** Opening Chrome Browser *****");
				// processLog.info("Test Process");
			}
			else{
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				System.setProperty("webdriver.ie.driver", "Drivers\\IEDriverServer.exe");
				Driver=new InternetExplorerDriver(capabilities);	
				log.info("***** Opening IE Browser *****");
			}*/
			
			file=new FileInputStream(System.getProperty("user.dir")+"\\src\\fotf_DDF\\ExpectedData.properties");
			expectedData=new Properties();
			expectedData.load(file);
			
			file=new FileInputStream(System.getProperty("user.dir")+"\\src\\fotf_DDF\\AboutUs-DataSource.properties");
			dataSourceProvider=new Properties();
			dataSourceProvider.load(file);
			
			file=new FileInputStream(System.getProperty("user.dir")+"\\src\\fotf_DDF\\CustomInitiative-DataSource.properties");
			customInitiativeDataSourceProvider=new Properties();
			customInitiativeDataSourceProvider.load(file);
			
			DOMConfigurator.configure("log4j.xml");
		
			Driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			Driver.manage().window().maximize();
			
			sitecoreObj=new Obj_Sitecore_AboutUs(Driver);
			sitecoreObj_Article=new Obj_Sitecore_AboutUs_Article(Driver);
			objSitecoreOperation=new Opr_Sitecore_AboutUs();
			dataSourceObj= new Obj_Sitecore_AboutusDatasource();
			 Navigate_About=new AboutUsNavigation();
			 Operation= new AboutUs_Operation();
			RM=new ReusableMethods();
			log.info(" ");
			log.info("******************************************************* ");
			log.info("TestCase Execution Starts for FOTF Media Center Project");
			log.info("******************************************************* ");
			log.info("Tests are Running On "+browser +" Browser");
		}
			
			@AfterTest
			public static void closeDriver(){
				log.info(" ");
				log.info("TestCase Execution Finished for FOTF Media Center Project");
				log.info("Closing Driver");
				Driver.quit();		
			}
			
		}