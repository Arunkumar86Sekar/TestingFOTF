package fotf_Operations;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Datasource.Obj_Sitecore_AboutusDatasource;

public class Opr_ContentEditor_AboutUs extends Basedriver{
	// Navigate to Create Layout for AutoAboutUs 
		public static void createLayout(String renderingControl) throws InterruptedException{
			RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
			RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
			PresentationDetail();
			objSitecoreOperation.Steps_CreateCommonComponent(renderingControl);
			objSitecoreOperation.Steps_SelectCommonComponent(renderingControl);
			RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
			//Thread.sleep(2000);
			//log.info("Back to Auto AboutUs");
			PublishPreview();
		}	
		
	// Navigate to Create Layout for AutoAboutUs 
			public static void createCustomInitiativeLayout(String renderingControl) throws InterruptedException{
				RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
				RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
				PresentationDetail();
				objSitecoreOperation.Steps_CreateCommonComponent(renderingControl);
				objSitecoreOperation.Steps_SelectCommonComponent(renderingControl);
				RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
				//Thread.sleep(2000);
				//log.info("Back to Auto AboutUs");
				PublishPreview();
			}
		public static void PresentationDetail() throws InterruptedException{		
			//Thread.sleep(2000);
			RM.clickAnElement(sitecoreObj.presentation);
			//Thread.sleep(2000);
			RM.clickAnElement(sitecoreObj.details);
		}
		
		public static void AddCustomInitiativeOnChannelSecondaryItems(String renderingControl, String secondaryItems) throws InterruptedException{
			RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
			RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
			PresentationDetail();
			objSitecoreOperation.Steps_CreateCommonComponent(renderingControl);
			objSitecoreOperation.Steps_SelectCommonComponent(renderingControl);
			RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
			//Thread.sleep(1000);
			RM.clickAnElement(sitecoreObj.channelMarriage);
			//Thread.sleep(2000);
			if(!secondaryItems.equals("Topic")){
			//Thread.sleep(2000);
			RM.clickAnElement(dataSourceObj.commonComponentContentTab);
			//Thread.sleep(1000);
			RM.clickAnElement(dataSourceObj.SecondaryRefresh);
			//Thread.sleep(2000);
			RM.clickAnElement(dataSourceObj.autoCustomInitiativeOnSecondary);
			//Thread.sleep(2000);
			RM.clickAnElement(dataSourceObj.secondaryItemsNext);
			}else{
				RM.clickAnElement(Obj_Sitecore_AboutusDatasource.marriageOpenblock);
				//Thread.sleep(1000);
				RM.clickAnElement(Obj_Sitecore_AboutusDatasource.topicItem);
			}
			HomeSave();
			PublishPreview();
		}
		
		public static void AddCustomInitiativeOnFeaturedAndProductItems(String renderingControl, String automationItems, WebElement deSelect, WebElement refreshItem, WebElement nextItem) throws InterruptedException{
			//Thread.sleep(1000);
			RM.clickAnElement(sitecoreObj.channelMarriage);
			//Thread.sleep(2000);
			RM.clickAnElement(dataSourceObj.commonComponentContentTab);
			//Thread.sleep(1000);
			RM.clickAnElement(deSelect);
			RM.clickAnElement(refreshItem);
			//Thread.sleep(2000);
			RM.clickAnElement(automationItems);
			//Thread.sleep(2000);
			RM.clickAnElement(nextItem);
			HomeSave();
		}
		
		// Navigate to Create Layout for AutoAboutUs 
		public static void PublishPreview() throws InterruptedException{
			RM.clickAnElement(sitecoreObj.publishItem);
			//Thread.sleep(3000);
			//log.info("Clicked on Publish Item");
			RM.clickAnElement(sitecoreObj.previewMode);
			//log.info("Preview Mode opened for the AboutUs");
		}
		
		public static void HomeSave() throws InterruptedException{
			RM.clickAnElement(dataSourceObj.homeMenuStrip);
			//Thread.sleep(1000);
			RM.clickAnElement(dataSourceObj.saveDataSource);
			//Thread.sleep(2000);
			SaveAnyway();
			Thread.sleep(2000);
		}
		
		public static void SaveAnyway(){
			try{
				AboutusCommonClass.SwitchToFrame();
				RM.clickAnElement(Obj_Sitecore_AboutusDatasource.itemTextAccept);
				Driver.switchTo().defaultContent();
			}catch(org.openqa.selenium.NoSuchElementException e){
				Driver.switchTo().defaultContent();
			}catch(Exception e){
				Driver.switchTo().defaultContent();
			}
		}
		public static void HomeInsertTemplates() throws InterruptedException{
			RM.clickAnElement(dataSourceObj.homeMenuStrip);
			//Thread.sleep(1000);
			RM.clickAnElement(sitecoreObj.insertFromTemplate);
			//Thread.sleep(2000);
		}
		
		public static void HomeFolderTemplates() throws InterruptedException{
			RM.clickAnElement(dataSourceObj.homeMenuStrip);
			//Thread.sleep(1000);
			RM.clickAnElement(dataSourceObj.createFolder);
			//Thread.sleep(3000);
		}
		
		public static void RefreshItem(WebElement webElement) throws InterruptedException{
			//Thread.sleep(2000);			
			Actions ref=new Actions(Driver);
			ref.contextClick(webElement).build().perform();
			RM.clickAnElement(sitecoreObj.refreshItem);
			//Thread.sleep(2000);
		}
		
		public static void DoubleClickItem(WebElement webElement) throws InterruptedException{
			Thread.sleep(2000);			
			Actions ref=new Actions(Driver);
			ref.doubleClick(webElement).build().perform();
			Thread.sleep(2000);
		}
		
		// Navigate to Child window for AutoAboutUs Preview page
		public static String GetChildWindow(String mode){
			Set<String> window=Driver.getWindowHandles();
			Iterator<String> itr = window.iterator();
			String parentName = itr.next();
			
			if (mode.equals("PARENT")){		
				return parentName;
			}
			
			try{
				return itr.next();
				}
			catch(Exception e){
				System.out.println(e);
			}
			
			return "Dummy";
		}	
}
