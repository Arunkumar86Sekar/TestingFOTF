package fotf_Operations;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Config.ReusableMethods;
import fotf_Datasource.Opr_AboutUsDataSource_DataProvider;
import fotf_Datasource.Opr_Sitecore_AboutusDatasource;

public class Opr_Sitecore_AboutUs extends Basedriver{

	// Login to the Sitecore
	public static void loginSitecore(){
		// Verify The Content Editor is Selected or not in Sitecore login page
			sitecoreObj.userName.clear();
			sitecoreObj.passWord.clear();
			
			RM.SendKeys(sitecoreObj.userName,AboutusCommonClass.SitecoreUserName());
			RM.SendKeys(sitecoreObj.passWord,AboutusCommonClass.SitecorePassword());			
			//sitecoreObj.loginButtion);
			RM.clickAnElement(sitecoreObj.loginButtion);
			
			RM.clickAnElement(sitecoreObj.contentEditor);
		}
	
	// Navigate to FOTF content of the Sitecore
	public static void navigateToFOTFcom() throws InterruptedException{
		// Verify The "Content" is Displayed or not under the Sitecore content
		try{
			dataSourceObj.contentExtract.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(dataSourceObj.sitecoreExtract);
		}
		
		try{
			dataSourceObj.fotfCom.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(dataSourceObj.contentExtract);
		}
		
		try{
			dataSourceObj.fotfComFirstItem.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(dataSourceObj.fotfExtract);
		}
			
		RM.clickAnElement(dataSourceObj.fotfCom);
		//log.info("Click on FOTF.COM");						
	}
	
	public static void navigateToItem(String itemPath, String itemName){
		RM.SendKeys(sitecoreObj.treeSearch, itemPath);
		sitecoreObj.treeSearch.sendKeys(Keys.ENTER);
		Driver.findElement(By.xpath("//div[@id='SearchResult']//a[text()='"+itemName+"']")).click();
		sitecoreObj.treeSearch.clear();
		RM.clickAnElement(sitecoreObj.closeTreeSearch);
		
	}
	
	// Navigate to Create AutoAboutUs for Page Component under FOTF.com content of the Sitecore
	public static void createAboutUs() throws InterruptedException{		
		try{
			if(AboutusCommonClass.PageComponentContentItem().isDisplayed()){
				DeleteAboutUs();
		}}catch(org.openqa.selenium.NoSuchElementException e){}
		RM.clickAnElement(dataSourceObj.fotfCom);
		try{
			RM.clickAnElement(sitecoreObj.insertFromTemplate);
			RM.clickAnElement(sitecoreObj.insertFromTemplate);
		Driver.switchTo().frame("jqueryModalDialogsFrame");
		Driver.switchTo().frame("scContentIframeId0");
		}catch(Exception e){Driver.switchTo().frame("jqueryModalDialogsFrame");
		Driver.switchTo().frame("scContentIframeId0");}
		NavigateToPageComponentTemplate();
		
		RM.clickAnElement(sitecoreObj.pageComponent);
		sitecoreObj.itemName.clear();
		//sitecoreObj.itemName,"auto-aboutus");
		RM.SendKeys(sitecoreObj.itemName,AboutusCommonClass.PageComponentContentItemName());
		RM.clickAnElement(sitecoreObj.insertButton);
		Driver.switchTo().defaultContent();
		// log.info("Page Component has created called 'auto-aboutus' ");
		RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
	}
	
	// Navigate to Create Custom Initiative Component under FOTF.com content of the Sitecore
		public static void createCustomInitiativeComponent() throws InterruptedException{		
			try{
				if(AboutusCommonClass.InitiativeComponentContentItem().isDisplayed()){
					DeleteInitiative();
			}}catch(org.openqa.selenium.NoSuchElementException e){}
			RM.clickAnElement(dataSourceObj.fotfCom);
			try{
				RM.clickAnElement(sitecoreObj.insertFromTemplate);
				RM.clickAnElement(sitecoreObj.insertFromTemplate);
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");
			}catch(Exception e){Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");}
			NavigateToPageComponentTemplate();
			
			RM.clickAnElement(sitecoreObj.customInitiativePageComponent);
			sitecoreObj.itemName.clear();
			RM.SendKeys(sitecoreObj.itemName,AboutusCommonClass.InitiativeComponentContentItemName());
			RM.clickAnElement(sitecoreObj.insertButton);
			Driver.switchTo().defaultContent();
			RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
		}
		
	public static void Steps_CreateCommonComponent(String renderingControl){
		SwitchToFrame();
		Driver.switchTo().frame("scContentIframeId0");
		RM.clickAnElement(sitecoreObj.deviceEditor);
		//log.info("Opened Device Editor");
		
		SwitchToFrame();
		Driver.switchTo().frame("scContentIframeId1");
		RM.clickAnElement(sitecoreObj.controls);
		
		if(renderingControl.equals("Article")){
			RM.clickAnElement(sitecoreObj.renderingComponentImg);
			RM.clickAnElement(sitecoreObj.changeControls);
		}else{		
			RM.clickAnElement(sitecoreObj.addControls);
		}
		
		SwitchToFrame();
		Driver.switchTo().frame("scContentIframeId2");
		
		switch(renderingControl)
		{		
			case "ImageRight":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.imageOnRight);
				break;
			case "ImageLeft":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.imageOnLeft);
				break;
			case "ButtonRight":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.buttonOnRight);
				break;
			case "ButtonLeft":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.buttonOnLeft);
				break;
			case "ButtonBottom":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.buttonOnBottom);
				break;
			case "LeftImageList":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.leftImageWithList);
				break;
			case "RightImageList":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.RightImageWithList);
				break;
			case "IFrameRightImageList":
				NavigateToRenderingList("OneItem");
				RM.clickAnElement(sitecoreObj.IFrameRightImageWithList);
				break;
			case "SideBySide":
				NavigateToRenderingList("2Item");
				RM.clickAnElement(sitecoreObj.sideBySide);
				break;
			case "SideBySideCentered": 
				NavigateToRenderingList("2Item");
				RM.clickAnElement(sitecoreObj.sideBySideCentered);
				break;
			case "3Items3Columns": 
				NavigateToRenderingList("3Item");
				RM.clickAnElement(sitecoreObj.threeColumns);
				break;
			case "StackedImageLeft": 
				NavigateToRenderingList("3Item");
				RM.clickAnElement(sitecoreObj.stackedImageLeft);
				break;	
			case "3ColumnsLinksOverlaid": 
				NavigateToRenderingList("3Item");
				RM.clickAnElement(sitecoreObj.threeColumnsLinksOverlaid);
				break;
			case "4Items4ColumnsRectangle": 
				NavigateToRenderingList("4Item");
				RM.clickAnElement(sitecoreObj.fourItem4ColumnRectangle);
				break;
			case "4ItemsWithInset": 
				NavigateToRenderingList("4Item");
				RM.clickAnElement(sitecoreObj.fourItemWithInset);
				break;
			case "4Items2Columns": 
				NavigateToRenderingList("4Item");
				RM.clickAnElement(sitecoreObj.fourItem2Column);
				break;
			case "4Items4ColumnsSquare": 
				NavigateToRenderingList("4Item");
				RM.clickAnElement(sitecoreObj.fourItem4ColumnSquare);
				break;	
			case "5Column":
				NavigateToRenderingList("5Item");
				RM.clickAnElement(sitecoreObj.fiveColumn);
				break;	
			case "2x3":
				NavigateToRenderingList("5Item");
				RM.clickAnElement(sitecoreObj.twoBythree);
				break;		
			case "6Items3Columns": 
				NavigateToRenderingList("6Item");
				RM.clickAnElement(sitecoreObj.sixItem3Column);
				break;
			case "6Items6Columns": 
				NavigateToRenderingList("6Item");
				RM.clickAnElement(sitecoreObj.sixItem6Column);
				break;
			case "6Items6ColumnsWithText":
				NavigateToRenderingList("6Item");
				RM.clickAnElement(sitecoreObj.sixItem6ColumnWithText);
				break;
			case "6Items2Columns": 
				NavigateToRenderingList("6Item");
				RM.clickAnElement(sitecoreObj.sixItem2Column);
				break;
			case "HeaderImageLeft":
				NavigateToRenderingList("HeaderItem");
				RM.clickAnElement(sitecoreObj.HeaderImageLeft);
				break;
			case "HeaderImageRight":
				NavigateToRenderingList("HeaderItem");
				RM.clickAnElement(sitecoreObj.HeaderImageRight);
				break;
			case "HeaderImageTop":
				NavigateToRenderingList("HeaderItem");
				RM.clickAnElement(sitecoreObj.HeaderImageTop);
				break;
			case "HeaderVideoRight":
				NavigateToRenderingList("HeaderItem");
				RM.clickAnElement(sitecoreObj.HeaderVideoRight);
				break;
			case "HeaderImageCarousel":
				NavigateToRenderingList("HeaderItem");
				RM.clickAnElement(sitecoreObj.HeaderImageCarousel);
				break;
			case "Article":
				NavigateToRenderingList("Components");
				RM.clickAnElement(sitecoreObj.articleComponentControl);
				break;
			case "ArticlePubDate": 
				NavigateToRenderingList("Components");
				RM.clickAnElement(sitecoreObj.articleComponentControl);
				break;
			case "ArticleIsFeatured":
				NavigateToRenderingList("3Item");
				RM.clickAnElement(sitecoreObj.threeColumns);
				break;
			case "ShowHideImageLeft":			
				NavigateToRenderingList("ShowHideItem");
				RM.clickAnElement(sitecoreObj.ShowHideImageLeft);
				break;
			case "ShowHideBasic":
				NavigateToRenderingList("ShowHideItem");
				RM.clickAnElement(sitecoreObj.ShowHideBasic);
				break;		
			case "TimeLineBasic":
				NavigateToRenderingList("TimeLineItem");
				RM.clickAnElement(sitecoreObj.TimeLineBasic);
				break;	
			case "PageComponentHeader": break;
			case "InitiativeLeftHeroImage":
				NavigateToRenderingList("HeroItem");
				RM.clickAnElement(sitecoreObj.HeroImageLeft);
				break;
			case "InitiativeRightHeroImage":
				NavigateToRenderingList("HeroItem");
				RM.clickAnElement(sitecoreObj.HeroImageRight);
				break;
			case "InitiativeLeftHeroVideo":
				NavigateToRenderingList("HeroItem");
				RM.clickAnElement(sitecoreObj.HeroVideoLeft);
				break;
			case "InitiativeRightHeroVideo":
				NavigateToRenderingList("HeroItem");
				RM.clickAnElement(sitecoreObj.HeroVideoRight);
				break;
			default: break;
		}
	}
	
public static void Steps_SelectCommonComponent(String renderingControl) throws InterruptedException{	
	if(!renderingControl.equals("Article")){
		RM.SendKeys(sitecoreObj.placeHolderName,"/main/pagecomponent");
	}
	RM.clickAnElement(sitecoreObj.selectComponent);
//log.info("Container Component has created");
	
	SwitchToFrame();
	Driver.switchTo().frame("scContentIframeId1");
	switch(renderingControl)
	{		
		case "ImageRight":
			RM.clickAnElement(sitecoreObj.imageOnRightContainer);
			break;
		case "ImageLeft":
			RM.clickAnElement(sitecoreObj.imageOnLeftContainer);
			break;
		case "ButtonRight":
			RM.clickAnElement(sitecoreObj.buttonOnRightContainer);
			break;
		case "ButtonLeft":
			RM.clickAnElement(sitecoreObj.buttonOnLeftContainer);
			break;
		case "ButtonBottom":
			RM.clickAnElement(sitecoreObj.buttonOnBottomContainer);
			break;
		case "LeftImageList":
			RM.clickAnElement(sitecoreObj.LeftImageWithListContainer);
			break;
		case "RightImageList":
			RM.clickAnElement(sitecoreObj.RightImageWithListContainer);
			break;
		case "IFrameRightImageList":
			RM.clickAnElement(sitecoreObj.IFrameRightImageWithListContainer);
			break;
		case "SideBySide": 
			RM.clickAnElement(sitecoreObj.sideBySideContainer);
			break;
		case "SideBySideCentered": 
			RM.clickAnElement(sitecoreObj.sideBySideCenteredContainer);
			break;
		case "3Items3Columns": 
			RM.clickAnElement(sitecoreObj.threeColumnsContainer);
			break;
		case "StackedImageLeft": 
			RM.clickAnElement(sitecoreObj.stackedImageLeftContainer);
			break;
		case "3ColumnsLinksOverlaid": 
			RM.clickAnElement(sitecoreObj.threeColumnsLinksOverlaidContainer);
			break;
		case "4Items4ColumnsRectangle": 
			RM.clickAnElement(sitecoreObj.fourItem4ColumnsRectangleContainer);
			break;
		case "4ItemsWithInset": 
			RM.clickAnElement(sitecoreObj.fourItemWithInsetContainer);
			break; 
		case "4Items2Columns": 
			RM.clickAnElement(sitecoreObj.fourItem2ColumnsContainer);
			break;
		case "4Items4ColumnsSquare": 
			RM.clickAnElement(sitecoreObj.fourItem4ColumnsSquareContainer);
			break;	
		case "5Column":
			RM.clickAnElement(sitecoreObj.fiveColumnContainer);
			break;
		case "2x3": 
			RM.clickAnElement(sitecoreObj.twoBythreeContainer);
			break;		
		case "6Items3Columns": 
			RM.clickAnElement(sitecoreObj.sixItem3ColumnContainer);
			break;
		case "6Items6Columns": 
			RM.clickAnElement(sitecoreObj.sixItem6ColumnContainer);
			break;
		case "6Items6ColumnsWithText":
			RM.clickAnElement(sitecoreObj.sixItem6ColumnWithTextContainer);
			break;
		case "6Items2Columns": 
			RM.clickAnElement(sitecoreObj.sixItem2ColumnContainer);
			break;
		case "HeaderImageLeft":
			RM.clickAnElement(sitecoreObj.HeaderImageLeftContainer);
			break;
		case "HeaderImageRight":
			RM.clickAnElement(sitecoreObj.HeaderImageRightContainer);
			break;
		case "HeaderImageTop":
			RM.clickAnElement(sitecoreObj.HeaderImageTopContainer);
			break;
		case "HeaderVideoRight":
			RM.clickAnElement(sitecoreObj.HeaderVideoRightContainer);
			break;
		case "HeaderImageCarousel":
			RM.clickAnElement(sitecoreObj.HeaderImageCarouselContainer);
			break;
		case "Article":
			RM.clickAnElement(sitecoreObj.articleContainer);
			break;
		case "ArticlePubDate":
			RM.clickAnElement(sitecoreObj.articleContainer);
			break;
		case "ArticleIsFeatured":
			RM.clickAnElement(sitecoreObj.threeColumnsContainer);
			break;
		case "ShowHideImageLeft":
			RM.clickAnElement(sitecoreObj.ShowHideImageLeftContainer);
			break;
		case "ShowHideBasic":
			RM.clickAnElement(sitecoreObj.ShowHideBasicContainer);
			break;		
		case "TimeLineBasic":
			RM.clickAnElement(sitecoreObj.timeLineBasicContainer);
			break;
		case "PageComponentHeader": break;
		case "InitiativeLeftHeroImage":
			RM.clickAnElement(sitecoreObj.HeroImageLeftContainer);
			break;
		case "InitiativeRightHeroImage":
			RM.clickAnElement(sitecoreObj.HeroImageRightContainer);
			break;
		case "InitiativeLeftHeroVideo":
			RM.clickAnElement(sitecoreObj.HeroVideoLeftContainer);
			break;
		case "InitiativeRightHeroVideo":
			RM.clickAnElement(sitecoreObj.HeroVideoRightContainer);
			break;
		default: 
			break;
	}
	
	RM.clickAnElement(sitecoreObj.editContainer);
	SwitchToFrame();
	Driver.switchTo().frame("scContentIframeId2");
	////Thread.sleep(3000);
	String featuredDataSource="/sitecore/content/FOTF/";
	if(renderingControl.startsWith("Initiative")){
		featuredDataSource += AboutusCommonClass.InitiativeComponentContentItemName() 
				+ "/" + renderingControl + "_DataSource";
	}else{
		featuredDataSource += AboutusCommonClass.PageComponentContentItemName() 
			+ "/" + renderingControl + "_DataSource";
	}
	
	switch(renderingControl)
	{		
		case "ImageRight":	
		case "ImageLeft":
		case "ButtonRight":
		case "ButtonLeft":			
		case "ButtonBottom":
		case "LeftImageList":
		case "RightImageList":
		case "IFrameRightImageList":
		case "SideBySide": 
		case "SideBySideCentered":
		case "3Items3Columns": 
		case "3ColumnsLinksOverlaid": 
		case "StackedImageLeft":
		case "4Items4ColumnsRectangle": 
		case "4ItemsWithInset":
		case "4Items2Columns": 
		case "4Items4ColumnsSquare": 
		case "5Column":
		case "2x3": 
		case "6Items3Columns": 
		case "6Items6Columns":
		case "6Items6ColumnsWithText":
		case "6Items2Columns": 
		case "HeaderImageLeft":
		case "HeaderImageRight":
		case "HeaderImageTop":
		case "HeaderVideoRight":
		case "HeaderImageCarousel":
		case "ShowHideImageLeft":
		case "ShowHideBasic":
		case "TimeLineBasic":
			RM.SendKeys(sitecoreObj.dataSource,Opr_Sitecore_AboutusDatasource.dataSourcePath);
			break;
		case "ArticleIsFeatured": 
			RM.SendKeys(sitecoreObj.dataSource,featuredDataSource);
			break;			
		case "Article":
			RM.SendKeys(sitecoreObj.dataSource,featuredDataSource + "/" + Opr_AboutUsDataSource_DataProvider.getClonedArticleText);
			break;
		case "ArticlePubDate":
			RM.SendKeys(sitecoreObj.dataSource,featuredDataSource + "/" + Opr_AboutUsDataSource_DataProvider.getClonedArticleText);
			break;		
		case "PageComponentHeader": break;	
		case "InitiativeLeftHeroImage":
			RM.SendKeys(sitecoreObj.dataSource,featuredDataSource);
			break;
		default: 
			RM.SendKeys(sitecoreObj.dataSource,Opr_Sitecore_AboutusDatasource.dataSourcePath);
			break;
	}
	
	////Thread.sleep(2000);
	RM.clickAnElement(sitecoreObj.selectDataSource);
	
	SwitchToFrame();
	Driver.switchTo().frame("scContentIframeId1");
	RM.clickAnElement(sitecoreObj.selectComponent);
	
	SwitchToFrame();
	Driver.switchTo().frame("scContentIframeId0");
	RM.clickAnElement(sitecoreObj.selectComponent);
	
	Driver.switchTo().defaultContent();
}
// Switching to Frame for Navigation on Layouts for AutoAboutUs 	
	public static void SwitchToFrame() {
		Driver.switchTo().defaultContent();
		Driver.switchTo().frame("jqueryModalDialogsFrame");		
	}

// Navigate to Delete AutoAboutUs If Exist 
	private static void DeleteAboutUs() throws InterruptedException{
		RM.clickAnElement(AboutusCommonClass.PageComponentContentItem());
		////Thread.sleep(2000);
		Actions ref=new Actions(Driver);
		ref.contextClick(AboutusCommonClass.PageComponentContentItem()).build().perform();
		////Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.deleteItem);
		Thread.sleep(2000);
		Driver.switchTo().frame("jqueryModalDialogsFrame");
		Driver.switchTo().frame("scContentIframeId0");	
		RM.clickAnElement(sitecoreObj.okButton);
		Driver.switchTo().defaultContent();
		Thread.sleep(2000);
		try{		
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");	
			//RM.clickAnElement(dataSourceObj.removeLinksForDeleteAboutUsItem);
			RM.clickAnElement(dataSourceObj.deleteWithDatasoureItem);
			RM.clickAnElement(dataSourceObj.deleteWithDatasoureItem);
			Driver.switchTo().defaultContent();
			Thread.sleep(3000);
		}catch(Exception e){
			Driver.switchTo().defaultContent();
			log.fatal("Terminated Abruptly - No Sub Items to Remove:  " + e.getLocalizedMessage());
		}
	}

	// Navigate to Delete Initiative If Exist 
		private static void DeleteInitiative() throws InterruptedException{
			RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
			////Thread.sleep(2000);
			Actions ref=new Actions(Driver);
			ref.contextClick(AboutusCommonClass.InitiativeComponentContentItem()).build().perform();
			////Thread.sleep(2000);
			RM.clickAnElement(sitecoreObj.deleteItem);
			////Thread.sleep(2000);
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");	
			RM.clickAnElement(sitecoreObj.okButton);
			Driver.switchTo().defaultContent();
			
			try{		
				Driver.switchTo().frame("jqueryModalDialogsFrame");
				Driver.switchTo().frame("scContentIframeId0");	
				//RM.clickAnElement(dataSourceObj.removeLinksForDeleteAboutUsItem);
				RM.clickAnElement(dataSourceObj.deleteWithDatasoureItem);
				RM.clickAnElement(dataSourceObj.deleteWithDatasoureItem);
				Driver.switchTo().defaultContent();
				////Thread.sleep(3000);
			}catch(Exception e){
				Driver.switchTo().defaultContent();
				log.fatal("Terminated Abruptly - No Sub Items to Remove:  " + e.getLocalizedMessage());
			}
		}
	public static void NavigateToRenderingList(String renderingControl){
		try{
			sitecoreObj.component_Rendering.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(sitecoreObj.renderings_Control);
		}
		
		try{
			sitecoreObj.oneItem_Components.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(sitecoreObj.component_Rendering);
		}
		
		if(renderingControl.equals("OneItem")){
			try{
			if(sitecoreObj.oneItemList.isDisplayed()){}
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.oneItem_Components);
			}
		}
		
		if(renderingControl.equals("HeaderItem")){
			try{
				sitecoreObj.headerItemlist.isDisplayed();
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.headerItem_Components);
			}
		}
		
		if(renderingControl.equals("ShowHideItem")){
			try{
				sitecoreObj.showhideItemlist.isDisplayed();
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.showHideItem_Components);
			}
		}
		
		if(renderingControl.equals("TimeLineItem")){
			try{
				sitecoreObj.timeLineBasicItemlist.isDisplayed();
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.timeLineBasicItem_Components);
			}
		}
		
		if(renderingControl.equals("5Item")){
			try{
				sitecoreObj.fiveItemlist.isDisplayed();
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.fiveItem_Components);
			}
		}
		
		if(renderingControl.equals("2Item")){
			try{
				sitecoreObj.twoItemlist.isDisplayed();
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.twoItem_Components);
			}
		}	
		
		if (renderingControl.equals("3Item")) {
			try {
				sitecoreObj.threeItemlist.isDisplayed();
			} catch (org.openqa.selenium.NoSuchElementException e) {
				RM.clickAnElement(sitecoreObj.threeItem_Components);
			}
		}
		
		if (renderingControl.equals("6Item")) {
			try {
				sitecoreObj.sixItemlist.isDisplayed();
			} catch (org.openqa.selenium.NoSuchElementException e) {
				RM.clickAnElement(sitecoreObj.sixItem_Components);
			}
		}
		
		if (renderingControl.equals("4Item")) {
			try {
				sitecoreObj.fourItemlist.isDisplayed();
			} catch (org.openqa.selenium.NoSuchElementException e) {
				RM.clickAnElement(sitecoreObj.fourItem_Components);
			}
		}
		
		if (renderingControl.equals("HeroItem")) {
			try {
				sitecoreObj.HeroImageLeft.isDisplayed();
			} catch (org.openqa.selenium.NoSuchElementException e) {
				RM.clickAnElement(sitecoreObj.heroItem_Components);
			}
		}
	}

	public static void NavigateToPageComponentTemplate() {
		try {
			sitecoreObj.componentsUnderCommon.isDisplayed();
		}
		catch(org.openqa.selenium.NoSuchElementException e){			
			RM.clickAnElement(sitecoreObj.commonUnderTemplates);
		}
		
		try{
			sitecoreObj.pageComponent.isDisplayed();
		}
		catch(org.openqa.selenium.NoSuchElementException e){
			RM.clickAnElement(sitecoreObj.componentsUnderCommon);			
		}
	}
}
