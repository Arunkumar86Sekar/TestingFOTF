package fotf_Operations;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import fotf_Config.Basedriver;

public class Opr_Sitecore_AboutUs_Assert extends Basedriver{

	static Opr_AboutUs_AssertElement objAssertAboutUs=new Opr_AboutUs_AssertElement();
	static Opr_AboutUs_AssertElement_Article objAssertAboutUsArticle=new Opr_AboutUs_AssertElement_Article();
	
// Verify Elements in Child window for AutoAboutUs Preview page
	public static void AssertAboutus(String imagePosition, String pageMode) throws InterruptedException{
		/*if(!pageMode.equals("PageEditor")){
			Thread.sleep(3000);
			Driver.switchTo().window(GetChildWindow("CHILD"));
			try{
				if(sitecoreObj.skipImage.isEnabled()){
					RM.clickAnElement(sitecoreObj.skipImage);	}
			}catch(org.openqa.selenium.NoSuchElementException e){
				System.out.println(e);
			}catch(Exception e){
				System.out.println(e);
			}
		}*/
		
		switch(imagePosition)
		{		
			
			case "RightImageList":
				objAssertAboutUs.VerifyCommonComponent_ListImageRightTitle("OneItem RightImageList");
				objAssertAboutUs.VerifyListIcon("OneItem RightImageList");
				objAssertAboutUs.VerifyListText("OneItem RightImageList");
				objAssertAboutUs.VerifyDescText_ContentDesc("//li[", "]/div/p");
				objAssertAboutUs.VerifyCommonComponent_ImageShape(1);
				break;
			
			case "ArticleShareBar":
				objAssertAboutUsArticle.VerifyArticle_ShareBarUrl();
			break;
			default: break;
		}

		/*Driver.switchTo().window(GetChildWindow("CHILD")).close();
		Driver.switchTo().window(GetChildWindow("PARENT"));		*/
	}
	
// Navigate to Child window for AutoAboutUs Preview page
	public static String GetChildWindow(String mode){
		Set<String> window=Driver.getWindowHandles();
		Iterator<String> itr = window.iterator();
		String parentName,childName, grandChildName;
		switch(mode){
		case"PARENT":
			return parentName=itr.next();
		
		case"CHILD":
			parentName=itr.next();
			return childName=itr.next();
			
		case"GRANDCHILD":
			parentName=itr.next();
			childName=itr.next();
			return grandChildName=itr.next();
		}
		return "Dummy";
	}	
}
