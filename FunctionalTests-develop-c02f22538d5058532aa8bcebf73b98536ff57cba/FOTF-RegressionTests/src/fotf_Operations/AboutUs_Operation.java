package fotf_Operations;

import org.openqa.selenium.WebDriver;

import fotf_Config.Basedriver;

public class AboutUs_Operation extends Basedriver{
	public static String getSetLinkUrl(WebDriver Driver){
		String pageText = Driver.getPageSource();
		int pFrom = pageText.indexOf("act.setLinkBack(");
		int pTo = pageText.lastIndexOf("act.addMediaItem({ ");
		String result=pageText.substring(pFrom, pTo);
		result=result.replace("act.setLinkBack(\"", "");
		result=result.replace("\");", "");
		result=result.replace("www.focusonthefamily.com", server);
		Driver.get(result);
		return result;
		
	}
}
