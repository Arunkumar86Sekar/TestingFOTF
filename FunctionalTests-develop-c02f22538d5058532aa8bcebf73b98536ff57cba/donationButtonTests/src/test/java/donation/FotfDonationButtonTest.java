package donation;

import org.testng.annotations.*;

//Verify Source Code is passed to the Donation URL from the button
public class FotfDonationButtonTest {
  private donationTests test = new donationTests();
  String baseUrl;
  String baseTitle = "Focus on the Family: Helping Families Thrive";
  String username;
  String password;
  String buttonValue;
  String sourceCode1;
  String sourceCode2;
  String buttonXpath = "//a[@class='donation_message--button']";
  String buttonAttr = "data-goal-id";
  String controlID =   "b5a39e32-9d28-4418-8885-76da9c0746fb";
  String variationID = "8921ab01-7c59-428e-a5cc-680615a92382";
  String campaignID = "6fd72f49-fd49-4a2c-ad18-d94201ea3a71";
  // String sitecoreURL = "http://10.194.5.65:81";
  String sitecoreURL;
  String version1 = "Control";
  String version2 = "Variation";
  String orgStartDate;
  String orgEndDate;
  String startDateXpath;
  String endDateXpath;

  @BeforeClass
  private void setUp() throws Exception {
    System.out.println("<----------Testing Summer 2016 Donation Buttons---------->");
    test.setWaits();
    baseUrl = test.setEnvironmentVariables();
    sitecoreURL = test.setSiteCoreVariables();
    System.out.println("baseUrl = " + baseUrl);
    username = "admin";
    password = "b";
    test.removeAllCookies();
  }

  @AfterClass
  public void tearDown() throws Exception {
    //return to sitecore button settings
    System.out.println("<--Return to Original Settings-->");
    test.removeAllCookies();
    test.openAndLogin(sitecoreURL + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Summer", "//a/span[contains(text(), '2016 Summer')]");

    System.out.println("Changing Start Date to -->" + orgStartDate);
    test.changeValue(startDateXpath, orgStartDate);

    System.out.println("Changing End Date to --->" + orgEndDate);
    test.changeValue(endDateXpath, orgEndDate);

    //publish changes
    test.siteCorePublishCampaign();

    test.quitTest();
  }

  @Test(priority = 1)
  private void loginToSiteCoreAndGetValues() throws Exception {
    //Determine which banner is displayed
    // test.open(baseUrl);
    // buttonValue = test.whichVariation(buttonXpath, buttonAttr, controlID, variationID);
    buttonValue = version1;
    System.out.println("<--Getting " + version1 + " Values -->");

    test.openAndLogin(sitecoreURL + "/sitecore/login", username, password);

    //Navigate to SiteCore Campaign Settings
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Summer", "//a/span[contains(text(), '2016 Summer')]/../../img");
    test.openSitecoreFolder(buttonValue + " Button", "//a/span[contains(text(), '" + buttonValue + "')]");

    sourceCode1 = test.getSiteCoreValue("Source Code:");
    test.isNotBlank(sourceCode1);

    //get sc values for variationID
    System.out.println("<--Getting " + version2 + " Values -->");
    buttonValue = version2;
    test.openSitecoreFolder(buttonValue + " Button", "//a/span[contains(text(), '" + buttonValue + "')]");
    sourceCode2 = test.getSiteCoreValue("Source Code:");
    test.isNotBlank(sourceCode2);

    System.out.println("Control SourceCode = " + sourceCode1);
    System.out.println("Variation SourceCode = " + sourceCode2);
  }

  @Test(priority = 2)
  private void testingButton1() throws Exception{
    System.out.println("<--Testing Control Button-->");
    buttonValue = version1;
    test.open(baseUrl);
    test.removeAllCookies();
    test.open(baseUrl);

    //reload until correct banner is displayed
    while (buttonValue != test.whichVariation(buttonXpath, buttonAttr, controlID, variationID)) {
      test.clearAndReload(baseUrl);
    }

    test.clickButton("//a[@class='donation_message--button']");

    test.urlContains(campaignID);
    test.urlContains(sourceCode1);
    }

  @Test(priority = 3)
  private void testingButton2() throws Exception{
    System.out.println("<--Testing Variation Button-->");
    buttonValue = version2;
    test.open(baseUrl);
    test.removeAllCookies();
    test.open(baseUrl);

    //reload until correct banner is displayed
    while (buttonValue != test.whichVariation(buttonXpath, buttonAttr, controlID, variationID)) {
      test.clearAndReload(baseUrl);
    }

    test.clickButton("//a[@class='donation_message--button']");

    test.urlContains(campaignID);
    test.urlContains(sourceCode2);
    }

@Test(priority = 4)
private void campaignActive() throws Exception {

 //Navigate to SiteCore Campaign Settings
    test.openAndLogin(sitecoreURL + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Summer", "//a/span[contains(text(), '2016 Summer')]");

     //change date by -->
    int days = 4;

    startDateXpath = "//*[contains(text(), 'Start Date:')]/following-sibling::div[2]//table[1]//input";
    endDateXpath = "//*[contains(text(), 'End Date:')]/following-sibling::div[2]//table[1]//input";

    orgStartDate = test.getAttribute(startDateXpath, "value");
    System.out.println("Original Start Date " + orgStartDate);

    String newStartDate = test.changeDate(-days);
    System.out.println("Changing Start Date to -->" + newStartDate);
    test.changeValue(startDateXpath, newStartDate);

    orgEndDate = test.getAttribute(endDateXpath, "value");
    System.out.println("Original End Date " + orgEndDate);

    String newEndDate = test.changeDate(days);
    System.out.println("Changing End Date to --->" + newEndDate);
    test.changeValue(endDateXpath, newEndDate);

    test.siteCorePublishCampaign();

    test.open(baseUrl);

    System.out.println("Testing button-->");

    String buttonDisplays = test.whichVariation(buttonXpath, buttonAttr, controlID, variationID);
    System.out.println("Button Displays--?= " + buttonDisplays);

    //button should display
    test.testTrue(test.doesElementExist(buttonXpath));

    //return to sitecore button settings
    test.removeAllCookies();
    test.openAndLogin(sitecoreURL + "/sitecore/login", username, password);
    test.openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    test.openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    test.openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    test.openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    test.openSitecoreFolder("2016 Summer", "//a/span[contains(text(), '2016 Summer')]");

    days = 10;

    newStartDate = test.changeDate(-days);
    System.out.println("Changing Start Date to -->" + newStartDate);
    test.changeValue(startDateXpath, newStartDate);

    days = -4;
    newEndDate = test.changeDate(days);
    System.out.println("Changing End Date to --->" + newEndDate);
    test.changeValue(endDateXpath, newEndDate);

    //publish changes
    test.siteCorePublishCampaign();

    test.open(baseUrl);

    buttonDisplays = test.whichVariation(buttonXpath, buttonAttr, controlID, variationID);
    System.out.println("Button Displays--?= " + buttonDisplays);

    //button should not display
    String val = test.whichVariation(buttonXpath, buttonAttr, controlID, variationID);
    test.testTrue(val == "Unknown");

}
}
