package donation;

import org.testng.annotations.*;

//Verify VisitorID is passed to the Donation URL from the Donation button
public class FotfVisitorIDTest {
  private donationTests test = new donationTests();
  String buttonXpath = "//a[@class='donation_message--button']";
  String baseUrl;

  @BeforeTest
  private void setUp() throws Exception {
    System.out.println("<----------Testing VisitorID---------->");
    test.setWaits();
    baseUrl = test.setEnvironmentVariables();
    System.out.println("baseUrl = " + baseUrl);
  }

  @Test
  public void testVisitorID() throws Exception {
    System.out.println("\n<----------Verify Visitor ID Cookie---------->");
    test.open(baseUrl);

    String cookie = test.getVisitorIDCookieValue("SC_ANALYTICS_GLOBAL_COOKIE");
    System.out.println("Cookie= -->" + cookie);
    test.clickButton(buttonXpath);
    String visitor = test.getVisitorIDUrlValue();
    System.out.println("VisitorID= " + visitor);
    test.testTrue(visitor.equals(cookie));
  }

  @AfterTest
  public void tearDown() throws Exception {
    test.quitTest();

  }
}
