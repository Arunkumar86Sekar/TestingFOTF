package donation;

// import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.*;
import java.text.*;
import java.lang.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

public class donationTests {
  public WebDriver driver = new FirefoxDriver();
  WebDriverWait wait = new WebDriverWait(driver, 20);

  private String baseTitle = "Focus on the Family: Helping Families Thrive";

  public String setSiteCoreVariables() {
    String sitecoreURL = System.getenv("SITECORE_HOST");

    while (sitecoreURL.endsWith("/")) {
      sitecoreURL = sitecoreURL.substring(0, sitecoreURL.length()-1);
    }

    System.out.println("Sitecore URL = " + sitecoreURL);

    return sitecoreURL;
  }
  public String setEnvironmentVariables() {
    String baseUrl = System.getenv("TEST_SERVER_HOST");
    System.out.println("Base URL = " + baseUrl);

    while (baseUrl.endsWith("/")) {
      baseUrl = baseUrl.substring(0, baseUrl.length()-1);
    }

    return baseUrl;

  }

  public void setWaits() {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  public void open(String baseUrl) {
    System.out.println("Opening --> " + baseUrl);
    driver.get(baseUrl);
    assertEquals(driver.getTitle(), baseTitle);
    driver.manage().window().maximize();
  }

  public void openAndLogin(String url, String usernameText, String passwordText) {
    driver.get(url);
    driver.manage().window().maximize();

    WebElement username = driver.findElement(By.id("UserName"));
    WebElement password = driver.findElement(By.id("Password"));

    username.clear();
    password.clear();

    username.sendKeys(usernameText);
    password.sendKeys(passwordText);

    clickButton("//input[@class='btn btn-primary btn-block']");
  }

  public int whichBannerIsDisplayed(String baseUrl) {
    driver.get(baseUrl);
    driver.manage().window().maximize();
    int output = 0;

    String var = "//div[@class='campaign--banner campaign--banner-variation banner--actions_visible']";
    String control = "//div[@class='campaign--banner banner--actions_visible']";

    if (doesElementExist(var)) {
      output = 2;
    } else if (doesElementExist(control)) {
      output = 1;
    } else LogEntry("Error --> Unknown Banner");

    //System.out.println("Banner " + output + " is displayed");
    return output;
  }

  public void clearAndReload(String baseUrl) {
    System.out.println("Reloading Page--->");
    removeAllCookies();
    driver.get(baseUrl);
  }

  public String getAttribute(String xpath, String attr) {
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    String value = driver.findElement(By.xpath(xpath)).getAttribute(attr);

    return value;
  }

  public void changeValue(String xpath, String newValue) {
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    WebElement element = driver.findElement(By.xpath(xpath));

    //clear original value
    element.clear();
    element.sendKeys(newValue);
  }

  //change date by number of days
  public String changeDate(int numDays){

    SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy");
    Date dt = new Date();
    Calendar c = Calendar.getInstance();
    c.setTime(dt);
    c.add(Calendar.DATE, numDays);
    dt = c.getTime();

    String newDate = ft.format(dt);

    return newDate;

  }

  //get text from Rich Text Editor
  public String getRTEvalue(String value) {

    String xpath = "//div[contains(text(), '"+ value +"')]/following-sibling::div[1]/a[1]";

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    driver.findElement(By.xpath(xpath)).click();

    driver.switchTo().frame("jqueryModalDialogsFrame");
    driver.switchTo().frame("scContentIframeId0");
    driver.switchTo().frame("Editor_contentIframe");

    String frameXpath = "//html/body";

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(frameXpath)));
    String text = driver.findElement(By.xpath(frameXpath)).getText();
    System.out.println(value + "= " + text);

    driver.switchTo().defaultContent();
    driver.switchTo().frame("jqueryModalDialogsFrame");
    driver.switchTo().frame("scContentIframeId0");

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='CancelButton']")));
    driver.findElement(By.xpath("//*[@id='CancelButton']")).click();
    driver.switchTo().defaultContent();

    return text;
  }

  public void siteCorePublishCampaign() {
    System.out.print("Publishing Changes --->");
    clickButton("//*[contains(text(), 'Save')]");
    clickButton("//*[contains(text(), 'Publish')]");

    clickButton("//div[@id='B414550BADAF4542C9ADF44BED5FA6CB3E']/a[1]");

    driver.switchTo().frame("jqueryModalDialogsFrame");

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

    driver.switchTo().frame("scContentIframeId0");

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='OK']")));
    driver.findElement(By.id("OK")).click();

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

    driver.switchTo().defaultContent();

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

    driver.switchTo().frame("jqueryModalDialogsFrame");

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

    driver.switchTo().frame("scContentIframeId0");

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='OK']")));
    driver.findElement(By.id("OK")).click();

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

    driver.switchTo().defaultContent();

    try {
      // thread to sleep for 5000 milliseconds
      Thread.sleep(5000);
    } catch (Exception e) {
      System.out.println(e);
    }

  }

  public void openSitecoreFolder(String folderName, String folderXpath) {

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(folderXpath)));
    WebElement folderButton = driver.findElement(By.xpath(folderXpath));
    System.out.println("Opening --> " + folderName);
    folderButton.click();
  }

  public void testCampiagnValues(String baseUrl, int bannerValue) {
    WebElement element;
    String actual;

    String title = "Equip parents to succeed!";
    String shortDescription = getRTEvalue("Short Description:");
    String subTitle2 = getRTEvalue("Subtitle 2:");
    String thankYouMessage = getRTEvalue("Thank you Message:");
    String donationButton = getSiteCoreValue("Donation Button Text:");
    String donationSubtext = getSiteCoreValue("Donation Button Sub Text:");
    String remindMeButton = getSiteCoreValue("Remind Me Button Text:");
    String remindMeButtonSubText = getSiteCoreValue("Reminder Me Button Sub Text:");
    String remindMeLabelText = getSiteCoreValue("Remind Me Label Text:");
    String remindMeCheckboxLabelText = getSiteCoreValue("Remind Me Checkbox Label Text:");
    String remindMeActionButtonText = getSiteCoreValue("Remind Me Action Button Text:");
    String remindMeCheckboxValidationText = getSiteCoreValue("Remind Me Checkbox Validation Text:");
    String emailAddressValidationText = getSiteCoreValue("Email Address Validation Text:");
    String thankyouContinueText = getSiteCoreValue("Thank you Continue Text:");
    String footerContinueText = getSiteCoreValue("Footer Continue Text:");
    String sourceCode = getSiteCoreValue("Source Code:");

    open(baseUrl);
    while (bannerValue != whichBannerIsDisplayed(baseUrl)) {
      clearAndReload(baseUrl);
    }

    //Title
    actual = driver.findElement(By.xpath("//div[@class='banner--cause_container']/h2")).getText();
    testValues(title, actual);

    //Short Description
    actual = driver.findElement(By.xpath("//div[@class='banner--cause_container']/p")).getText();
    testValues(shortDescription, actual);

    //buttons
    WebElement yesButton = driver.findElement(By.xpath("//div[@class='banner--actions_container']//button[1]"));
    WebElement noButton = driver.findElement(By.xpath("//form//*[contains(text(), 'Remind')]"));

    yesButton = driver.findElement(By.xpath("//div[@class='banner--actions_container']//button[1]"));
    noButton = driver.findElement(By.xpath("//form//*[contains(text(), 'Remind')]"));
    noButton.click();

    enterText("//input[@id='email']", "testing@internet.com");
    checkBox("//div[@class='banner--form-agree']/input");
    clickButton("//div[@class='banner--email_inline_submit_button_container']/button");
    checkText("//p[@class='banner--confirmation_text']",thankYouMessage);

    //Test webconnex form
    open(baseUrl);
    while (bannerValue != whichBannerIsDisplayed(baseUrl)) {
      clearAndReload(baseUrl);
    }

    driver.findElement(By.xpath("//div[@class='banner--minimized-shell']/button")).click();
    driver.findElement(By.xpath("//div[@class='banner--actions_container']//button[1]")).click();
    urlContains("https://focusonthefamily.webconnex.com");
    open(baseUrl);

    while (bannerValue != whichBannerIsDisplayed(baseUrl)) {
      clearAndReload(baseUrl);
    }

    //minimizedDonationAmounts
    String[] minimizedDonationNames = {"$50", "$100", "$150", "$300", "Other..."};
    String[] minimizedDonationAmounts = {"50", "100", "150", "300", "0"};
    for (int i = 0; i < minimizedDonationAmounts.length; i++) {
      int j = i + 1;
      System.out.println("Expected ->" + minimizedDonationAmounts[i]);
      String actualAmount = driver.findElement(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + j + "]")).getAttribute("value");
      System.out.println("Actual --->" + actualAmount);
      // assertTrue(isElementPresent(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + i + "]")));
      assertEquals(actualAmount, minimizedDonationAmounts[i]);
      System.out.println("Expected ->" + minimizedDonationNames[i] + "\nActual --->" + driver.findElement(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + j + "]")).getText());
      assertEquals(driver.findElement(By.xpath("//div[@class='banner--minimized-column_secondary']//button["  + j + "]")).getText(), minimizedDonationNames[i]);
    }

  }

  public void testValues(String expected, String actual) {

    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    assertEquals(expected, actual);
  }

  public void testTrue(boolean x) {
    assertTrue(x);
  }

  public boolean doesElementExist(String elementPath) {
    boolean elementExists = driver.findElements(By.xpath(elementPath)).size() != 0;
    return elementExists;
  }

  public void switchRichTextEditorFrame() {
    driver.switchTo().frame("jqueryModalDialogsFrame");
    driver.switchTo().frame("scContentIframeId0");
  }

  public String getSiteCoreValue(String getValue) {
    String localXpath = "//*[contains(text(), '" + getValue + "')]/following-sibling::div/input";

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(localXpath)));

    String value = driver.findElement(By.xpath(localXpath)).getAttribute("value");

    System.out.println(getValue + "= " + value);

    return value;
  }

  public String getURLSourceCode() {
    String sourceCode;
    String urlValue = driver.getCurrentUrl();

    sourceCode = urlValue;
    return sourceCode;
  }

  //returns a string containing the visitor id
  public String getVisitorIDUrlValue() {
    String visitorRef = null;
    String visitorID;
    String urlValue = driver.getCurrentUrl();

    //Check for variations of visitor id
    if (urlValue.contains("VisitorId=")) {
      visitorRef = "VisitorId=";
    }
    if (urlValue.contains("VisitorID=")) {
      visitorRef = "VisitorID=";
    }
    if (urlValue.contains("Visitorid=")) {
      visitorRef = "Visitorid=";
    }
    if (urlValue.contains("visitorid=")) {
      visitorRef = "visitorid=";
    }
    if (urlValue.contains("visitorId=")) {
      visitorRef = "visitorId=";
    }
    if (urlValue.contains("visitorID=")) {
      visitorRef = "visitorID=";
    }

    if (urlValue.contains(visitorRef)){
      visitorID = urlValue.substring(urlValue.indexOf(visitorRef));
      visitorID = visitorID.replaceAll("-", "");
      visitorID = visitorID.substring(visitorID.indexOf("=") + 1);
      if(visitorID.contains("&")) {
        visitorID = visitorID.substring(0, visitorID.indexOf("&"));
      }
    }else visitorID = "ERROR, URL does not contain a valid VisitorID";

    return visitorID;
  }
  // public String getVisitorIDUrlValue() {
  //   String visitorID;
  //   String urlValue = driver.getCurrentUrl();
  //   visitorID = urlValue.substring(urlValue.indexOf("visitorid="));
  //   if (visitorID != null) {
  //     visitorID = visitorID.replaceAll("-", "");
  //     visitorID = visitorID.substring(visitorID.indexOf("=") + 1);
  //     if(visitorID.contains("&")) {
  //       visitorID = visitorID.substring(0, visitorID.indexOf("&"));
  //     }else {
  //       visitorID = "ERROR, URL does not contain VisitorID";
  //     }
  //
  //   }
  //   return visitorID;
  // }

  public String getVisitorIDCookieValue(String cookieName) {
    String cookieValue = driver.manage().getCookieNamed(cookieName).getValue();
    cookieValue = cookieValue.substring(0, cookieValue.indexOf("|"));

    return cookieValue;
  }

  public String getCookieAttribute (String cookie, String attr) {
    int startIndex = cookie.indexOf(attr);
    int endIndex = cookie.indexOf(";", startIndex);
    String newString = cookie.substring(startIndex, endIndex);
    //remove 'attr'= value, there should be a better way to do this.
    newString = newString.substring(newString.indexOf("=") + 1);

    return newString;
  }

  public void removeAllCookies() {
    //System.out.println("Deleting Cookies-->");
    driver.manage().deleteAllCookies();
    try {
      // thread to sleep for 8000 milliseconds
      Thread.sleep(8000);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public void enterText(String path, String text) {
    System.out.println("TYPING---> " + text);
    WebElement element = driver.findElement(By.xpath(path));

    element.sendKeys(text);
  }

  public void checkBox(String path) {
    WebElement element = driver.findElement(By.xpath(path));
    if (!element.isSelected()) {
      System.out.println("Checking box");
      element.click();
    }else System.out.println("Box is checked");
  }

  public void uncheckBox(String path){
    WebElement element = driver.findElement(By.xpath(path));
    if (element.isSelected()) {
      System.out.println("Unchecking box");
      element.click();
    } else {
      System.out.println("Box is unchecked");
    }
  }

  public void clickButton(String path){
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(path)));

    WebElement element = driver.findElement(By.xpath(path));

    element.click();
  }

  public void submitForm(String path){
    WebElement element = driver.findElement(By.xpath(path));

    element.submit();
  }

  public void checkText(String path, String text) {
    WebElement element = driver.findElement(By.xpath(path));
    String actual = element.getText();
    String expected = text;

    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    assertEquals(expected, actual);
  }

  public void urlContains(String expected) {
    String actual = driver.getCurrentUrl();

    boolean val = actual.contains(expected);

    System.out.println("URL contains -->"  + expected + "? == " + val);

    assertTrue(val);
  }

  public void containsValue(String expected, String actual) {
    boolean val = actual.contains(expected);
    System.out.println("Actual value -> " + actual);
    System.out.println("Contains -----> " + expected);
  }

  public void quitTest() {
    driver.quit();
  }

  public void getCampaignValues(String campaign) {

    //Navigate to SiteCore Campaign Settings
    openSitecoreFolder("Content Editor", "//a[@title='Content Editor']");
    openSitecoreFolder("Content", "//a/span[contains(text(), 'Content')]/../../img");
    openSitecoreFolder("Standard Items", "//a/span[contains(text(), 'Standard Items')]/../../img");
    openSitecoreFolder("Campaigns", "//a/span[contains(text(), 'Campaigns')]/../../img");
    openSitecoreFolder("2016 Spring", "//a/span[contains(text(), '2016 Spring')]/../../img");
    openSitecoreFolder(campaign, "//a/span[contains(text(), '" + campaign + "')]/..");

    String title = "Equip parents to succeed!";
    String shortDescription = "Help transform families in at-risk communities through Raising Highly Capable Kids™.";
    String subTitle2 = "Equip parents to succeed!";
    String thankYouMessage = "Thank you for your support.\nCheck your inbox soon for more information.";
    String donationButton = getSiteCoreValue("Donation Button Text:");
    String donationSubtext = getSiteCoreValue("Donation Button Sub Text:");
    String remindMeButton = getSiteCoreValue("Remind Me Button Text:");
    String remindMeButtonSubText = getSiteCoreValue("Reminder Me Button Sub Text:");
    String remindMeLabelText = getSiteCoreValue("Remind Me Label Text:");
    String remindMeCheckboxLabelText = getSiteCoreValue("Remind Me Checkbox Label Text:");
    String remindMeActionButtonText = getSiteCoreValue("Remind Me Action Button Text:");
    String remindMeCheckboxValidationText = getSiteCoreValue("Remind Me Checkbox Validation Text:");
    String emailAddressValidationText = getSiteCoreValue("Email Address Validation Text:");
    String thankyouContinueText = getSiteCoreValue("Thank you Continue Text:");
    String footerContinueText = getSiteCoreValue("Footer Continue Text:");
    String sourceCode = getSiteCoreValue("Source Code:");
  }

  public void isNotBlank(String text) {
    int textLength = text.length();
    if (textLength <= 0) {
      LogEntry("!---------Text is blank--------!");
    }

    assertTrue(textLength > 0);
  }

  public String getCookieAndConvertToString(String cookieName) {
    Cookie cookie = driver.manage().getCookieNamed(cookieName);
    String cookieString = null;

    //Convert Cookie to String
    if (cookie != null) {
      cookieString = cookie.toString();
    }

    System.out.println("Cookie = " + cookieString);
    return cookieString;
  }

  //change date by number of days
  public String changeDateAndFormat(int numDays){

    SimpleDateFormat ft = new SimpleDateFormat ("E, d MMM y");
    Date dt = new Date();
    Calendar c = Calendar.getInstance();
    c.setTime(dt);
    c.add(Calendar.DATE, numDays);
    dt = c.getTime();

    String newDate = ft.format(dt);

    return newDate;
  }

  public static void printProgBar(int percent){
    StringBuilder bar = new StringBuilder("[");

    for(int i = 0; i < 50; i++){
        if( i < (percent/2)){
            bar.append("=");
        }else if( i == (percent/2)){
            bar.append(">");
        }else{
            bar.append(" ");
        }
    }

    bar.append("]   " + percent + "%     ");
    System.out.print("\r" + bar.toString());
}

public void LogEntry(String logmsg){
  System.out.println("\n<-- "+logmsg+" -->");
  Reporter.log("\n<-- "+logmsg+" -->");
}

public void LogVerification(Boolean condition, String msg)
  {
    if (condition)
    {
      LogEntry(" *** "+msg +" is verified.");
    }
    else
    {
      LogEntry(" XXXXX  FAIL  XXXXX "+msg+" is not verified.");
    }
  }

public String whichVariation(String xpath, String attribute, String controlID, String variationID) {
  String output = "Unknown";

  if (doesElementExist(xpath)) {
    String var = driver.findElement(By.xpath(xpath)).getAttribute(attribute);
    if (var != null){
        if (var.equals(controlID)) {
        System.out.println("Control Displayed");
        output = "Control";
      } else {
        if (var.equals(variationID)) {
          System.out.println("Variation Displayed");
          output =  "Variation";
        }
        }
      }
    }
    return output;
 }

//  public void doesAttributeExist(String xpath) {
//    String string = driver.findElement(By.xpath(xpath)).getAttribute("data-goal-id");
//    System.out.println(string);
//  }
//
//
// public boolean isElementPresent(By by) {
//   try {
//     driver.findElement(by);
//     return true;
//   } catch (NoSuchElementException e) {
//     return false;
//   }
// }
}
