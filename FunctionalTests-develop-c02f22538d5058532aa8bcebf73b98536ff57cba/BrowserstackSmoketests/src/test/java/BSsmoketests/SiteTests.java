package BSsmoketest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.*;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SiteTests {
  // public WebDriver driver = new FirefoxDriver();
  public WebDriver driver;

  private String baseUrl;
  private String baseTitle;
  private String channel;
  private String fotf;
  private JavascriptExecutor js = (JavascriptExecutor)driver;

  public void setUpBrowserStack() throws Exception {
    DesiredCapabilities capability = new DesiredCapabilities();
    capability.setPlatform(Platform.WINDOWS);
    capability.setCapability("build", "TestNG - Sample");
    capability.setCapability("browserstack.local", "true");

    driver = new RemoteWebDriver(
      new URL("https://austinjones5:r16JqkpWrqwoULicr9KG@hub-cloud.browserstack.com/wd/hub"),
      capability
    );
  }

  public void setWaits() {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  public void maximizeBrowser() {
    driver.manage().window().maximize();
  }

  public void checkImage (String xpath) {
    WebElement ImageFile = driver.findElement(By.xpath(xpath));

    Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver)
    .executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
    if (!ImagePresent)
    {
         System.out.println("Image not displayed.");
    }
    else
    {
        System.out.println("Image displayed.");
    }
  }

  public boolean isLinkExternal (String xpath) {
    WebElement element = driver.findElement(By.xpath(xpath));
    String value = element.getAttribute("innerHTML");
    boolean opensExternalLink;

    if (value.contains("target=")) {
      opensExternalLink = true;
    } else opensExternalLink = false;

    return opensExternalLink;

  }

  public void setAttribute(WebElement element, String attName, String attValue) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    // display("Setting " + attName + " to " + attValue);

    js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);

  }

  public void goToLinkAndTestTitle(String xpath) {
    //Go to link, check title and return
    WebElement element = driver.findElement(By.xpath(xpath));
    String linkTitle = element.getText();
    String link = element.getAttribute("href");

    display("Go to --> " + link);
    driver.get(link);
    display("Checking titles: \nExpected ->" + linkTitle + fotf + "\nActual --->" + driver.getTitle());
    AssertJUnit.assertEquals(linkTitle + fotf, driver.getTitle());
    display("Return to <-- " + baseUrl);
    driver.get(baseUrl);
  }

  public void testLink(String xpath) {
    //Go to link, return
    WebElement element = driver.findElement(By.xpath(xpath));
    AssertJUnit.assertTrue(isAttributePresent(element, "href"));
    display("Go to --> " + element.getAttribute("href"));
    driver.get(element.getAttribute("href"));
    driver.get(baseUrl);
    display("Return to <-- " + baseUrl);
  }

  public void setEnvironmentVariables(String defaultChannel) {
    fotf = " | Focus on the Family";
    String base;
    baseUrl = System.getenv("TEST_SERVER_HOST");

    String endChar = baseUrl.substring(baseUrl.length() -1);

    if (endChar.equals("/")) {
      System.out.println("Changing --> " + baseUrl);
      baseUrl = baseUrl.substring(0 , baseUrl.length() -1);
      System.out.println("to --------> " + baseUrl);
    }

    base = defaultChannel;

    baseTitle = base + fotf;

    String newString = base.replaceAll(" ", "");
    newString = newString.toLowerCase();
    newString = "/" + newString;
    channel =  newString;

  }

  public void startTest(String type) {
    System.out.println("----------------> Starting " + type + " Test: " + baseUrl + ": " + baseTitle);
  }

  public void openHomepage() {
    driver.get(baseUrl + channel);
    AssertJUnit.assertEquals(driver.getTitle().toLowerCase(), baseTitle.toLowerCase());
    driver.manage().window().maximize();
  }

  public void newsletterSignupURL (String link, String path) {
    String expectedTitle = "Focus on the Family eNewsletter";
    String actualTitle;
    String expected = link;
    String actual = driver.findElement(By.xpath(path)).getAttribute("action");
    System.out.println("Testing Newsletter URL");
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    driver.get(actual);
    actualTitle = driver.getTitle();
    System.out.println("Expected Title->" + expectedTitle + "\nActual Title--->" + actualTitle);
    AssertJUnit.assertEquals(expectedTitle, actualTitle);
    driver.get(baseUrl);
  }

  public boolean doesElementExist(String elementPath) {
    boolean elementExists = driver.findElements(By.xpath(elementPath)).size() != 0;
    return elementExists;
  }

  public boolean isAttributePresent(WebElement element, String attribute) {
    // display("Looking for " + "<--" + attribute + "-->");
    boolean attributeExists;
    if (element.getAttribute(attribute) != null) {
      attributeExists = true;
    } else attributeExists = false;

    // if (attributeExists == true) {
    //   display(attribute + "--> found");
    // } else display(attribute + "--> was not found");

    return attributeExists;
  }

  public boolean isTextPresent(WebElement element) {
    //check for any text
    String text = element.getText();
    if (text.length() > 0){
      display("Text present -->\n" + text);
      return true;
    } else {
      display("Text not found");
      return false;
    }
  }

  public void checkDate(String localXpath) {
    WebElement element = driver.findElement(By.xpath(localXpath));
    SimpleDateFormat ft = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);

    String input = element.getText();

    Date date;

    try {
          date = ft.parse(input);
          System.out.println(date);
        } catch (ParseException e) {
          System.out.println("Unparseable using " + ft);
        }

    Date dateNow = new Date();
    System.out.println(dateNow.toString());

  }

  public void closeBanner() {
    driver.manage().window().maximize();
    String bannerPath = "//div[@class='banner--close-button js--banner_close']";
    String closeBannerXpath = "//div[@class='banner--close-button js--banner_close']";
    String campaignBannerXPath = "//div[@class='banner--minimized sticky']";

    if (doesElementExist(bannerPath) == true) {
      String bannerDisplayType = driver.findElement(By.xpath(campaignBannerXPath)).getAttribute("style");
      boolean isBannerOpen = bannerDisplayType.contains("none");

      if (isBannerOpen == true) {
        System.out.println("Closing campaign banner");
        driver.findElement(By.xpath(closeBannerXpath)).click();
      }
    }
  }

  public void clickButton(String buttonPath) {
    driver.findElement(By.xpath(buttonPath)).click();
  }

  public void clickLink(String path) {
    driver.findElement(By.xpath(path)).click();
  }

  public int countElements(String localXpath) {
    localXpath = localXpath + "/*";
    int count = driver.findElements(By.xpath(localXpath)).size();
    return count;
  }

  public void testTopicLinks(String localXpath, String buttonXpath, int numLinks) {

    for (int i = 1; i <= numLinks; i++) {
      clickButton(buttonXpath);

      String linkXpath = localXpath + "/li[" + i + "]/a";

      String linkTitle = driver.findElement(By.xpath(linkXpath)).getText();

      // check links
        driver.get(driver.findElement(By.xpath(linkXpath)).getAttribute("href"));
        System.out.println("Expected Title: " + linkTitle + fotf + " <---> Actual Title: " + driver.getTitle());
        AssertJUnit.assertEquals(driver.getTitle(), linkTitle + fotf);
        driver.get(baseUrl + channel);

        AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

      }
  }

  public boolean isElementPresent(By by) {
    // display("Is element present");
    // display("by = " + by);
    try {
      driver.findElement(by);
      // display("element found");
      return true;
    } catch (NoSuchElementException e) {
      // display("element not found");
      return false;
    }
  }

  public void closeBrowser() {
    driver.close();
  }

  public void quitTest() {
    driver.quit();
  }

  public void display(String content) {
    System.out.println(content);
  }

  public void testChannelLinks() {
    System.out.println("<----------Testing Channel Links ------------>");
    String localXpath = "//nav[@id='main_nav']/ul[2]";
    String[] channelsList = {"Marriage", "Parenting", "Life Challenges", "Faith", "Social Issues"};
    // int numLinks = driver.findElements(By.xpath(localXpath + "/*")).size();
    int numLinks = channelsList.length;

    for (int i =0; i < numLinks; i++) {
      String actual;
      String expected;
      String channel = channelsList[i];
      String channelTitle = channel + fotf;
      int j = i + 1;
      WebElement element = driver.findElement(By.xpath(localXpath + "/li["+j+"]/a"));

      System.out.println("testing -> " + channel + " channel");
      String newString = channel.replaceAll(" ", "");
      newString = newString.toLowerCase();
      newString = "/" + newString;
      channel =  newString;

      expected = baseUrl + channel;
      actual = element.getAttribute("href");
      System.out.println("Expected Channel ->" + expected + "\nActual Channel --->" + actual);
      AssertJUnit.assertEquals(expected, actual);

      driver.get(baseUrl + channel);
      AssertJUnit.assertEquals(driver.getTitle(), channelTitle);
      openMainPage();

    }

  }

  // Main page tests
  public void setMainPageVariables() {
    baseTitle = "Focus on the Family: Helping Families Thrive";
  }

  public void openMainPage() {
    driver.manage().window().maximize();
    driver.get(baseUrl);
    System.out.println("----------> Opening Main page: " + baseUrl);
    AssertJUnit.assertEquals(driver.getTitle(), baseTitle);

  }

  public void testTodaysBroadcast (String localXpath) {
    WebElement localElement;
    String actual;
    String expected;

    display("Testing <--Today's Broadcast-->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    display("header");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[1]/h2")));
    localElement = driver.findElement(By.xpath(localXpath + "/div[1]/h2"));
    expected = "Today's Broadcast";
    actual = localElement.getText();
    System.out.println("Testing Title: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    display("media");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[2]")));
    localElement = driver.findElement(By.id("homepage-broadcast-title"));
    expected = localElement.getText() + fotf;
    driver.get(localElement.getAttribute("href"));
    actual = driver.getTitle();
    System.out.println("Testing Title: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    display("Return to <-- " + baseUrl);
    driver.get(baseUrl);

    display("Today's Broadcast --> Buttons");
    //left button
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[3]")));
    localElement = driver.findElement(By.xpath(localXpath + "/div[3]/div[1]/a"));
    expected = driver.findElement(By.xpath(localXpath + "/div[2]/div/div[1]/div[2]/a")).getText() + fotf;
    driver.get(localElement.getAttribute("href"));
    actual = driver.getTitle();
    System.out.println("Testing Title: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    display("Return to <-- " + baseUrl);
    driver.get(baseUrl);


    //right button
    AssertJUnit.assertTrue(isElementPresent(By.id("homepage-broadcast-secondary")));
    localElement = driver.findElement(By.id("homepage-broadcast-secondary"));
    driver.get(localElement.getAttribute("href"));
    expected = "Focus' Mobile Apps - Focus on the Family";
    actual = driver.getTitle();
    System.out.println("Testing Title: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    display("Return to <-- " + baseUrl);
    driver.get(baseUrl);
  }

  public void testDonateBox(String localXpath) {
    WebElement element;
    String actual;
    String expected;

    System.out.println("Testing <--Donation Box-->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    //Title text
    element = driver.findElement(By.xpath(localXpath + "/div[1]/h2"));
    expected = "YOU can help families thrive!";
    actual = element.getText();
    System.out.println("Testing Title: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    //Main text
    element = driver.findElement(By.xpath(localXpath + "/div[2]/p"));
    AssertJUnit.assertTrue(isTextPresent(element));

    //buttons
    System.out.println("Donation Box --> Buttons");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[3]/div[1]")));
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[3]/div[2]")));

    //button1
    element = driver.findElement(By.xpath(localXpath + "/div[3]/div[1]/a"));
    expected = "http://www.focusonthefamily.com/donate.aspx?refcd=118203";
    actual = element.getAttribute("href");
    System.out.println("Testing Links: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    expected = "DONATE NOW";
    actual = element.getText();
    System.out.println("Testing Button Text: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    //button2
    element = driver.findElement(By.xpath(localXpath + "/div[3]/div[2]/a"));
    expected = "http://www.focusonthefamily.com/pledge?refcd=127701";
    actual = element.getAttribute("href");
    System.out.println("Testing Links: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    expected = "GIVE MONTHLY";
    actual = element.getText();
    System.out.println("Testing Button Text: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

  }

  public void testFreeResourceBox() {
    String localXpath;
    WebElement element;
    String actual;
    String expected;

    display("Testing <--Free Resource-->");
    display("Testing container");
      // Header
    display("Free Resource --> Title");
    localXpath = "//*[@id='homepage-monthresource-title']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    element = driver.findElement(By.xpath(localXpath));
    AssertJUnit.assertTrue(isAttributePresent(element, "href"));
    display("Go to --> " + element.getAttribute("href"));
    driver.get(element.getAttribute("href"));
    driver.get(baseUrl);
    display("Return to <-- " + baseUrl);

    // close campaign banner
    closeBanner();

      //links
    expected = driver.findElement(By.xpath("//*[@id='homepage-monthresource-title']")).getAttribute("href");
    actual = driver.findElement(By.xpath("//*[@id='homepage-monthresource-image']")).getAttribute("href");
    display("Testing links: \nExpected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

      //Image
    display("Testing image");
    localXpath = "//*[@id='homepage-monthresource-image']/img";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    //Free Resource -- Text
    display("Testing text");
    localXpath = "//*[@id='thrivingFamilyContainer']/div[2]/div[2]/span";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    element = driver.findElement(By.xpath(localXpath));
    AssertJUnit.assertTrue(isTextPresent(element));
  }

  public void testContactsBox(String localXpath) {
    WebElement element;
    String actual;
    String expected;

    //testing that elements exists
    display("Testing <--Contact Information-->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    display("Contact Information --> Phone icon");
    localXpath = "//div[@class='icon-phone-icon']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    localXpath = "//*[@id='ContactSection']/div[1]/div[1]/div/div[3]/p";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    localXpath = "//div[@class='icon-letter-icon']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    localXpath = "//*[@id='ContactSection']/div[1]/div[2]/div/div[3]/p";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    // Contact text
    localXpath = "//*[@id='ContactSection']/div[1]/div[1]/div/div[3]/p";
    actual = driver.findElement(By.xpath(localXpath)).getText();
    expected = "1-800-A-FAMILY\n1-800-232-6459\nM-F: 6 a.m. – 8 p.m. MST";
    System.out.println("Expected Contact info -->" + expected + "\n Actual Contact info --> " + actual);
    AssertJUnit.assertEquals(actual, expected);

    // Contact email
    localXpath = "//*[@id='ContactSection']/div[2]";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    localXpath = "//*[@id='ContactSection']/div[2]/strong/a";
    expected = "mailto:%20help@focusonthefamily.com";
    actual = driver.findElement(By.xpath(localXpath)).getAttribute("href");
    System.out.println("Expected email-->" + expected + "\n" + "Actual email---> "+ actual);
    AssertJUnit.assertEquals(actual, expected);
  }

  public void testTabbedPanel(String path) {
    WebElement element;
    String actual;
    String expected;
    String localXpath;
    String[] channelsList = {"Marriage", "Parenting", "Life Challenges", "Social Issues"};

    display("Testing <-- Tabbed Panel -->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(path)));

    int tabCount = 0;

    for (String channel: channelsList) {
      channel = channel.toUpperCase();
      String activeTabPath = "//div[@ng-show='isSet(tabs[" + tabCount + "])']";
      display(channel + " Tab");
      String tabPath = "//a[contains(.,'" + channel + "')]";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(tabPath)));
      element = driver.findElement(By.xpath(tabPath));
      closeBanner();

      setAttribute(driver.findElement(By.xpath(activeTabPath)), "class", "tabpanel media");

      // clickLink(tabPath);
      String tabName = element.getText();
      System.out.println("Expected ->" + channel + "\nActual --->" + tabName);
      AssertJUnit.assertEquals(channel, tabName);

      display("Tabbed Panel -> image and title");
      AssertJUnit.assertTrue(isElementPresent(By.xpath(activeTabPath)));

      localXpath = activeTabPath + "/div[1]/div/div[1]/img";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
      checkImage(localXpath);

      localXpath = activeTabPath + "/div[1]/div/div[2]";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
      testLink(localXpath + "/h2/a");
      closeBanner();
      setAttribute(driver.findElement(By.xpath(activeTabPath)), "class", "tabpanel media");

      // clickLink(tabPath);
      element = driver.findElement(By.xpath(localXpath + "/p"));
      AssertJUnit.assertTrue(isTextPresent(element));

      display("Tabbed Panel --> media subsection");
      localXpath = activeTabPath + "/div[2]/ul";
      int numLinks = countElements(localXpath);
      for (int i = 1; i <= numLinks; i++) {
        String itemType = driver.findElement(By.xpath(localXpath + "/li["+i+"]/div[1]")).getText();
        String itemLink = driver.findElement(By.xpath(localXpath + "/li["+i+"]/div[2]")).getAttribute("href");
        String itemText = driver.findElement(By.xpath(localXpath + "/li["+i+"]/div[2]")).getText();
        display("--> Testing " + itemType + itemText);
        AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/li["+i+"]")));
        if (isLinkExternal(localXpath + "/li[" + i + "]/div[2]") == false) {
          goToLinkAndTestTitle(localXpath + "/li[" + i + "]/div[2]/a");
        } else display("Skipping external link -->" + itemType + itemText);

        closeBanner();
        setAttribute(driver.findElement(By.xpath(activeTabPath)), "class", "tabpanel media");

        // clickLink(tabPath);
      }


      //media - footer
      display("Tabbed Panel --> Media footer");
      localXpath = "//div[@ng-show='isSet(tabs[" + tabCount + "])']";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[3]")));
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[3]/div[2]")));
      element = driver.findElement(By.xpath(localXpath + "/div[3]/div[2]/a"));

      channel = channel.toLowerCase();

      System.out.println("Expected ->" + element.getText() + "\nActual --->" + "See more " + channel);
      AssertJUnit.assertEquals(element.getText(),"See more " + channel);

      channel = "/" + channel.replaceAll(" ", "");

      System.out.println("Expected ->" + element.getAttribute("href") + "\nActual --->"+ baseUrl + channel);
      AssertJUnit.assertEquals(element.getAttribute("href"), baseUrl + channel);

      //Family Store
      display("Tabbed Pannel --> Family Store");
      localXpath = "//div[@ng-show='isSet(tabs[" + tabCount + "])']";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[4]")));
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div[4]/div[1]")));

      tabCount++;
    }

  }

  public void testDalyBlog(String localXpath) {
    WebElement element;
    String actual;
    String expected;

    System.out.println("Testing <--Jim Daly Blog-->");
    closeBanner();
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    System.out.println("Testing title link");
    localXpath = "//a[@id='homepage-featured-blog-title']";
    element = driver.findElement(By.xpath(localXpath));
    expected = "http://jimdaly.focusonthefamily.com/";
    actual = element.getAttribute("href");
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    driver.get(actual);

    expected = "Jim Daly - Daly Focus Blog";
    actual = driver.getTitle();
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    driver.get(baseUrl);
    closeBanner();

    // goToLinkAndTestTitle("//a[@id='homepage-featured-blog-post-tile']");

    element = driver.findElement(By.xpath("//a[@id='homepage-featured-blog-post-tile']"));

    String linkTitle = element.getText();
    String link = element.getAttribute("href");
    display("Testing link");

    display("Go to --> " + link);
    driver.get(link);
    display("Checking titles: \nExpected ->" + linkTitle + " | Jim Daly" + "\nActual --->" + driver.getTitle());
    AssertJUnit.assertEquals(linkTitle + " | Jim Daly", driver.getTitle());
    display("Return to <-- " + baseUrl);
    driver.get(baseUrl);

    element = driver.findElement(By.xpath("//a[@id='homepage-featured-blog-post-tile']"));

    AssertJUnit.assertTrue(isTextPresent(element));

    System.out.println("Testing title text");
    expected = "DalyFocus: Jim Daly's Blog";
    actual = driver.findElement(By.xpath(localXpath)).getText();
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

  }

  public void testSlideshow(String localXpath) {
    WebElement element;
    String actual;
    String expected;

    System.out.println("Testing <--Slideshow-->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath + "/div")));

    for (int i = 1; i <= 4; i++) {
      localXpath = "//*[@id='SliderSection']/div/div/div[" + i + "]";
      AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
      testLink(localXpath + "/a");
      checkImage(localXpath + "/a/img");
    }
    AssertJUnit.assertTrue(isElementPresent(By.xpath("//*[@id='SliderSection']/div/div/div[5]")));

  }

  public void testFooterAd(String localXpath) {
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
  }

  //Header and footer tests
  public void testHeader(String path){
    WebElement element;
    String expected;
    String actual;
    String actualLink;
    String expectedLink;
    String localXpath = path;
    display("<-------------------- Testing Header -------------------->");
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    display("<--------- Testing Header Image ---------->");
    localXpath = path + "/div/h1/a";
    display(localXpath);
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    testLink(localXpath);

    // Not checking image
    // checkImage(localXpath + "/i");
    // Slogan
    localXpath = path + "/div/h2";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    // not checking image
    // checkImage(localXpath);

    //Utility links
    display("<---------- Testing utility links ---------->");
    localXpath = path + "/div/nav/ul[1]";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    int count = countElements(localXpath);
    AssertJUnit.assertEquals(count, 4);

    //Todays Broadcast
    localXpath = path + "/div/nav/ul[1]/li[1]/a";
    element = driver.findElement(By.xpath(localXpath));
    expected = element.getText();
    actual = "TODAY'S BROADCAST";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    actualLink = element.getAttribute("href");
    expectedLink = baseUrl + "/media/daily-broadcast/latest";
    System.out.println("Expected ->" + expectedLink + "\nActual --->" + actualLink);
    AssertJUnit.assertEquals(expectedLink, actualLink);
    testLink(localXpath);

    //Family Help
    localXpath = path + "/div/nav/ul[1]/li[2]/a";
    element = driver.findElement(By.xpath(localXpath));
    expected = element.getText();
    actual = "FAMILY HELP";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    actualLink = element.getAttribute("href");
    expectedLink = "http://family.custhelp.com/app/home";
    System.out.println("Expected ->" + expectedLink + "\nActual --->" + actualLink);
    AssertJUnit.assertEquals(expectedLink, actualLink);
    testLink(localXpath);

    //About
    localXpath = path + "/div/nav/ul[1]/li[3]/a";
    element = driver.findElement(By.xpath(localXpath));
    expected = element.getText();
    actual = "ABOUT";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    actualLink = element.getAttribute("href");
    expectedLink = "http://www.focusonthefamily.com/about_us.aspx";
    System.out.println("Expected ->" + expectedLink + "\nActual --->" + actualLink);
    AssertJUnit.assertEquals(expectedLink, actualLink);
    testLink(localXpath);

    //STORE
    localXpath = path + "/div/nav/ul[1]/li[4]/a";
    element = driver.findElement(By.xpath(localXpath));
    expected = element.getText();
    actual = "STORE";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    actualLink = element.getAttribute("href");
    expectedLink = "http://store.focusonthefamily.com/?p=1143702";
    System.out.println("Expected ->" + expectedLink + "\nActual --->" + actualLink);
    AssertJUnit.assertEquals(expectedLink, actualLink);
    testLink(localXpath);

    // search button
    display("<----------Testing Search Box --------->");
    path = "//*[@id='search']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(path)));
    localXpath = path + "/form/div/input[1]";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    element = driver.findElement(By.name("q"));
    element.sendKeys("testing");
    element.submit();
    String testString = "testing";
    expected = "Focus on the Family Search Results: " + testString;
    actual = driver.getTitle();
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    driver.get(baseUrl);

    //checking for search button
    AssertJUnit.assertTrue(isElementPresent(By.xpath(path + "/form/div/button")));

    // check channel links
    testChannelLinks();
    // pro life channel is not included in above test.  The link is hard coded to prod.
    // localXpath = "//nav[@id='main_nav']/ul[2]/li[6]";
    // String channel = "Pro-life";
    // String channelTitle = channel + fotf;
    //
    // System.out.println("testing -> " + channel + " channel");
    // String newString = channel.replaceAll(" ", "");
    // newString = newString.toLowerCase();
    // newString = "/" + newString;
    // channel =  newString;
    //
    // expected = "http://www.focusonthefamily.com/pro-life";
    // actual = driver.findElement(By.xpath(localXpath + "/a")).getAttribute("href");
    // System.out.println("Expected Channel ->" + expected + "\nActual Channel --->" + actual);
    // AssertJUnit.assertEquals(expected, actual);
    //
    // driver.get(baseUrl + channel);
    // AssertJUnit.assertEquals(driver.getTitle(), channelTitle);
    // openMainPage();

    //Check donate button
    display("<---------- Testing donate button ---------->");
    localXpath = "//div[@class='donation_message--container']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    element = driver.findElement(By.xpath(localXpath + "/a"));
    actualLink = element.getAttribute("href");
    // expectedLink = "http://www.focusonthefamily.com/donate.aspx?refcd=122905";
    // System.out.println("Expected ->" + expectedLink + "\nActual --->" + actualLink);
    // AssertJUnit.assertEquals(expectedLink, actualLink);

    driver.get(actualLink);
    expected = "Donate to Focus on the Family";
    actual = driver.getTitle();
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    driver.get(baseUrl);

    //donate button text and text
    element = driver.findElement(By.xpath(localXpath + "/a/span[1]"));
    actual = element.getText();
    expected = "DONATE NOW";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

  }

  public void testFooter(String path) {
    WebElement element;
    String expected;
    String actual;
    String localXpath = path;

    display("<-------------------- Footer Test -------------------->");

    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    //logo
    display("<---------- Testing Logo ---------->");
    localXpath = path + "/div[1]/div/div[1]/a";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    element = driver.findElement(By.xpath(localXpath));
    actual = element.getAttribute("href");
    expected = "http://www.focusonthefamily.com/";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    driver.get(actual);
    actual = driver.getTitle();
    expected = "Focus on the Family: Helping Families Thrive";
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);
    driver.get(baseUrl);

    //newsletter URL
    expected = "https://connect.focusonthefamily.com/aprimo/AudienceUpdate.aspx?D=b9ca57b2fbe8cb42458807853387983f6a0f6be5ccdab113&T=1&A=83f2ca9b0cad5323&F=d0f05528368ce09a&M=b333208886639fd5&G=951c0aad4f451d07ded428ddc25201eb&FromSite=FOTF";
    localXpath = path + "/div[1]/div/form";
    newsletterSignupURL(expected, localXpath);

    //test Footer ad
    display("<---------- Testing Footer Ad ---------->");
    testFooterAd("//div[@id='footer-ad-container']/div/iframe");

    //test Utility links
    display("<---------- Testing Utility Links ---------->");
    localXpath = "//*[@class='footer_bottom--nav']/ul";
    int numLinks = countElements(localXpath);
    AssertJUnit.assertEquals(numLinks, 4);

    for (int i = 0; i < numLinks; i++){
      int j = i + 1;
      WebElement navElement;
      String[] linksList ={"FAQs", "Privacy & Terms", "En Español", "Need Help? Call 1-800-A-FAMILY (232-6459)"};
      String[] linksListTitle ={"Support Home Page", "Focus on the Family Policies | Focus on the Family", "Enfoque a la Familia", "How do I contact Focus on the Family?"};

      navElement = driver.findElement(By.xpath(localXpath + "/li[" + j + "]/a"));
      expected = linksList[i];
      actual = navElement.getText();
      System.out.println("Expected ->" + expected + "\nActual --->" + actual);
      AssertJUnit.assertEquals(expected, actual);

      String value = navElement.getAttribute("target");
      String localLink = navElement.getAttribute("href");

      driver.get(localLink);
      String expectedTitle = linksListTitle[i];
      String actualTitle = driver.getTitle();
      System.out.println("Expected ->" + expectedTitle + "\nActual --->" + actualTitle);
      AssertJUnit.assertEquals(expectedTitle, actualTitle);

      driver.get(baseUrl);

    }

    //Copyright
    display("<---------- Testing Copyright ---------->");
    localXpath = "//p[2][@class='footer_bottom--copyright']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));
    element = driver.findElement(By.xpath(localXpath));
    expected = "© 1997–2016 Focus on the Family";
    actual = element.getText();
    System.out.println("Expected ->" + expected + "\nActual --->" + actual);
    AssertJUnit.assertEquals(expected, actual);

    //Check link containers
    display("<---------- Testing Container ---------->");
    localXpath = "//ul[@class='footer_nav--list']";
    AssertJUnit.assertTrue(isElementPresent(By.xpath(localXpath)));

    String[] linkContainers = {"Donate", "Shop", "Media", "About", "Popular Websites"};

    System.out.println("Expected # Links ->" + 5 + "\nActual # Links --->" + countElements(localXpath));
    assertEquals(linkContainers.length, 5);

    for (int i = 0; i < 5; i++) {
      int j = i + 1;
      WebElement navElement = driver.findElement(By.xpath(localXpath + "/li[" +j+"]/a"));

      expected = linkContainers[i];
      actual = navElement.getText();

      System.out.println("Expected ->" + expected + "\nActual --->" + actual);
      AssertJUnit.assertEquals(expected, actual);

      int numSubLinks = countElements(localXpath + "/li[" + j + "]/ul");

      for (int x = 1; x <= numSubLinks; x++){
        String linkXpath = localXpath + "/li[" + j + "]/ul/li[" + x + "]/a";
        element = driver.findElement(By.xpath(linkXpath));
        String title = element.getText();
        display("testing " + expected + "--> " + title);

        // tests for working links, does not check titles
        testLink(linkXpath);
      }
    }

    //Social Media buttons
    display("<---------- Testing Social Media Buttons ---------->");
    String[] socialMediaLinks = {"FaceBook", "Twitter", "Google +", "Email", "YouTube"};
    String[] socialMediaTitles = {"Focus on the Family", "Focus on the Family (@FocusFamily) | Twitter", "Focus on the Family - Videos - Google+", "Questions / Feedback", " Focus on the Family  - YouTube"};

    localXpath = "//div[@id='footer-social']";
    numLinks = countElements(localXpath + "/a");

    AssertJUnit.assertEquals(5, numLinks);

    for (int i = 1; i < numLinks; i++) {
      display("Testing --> " + socialMediaLinks[i-1]);
      element = driver.findElement(By.xpath(localXpath + "/a[" + i + "]"));
      driver.get(element.getAttribute("href"));
      String expectedTitle = socialMediaTitles[i-1];
      String actualTitle = driver.getTitle();
      System.out.println("Expected ->" + expectedTitle + "\nActual --->" + actualTitle);
      AssertJUnit.assertEquals(expectedTitle, actualTitle);
      driver.get(baseUrl);
    }
  }
}
