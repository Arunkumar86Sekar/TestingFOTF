package BSsmoketest;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

// import java.io.File;
// import java.io.IOException;
// import java.net.URL;
//
// import org.apache.commons.io.FileUtils;
// import org.openqa.selenium.By;
// import org.openqa.selenium.OutputType;
// import org.openqa.selenium.Platform;
// import org.openqa.selenium.TakesScreenshot;
// import org.openqa.selenium.WebDriver;
// import org.openqa.selenium.WebElement;
// import org.openqa.selenium.remote.Augmenter;
// import org.openqa.selenium.remote.DesiredCapabilities;
// import org.openqa.selenium.remote.RemoteWebDriver;

public class FOTFsmoketest {

  // private WebDriver driver;
  private SiteTests test = new SiteTests();

  @BeforeClass
  public void setUp() throws Exception {
    test.setUpBrowserStack();
  }

  // public void setUp() throws Exception {
  //   DesiredCapabilities capability = new DesiredCapabilities();
  //   capability.setPlatform(Platform.WINDOWS);
  //   capability.setCapability("build", "TestNG - Sample");
  //   capability.setCapability("browserstack.local", "true");
  //
  //   driver = new RemoteWebDriver(
  //     new URL("https://austinjones5:r16JqkpWrqwoULicr9KG@hub-cloud.browserstack.com/wd/hub"),
  //     capability
  //   );
  // }

  // @Test
  // public void testSimple() throws Exception {
  //   driver.get("http://testpub.focusonthefamily.com/");
  //   System.out.println("Page title is: " + driver.getTitle());
  //   // WebElement element = driver.findElement(By.name("q"));
  //   // element.sendKeys("BrowserStack");
  //   // element.submit();
  //   driver = new Augmenter().augment(driver);
  //   File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
  //   try {
  //     FileUtils.copyFile(srcFile, new File("Screenshot.png"));
  //   } catch (IOException e) {
  //     e.printStackTrace();
  //   }
  // }
  @Test
  public void mainTest() throws Exception {
    String testType = "<--Main Page-->";
    test.setEnvironmentVariables("Main Page");

    test.setMainPageVariables();

    test.startTest(testType);

    test.openMainPage();

    // test.checkDate("//div[@id='BlogSection']/div/div/div/div[2]/div[2]");

    test.testFooter("//*[@class='site--footer']");

    test.testHeader("//*[@class='site--header']");

    test.testSlideshow("//*[@id='SliderSection']");

    test.closeBanner();

    test.testChannelLinks();

    test.testTabbedPanel("//*[@id='ChannelSection']");

    test.testContactsBox("//div[@id='thrivingFamilyContainer']");

    test.testDonateBox("//div[@id='helpingFamily']");

    test.testTodaysBroadcast("//div[@id='todaysBroadcast']");

    test.testFreeResourceBox();

    test.testDalyBlog("//div[@id='BlogSection']");

    test.testFooterAd("//div[@id='footer-ad-container']/div/iframe");

    test.closeBrowser();
  }

  @AfterClass
  public void tearDown() throws Exception {
    test.quitTest();
  }

}
