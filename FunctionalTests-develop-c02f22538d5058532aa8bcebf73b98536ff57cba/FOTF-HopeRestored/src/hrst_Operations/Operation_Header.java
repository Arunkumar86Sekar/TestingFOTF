package hrst_Operations;

import hrst_Config.Basedriver;
import hrst_DDF.CommonComponentDataSource;
import hrst_DDF.DraftInput;
import hrst_Objects.WebElements_Header;

import org.openqa.selenium.By;
import org.testng.Assert;

public class Operation_Header extends Basedriver{
	
	public static void VerifyHRST_HeaderPullLeft(){
		String actualPullLeft="";
		try{
			actualPullLeft=RM.GetAttributeByWebelement(headerObj.hPullLeft, "class");
			Assert.assertEquals(actualPullLeft, RM.RemoveSpace(expectedData.getProperty("hPullLeft")));
			log.info("The Actual Hope Restored Header Sticky header - "+actualPullLeft);
			log.info("The Expected Hope Restored Header Sticky header - "+RM.RemoveSpace(expectedData.getProperty("hPullLeft")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Header Sticky header are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header Sticky header - "+actualPullLeft);
			log.error("The Expected Hope Restored Header Sticky header - "+RM.RemoveSpace(expectedData.getProperty("hPullLeft")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header Sticky header are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header Sticky header element");
		}
	}

	public static void VerifyHRST_HeaderPullRight(){
		String actualPullRight="";
		try{
			actualPullRight=RM.GetAttributeByWebelement(headerObj.hPullRight, "class");
			Assert.assertEquals(actualPullRight, RM.RemoveSpace(expectedData.getProperty("hPullRight")));
			log.info("The Actual Hope Restored Header Sticky header - "+actualPullRight);
			log.info("The Expected Hope Restored Header Sticky header - "+RM.RemoveSpace(expectedData.getProperty("hPullRight")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Header Sticky header are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header Sticky header - "+actualPullRight);
			log.error("The Expected Hope Restored Header Sticky header - "+RM.RemoveSpace(expectedData.getProperty("hPullRight")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header Sticky header are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header Sticky header element");
		}
	}
	
	public static void VerifyHRST_HeaderLogo(){
		String actualLogo="";
		try{
			actualLogo=RM.GetAttributeByWebelement(headerObj.hLogo, "alt");
			Assert.assertEquals(actualLogo, RM.RemoveSpace(expectedData.getProperty("hLogo")));
			log.info("The Actual Hope Restored Header Logo - "+actualLogo);
			log.info("The Expected Hope Restored Header Logo - "+RM.RemoveSpace(expectedData.getProperty("hLogo")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Header Logo are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header Logo - "+actualLogo);
			log.error("The Expected Hope Restored Header Logo - "+RM.RemoveSpace(expectedData.getProperty("hLogo")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header Logo are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header Logo element");
		}
	}
	
	public static void VerifyHRST_HeaderMenuItems(){
		String actualMenuItems="";
		int menuItems=0;
		try{
			for(menuItems=1; menuItems<=4; menuItems++){
				actualMenuItems=RM.GetAttributeByString(headerObj.hMenuItems + menuItems +"]/a", "href");
				Assert.assertEquals(actualMenuItems, "http://" + server + RM.RemoveSpace(expectedData.getProperty("hMenuItems" + menuItems)));
				log.info("The Actual Hope Restored Header MenuItems - "+actualMenuItems);
				log.info("The Expected Hope Restored Header MenuItems - "+"http://" + server + RM.RemoveSpace(expectedData.getProperty("hMenuItems" + menuItems)));
				log.info("TEST PASSED: The Actual and Expected Hope Restored Header MenuItems are Same");
			}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header MenuItems - "+actualMenuItems);
			log.error("The Expected Hope Restored Header MenuItems - "+RM.RemoveSpace(expectedData.getProperty("hMenuItems" + menuItems)));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header MenuItems are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header MenuItems element");
		}
	}

	public static void VerifyHRST_HeaderMenuItemUrl(){
		String actualMenuItems="";
		try{
				actualMenuItems=RM.GetAttributeByString(headerObj.hMainMenuUrl, "href");
				Assert.assertEquals(actualMenuItems, "http://"+server+DraftInput.mainUrl.toLowerCase());
				log.info("The Actual Hope Restored Header MenuItems - "+actualMenuItems);
				log.info("The Expected Hope Restored Header MenuItems - "+ DraftInput.mainUrl);
				log.info("TEST PASSED: The Actual and Expected Hope Restored Header MenuItems are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header MenuItems - "+actualMenuItems);
			log.error("The Expected Hope Restored Header MenuItems - "+DraftInput.mainUrl);
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header MenuItems are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header MenuItems element");
		}
	}
	
	public static void VerifyHRST_HeaderMenuItemText(){
		String actualMainMenuText="";
		try{
			actualMainMenuText=Driver.findElement(By.xpath(headerObj.hMainMenuUrl)).getText();
			actualMainMenuText=RM.RemoveSpace(actualMainMenuText);
			Assert.assertEquals(actualMainMenuText, DraftInput.mainText.toUpperCase());
			log.info("The Actual Hope Restored Header Main Menu Text - "+actualMainMenuText);
			log.info("The Expected Hope Restored Header Main Menu Text - "+RM.RemoveSpace(DraftInput.mainText.toUpperCase()));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Header Main Menu Text are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Header Main Menu Text - "+actualMainMenuText);
			log.error("The Expected Hope Restored Header Main Menu Text - "+RM.RemoveSpace(DraftInput.mainText.toUpperCase()));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Header Main Menu Text are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Header Main Menu Text element");
		}
	}

	public static void VerifyHRST_FooterPrivacyUrl(){
		String actualFooterPrivacyUrl="";
		try{
			actualFooterPrivacyUrl=RM.GetAttributeByWebelement(headerObj.FooterPrivacy, "href");
			Assert.assertEquals(actualFooterPrivacyUrl, CommonComponentDataSource.privacySitecoreUrlValue);
			log.info("The Actual Hope Restored Footer Privacy Url - "+actualFooterPrivacyUrl);
			log.info("The Expected Hope Restored Footer Privacy Url - "+CommonComponentDataSource.privacySitecoreUrlValue);
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer Privacy Url are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer Privacy Url - "+actualFooterPrivacyUrl);
			log.error("The Expected Hope Restored Footer Privacy Url - "+CommonComponentDataSource.privacySitecoreUrlValue);
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer Privacy Url are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer Privacy Url element");
		}
	}

	public static void VerifyHRST_FooterPrivacyText(){
		String actualFooterPrivacyText="";
		try{
			actualFooterPrivacyText=headerObj.FooterPrivacy.getText();
			Assert.assertEquals(actualFooterPrivacyText, CommonComponentDataSource.privacySitecoreTextValue);
			log.info("The Actual Hope Restored Footer Privacy Text - "+actualFooterPrivacyText);
			log.info("The Expected Hope Restored Footer Privacy Text - "+CommonComponentDataSource.privacySitecoreTextValue);
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer Privacy Text are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer Privacy Text - "+actualFooterPrivacyText);
			log.error("The Expected Hope Restored Footer Privacy Text - "+CommonComponentDataSource.privacySitecoreTextValue);
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer Privacy Text are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer Privacy Text element");
		}
	}
	
	public static void VerifyHRST_FooterFocusLogoImg(){
		String actualFooterFocusLogoImg="";
		try{
			actualFooterFocusLogoImg=RM.GetAttributeByWebelement(headerObj.FooterFocusLogo, "src");
			boolean result=actualFooterFocusLogoImg.indexOf(CommonComponentDataSource.focusLogoSitecoreImgValue) != -1;
			//actualFooterFocusLogoImg=actualFooterFocusLogoImg.substring(CommonComponentDataSource.focusLogoSitecoreImgValue.length()-actualFooterFocusLogoImg.length());
			Assert.assertEquals(true, result);
			log.info("The Actual Hope Restored Footer FocusLogoImg - "+actualFooterFocusLogoImg);
			log.info("The Expected Hope Restored Footer FocusLogoImg - "+CommonComponentDataSource.focusLogoSitecoreImgValue);
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer FocusLogoImg are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer FocusLogoImg - "+actualFooterFocusLogoImg);
			log.error("The Expected Hope Restored Footer FocusLogoImg - "+CommonComponentDataSource.focusLogoSitecoreImgValue);
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer FocusLogoImg are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer FocusLogoImg element");
		}
	}
	
	public static void VerifyHRST_FooterhopeLogoImg(){
		String actualFooterhopeLogoImg="";
		try{
			actualFooterhopeLogoImg=RM.GetAttributeByWebelement(headerObj.FooterhopeLogo, "src");
			boolean result=actualFooterhopeLogoImg.indexOf(CommonComponentDataSource.hopeLogoSitecoreImgValue) != -1;
			actualFooterhopeLogoImg=actualFooterhopeLogoImg.substring(CommonComponentDataSource.hopeLogoSitecoreImgValue.length()-actualFooterhopeLogoImg.length());
			Assert.assertEquals(true, result);
			log.info("The Actual Hope Restored Footer hopeLogoImg - "+actualFooterhopeLogoImg);
			log.info("The Expected Hope Restored Footer hopeLogoImg - "+getData.GetPrivacySitecoreText());
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer hopeLogoImg are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer hopeLogoImg - "+actualFooterhopeLogoImg);
			log.error("The Expected Hope Restored Footer hopeLogoImg - "+getData.GetPrivacySitecoreText());
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer hopeLogoImg are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer hopeLogoImg element");
		}
	}
}