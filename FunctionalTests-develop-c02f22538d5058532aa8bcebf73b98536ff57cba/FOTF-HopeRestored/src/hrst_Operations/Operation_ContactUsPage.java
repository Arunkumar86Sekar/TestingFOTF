package hrst_Operations;

import java.util.List;

import hrst_Config.Basedriver;
import hrst_Objects.WebElements_Header;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Operation_ContactUsPage extends Basedriver{
	
	public static void VerifyContactItems(){
		String item="";
		int i = 1;
		try{
				for(i=1; i<=4; i++){
					item=contactUsObj.contactUsItems.get(i).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("contactSingleLineItems"));
					log.info("The Actual Contact Us item: "+ item);
					log.info("The Expected Contact Us item "+i+"- "+ expectedData.getProperty("contactSingleLineItems"));
					log.info("TEST PASSED: The Actual and Expected Hope Restored Home Contact Us items are Same");
				}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Home Contact Us item- "+item);
			log.error("The Expected Hope Restored Home Contact Us item "+i+"- "+ expectedData.getProperty("contactSingleLineItems"));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home Contact Us are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home Contact Us element");
		}
		catch(IndexOutOfBoundsException e){
			log.error("TEST FAILED: There is no Hope Restored Home Contact Us element");
		}
	}
	
	public static void VerifyHRST_contactUsTitle(){
		String actualContactUsTitle = null;
		try{
			actualContactUsTitle=RM.GetAttributeByWebelement(contactUsObj.contactUsTitle, "class");
			Assert.assertEquals(actualContactUsTitle, RM.RemoveSpace(expectedData.getProperty("contactUsTitle")));
			log.info("The Actual Hope Restored Contact Us Title  - "+actualContactUsTitle);
			log.info("The Expected Hope Restored Contact Us Title  - "+RM.RemoveSpace(expectedData.getProperty("contactUsTitle")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Us Title  are Same");
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Us Title  - "+actualContactUsTitle);
			log.error("The Expected Hope Restored Contact Us Title  - "+RM.RemoveSpace(expectedData.getProperty("contactUsTitle")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Us Title  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Us Title  element");
		}
	}
	
	public static void VerifyHRST_contactUsSubmitButton(){
		String actualContactUsSubmitButton = null;
		String expectedItems="";
		try{
			actualContactUsSubmitButton=RM.GetAttributeByWebelement(contactUsObj.contactUsSubmitButton, "class");
			Assert.assertEquals(actualContactUsSubmitButton, RM.RemoveSpace(expectedData.getProperty("contactUsSubmitButton")));
			log.info("The Actual Hope Restored Contact Us Submit Button  - "+actualContactUsSubmitButton);
			log.info("The Expected Hope Restored Contact Us Submit Button  - "+RM.RemoveSpace(expectedData.getProperty("contactUsSubmitButton")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Us Submit Button  are Same");
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Us Submit Button  - "+actualContactUsSubmitButton);
			log.error("The Expected Hope Restored Contact Us Submit Button  - "+RM.RemoveSpace(expectedData.getProperty("contactUsSubmitButton")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Us Submit Button  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Us Submit Button  element");
		}
	}
	
	public static void VerifyHRST_contactUsCaptchaLabel(){
		String actualContactUsCaptchaLabel = null;
		String expectedItems="";
		try{
			actualContactUsCaptchaLabel=RM.GetAttributeByWebelement(contactUsObj.contactUsCaptchaLabel, "class");
			Assert.assertEquals(actualContactUsCaptchaLabel, RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaLabel")));
			log.info("The Actual Hope Restored Contact Us Captcha Label  - "+actualContactUsCaptchaLabel);
			log.info("The Expected Hope Restored Contact Us Captcha Label  - "+RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaLabel")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Us Captcha Label  are Same");
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Us Captcha Label  - "+actualContactUsCaptchaLabel);
			log.error("The Expected Hope Restored Contact Us Captcha Label  - "+RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaLabel")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Us Captcha Label  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Us Captcha Label  element");
		}
	}
	
	public static void VerifyHRST_contactUsCaptchaTextbox(){
		String actualContactUsCaptchaTextbox = null;
		String expectedItems="";
		try{
			actualContactUsCaptchaTextbox=RM.GetAttributeByWebelement(contactUsObj.contactUsCaptchaTextbox, "class");
			Assert.assertEquals(actualContactUsCaptchaTextbox, RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaTextbox")));
			log.info("The Actual Hope Restored Contact Us Captcha Textbox  - "+actualContactUsCaptchaTextbox);
			log.info("The Expected Hope Restored Contact Us Captcha Textbox  - "+RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaTextbox")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Us Captcha Textbox  are Same");
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Us Captcha Textbox  - "+actualContactUsCaptchaTextbox);
			log.error("The Expected Hope Restored Contact Us Captcha Textbox  - "+RM.RemoveSpace(expectedData.getProperty("contactUsCaptchaTextbox")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Us Captcha Textbox  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Us Captcha Textbox  element");
		}
	}
	
	public static void VerifyHRST_contactUsMultiLineTextbox(){
		String actualContactUsMultiLineTextbox = null;
		String expectedItems="";
		try{
			actualContactUsMultiLineTextbox=RM.GetAttributeByWebelement(contactUsObj.contactUsMultiLineTextBox, "class");
			Assert.assertEquals(actualContactUsMultiLineTextbox, RM.RemoveSpace(expectedData.getProperty("contactUsMultiLineTextbox")));
			log.info("The Actual Hope Restored Contact Us MultiLine Textbox  - "+actualContactUsMultiLineTextbox);
			log.info("The Expected Hope Restored Contact Us MultiLine Textbox  - "+RM.RemoveSpace(expectedData.getProperty("contactUsMultiLineTextbox")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Us MultiLine Textbox  are Same");
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Us MultiLine Textbox  - "+actualContactUsMultiLineTextbox);
			log.error("The Expected Hope Restored Contact Us MultiLine Textbox  - "+RM.RemoveSpace(expectedData.getProperty("contactUsMultiLineTextbox")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Us MultiLine Textbox  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Us MultiLine Textbox  element");
		}
	}
}
