package hrst_Operations;

import hrst_Config.Basedriver;
import hrst_Objects.WebElements_Footer ;

import org.openqa.selenium.By;
import org.testng.Assert;

public class Operation_Footer extends Basedriver{
	
	public static void VerifyHRST_FooterWidgetLeft(){
		String actualWidgetLeft="";
		try{
			actualWidgetLeft=RM.GetAttributeByWebelement(footerObj.fWidgetLeft, "alt");
			Assert.assertEquals(actualWidgetLeft, RM.RemoveSpace(expectedData.getProperty("fWidgetLeft")));
			log.info("The Actual Hope Restored Footer  Widget Left - "+actualWidgetLeft);
			log.info("The Expected Hope Restored Footer  Widget Left - "+RM.RemoveSpace(expectedData.getProperty("fWidgetLeft")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer  Widget Left are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer  Widget Left - "+actualWidgetLeft);
			log.error("The Expected Hope Restored Footer  Widget Left - "+RM.RemoveSpace(expectedData.getProperty("fWidgetLeft")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer  Widget Left are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer  Widget Left element");
		}
	}
	
	public static void VerifyHRST_FooterWidgetRight(){
		String actualWidgetRight="";
		try{
			actualWidgetRight=RM.GetAttributeByWebelement(footerObj.fWidgetRight, "alt");
			Assert.assertEquals(actualWidgetRight, RM.RemoveSpace(expectedData.getProperty("fWidgetRight")));
			log.info("The Actual Hope Restored Footer  Widget Right - "+actualWidgetRight);
			log.info("The Expected Hope Restored Footer  Widget Right - "+RM.RemoveSpace(expectedData.getProperty("fWidgetRight")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer  Widget Right are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer  Widget Right - "+actualWidgetRight);
			log.error("The Expected Hope Restored Footer  Widget Right - "+RM.RemoveSpace(expectedData.getProperty("fWidgetRight")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer  Widget Right are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer  Widget Right element");
		}
	}
	
	public static void VerifyHRST_FooterWidgetInfo(){
		boolean actualWidgetInfo = false;		
		try{
			actualWidgetInfo=RM.IsDisplayedByWebelement(footerObj.fWidgetInfo);
			Assert.assertEquals(actualWidgetInfo, true);
			log.info("The Actual Hope Restored Footer  Widget Info - <" + RM.findAnElement(footerObj.fWidgetInfo).getTagName() + ">");
			log.info("The Expected Hope Restored Footer  Widget Info - " + "<p>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer  Widget Info are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer  Widget Info - <" + RM.findAnElement(footerObj.fWidgetInfo).getTagName() + ">");
			log.error("The Expected Hope Restored Footer  Widget Info - " + "<p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer  Widget Info are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer  Widget Info element");
		}
	}
	
	public static void VerifyHRST_FooterWidgetPullRight(){
		String actualWidgetPullRight="";
		try{
			actualWidgetPullRight=RM.GetAttributeByWebelement(footerObj.fWidgetPullRight, "href");
			Assert.assertEquals(actualWidgetPullRight, RM.RemoveSpace(expectedData.getProperty("fWidgetPullRight")));
			log.info("The Actual Hope Restored Footer  Widget PullRight - "+actualWidgetPullRight);
			log.info("The Expected Hope Restored Footer  Widget PullRight - "+RM.RemoveSpace(expectedData.getProperty("fWidgetPullRight")));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Footer  Widget PullRight are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Footer  Widget PullRight - "+actualWidgetPullRight);
			log.error("The Expected Hope Restored Footer  Widget PullRight - "+RM.RemoveSpace(expectedData.getProperty("fWidgetPullRight")));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Footer  Widget PullRight are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Footer  Widget PullRight element");
		}
	}
}
