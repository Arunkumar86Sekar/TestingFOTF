package hrst_Operations;

import java.util.List;

import hrst_Config.Basedriver;
import hrst_Objects.WebElements_Header;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Operation_AboutPage extends Basedriver{
	
	public static void VerifyHrParallaxItems(){
		String item="";
		int i = 1;
		try{
				for(i=0; i<=1; i++){
					item=aboutObj.hrParallax.get(i).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("hrParallaxItems"+i));
					log.info("The Actual Hr Parallax item: "+ item);
					log.info("The Expected Hr Parallax item- "+expectedData.getProperty("hrParallaxItems"+i));
					log.info("TEST PASSED: The Actual and Expected Hope Restored Home Hr Parallax items are Same");
				}
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Home Hr Parallax item- "+item);
			log.error("The Expected Hope Restored Home Hr Parallax item- "+expectedData.getProperty("hrParallaxItems"+i));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home Hr Parallax are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home Hr Parallax element");
		}
	}
	
	public static void VerifyHrContentTextItems(){
		String item="";
		int i =0;
		int totalItem;
		try{
			totalItem=aboutObj.hrContentText.size()-1;
				for(i=0; i<=totalItem; i++){
					item=aboutObj.hrContentText.get(i).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("hrContentTextItems"));
					log.info("The Actual Hr ContentText item: "+ item);
					log.info("The Expected Hr ContentText item- "+expectedData.getProperty("hrContentTextItems"));
					log.info("TEST PASSED: The Actual and Expected Hope Restored Home Hr ContentText items are Same");
				}
			
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Home Hr ContentText item- "+ item);
			log.error("The Expected Hope Restored Home Hr ContentText item- "+expectedData.getProperty("hrContentTextItems"));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home Hr ContentText are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home Hr ContentText element");
		}
	}
	
	public static void VerifyHRST_AboutHrTwoColItemsItems(){
		String item="";
		int j = 0;
		int totalCount;
		try{
			totalCount=aboutObj.aboutHrTwoColItems.size()-1;
				for(j=0; j<=totalCount; j++){
					item=aboutObj.aboutHrTwoColItems.get(j).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("aboutHrTwoColItem"+j));
					log.info("The Actual Module Box Wrapper item: "+ item);
					log.info("The Expected Module Box Wrapper item- "+expectedData.getProperty("aboutHrTwoColItem"+j));
					log.info("TEST PASSED: The Actual and Expected Hope Restored About Hr Two Col items are Same");
				}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored About Hr Two Col Items item- "+item);
			log.error("The Expected Hope Restored About Hr Two Col Items item- "+expectedData.getProperty("aboutHrTwoColItem"+j));
			log.error("TEST FAILED: The Actual and Expected Hope Restored About Hr Two Col Items are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored About Hr Two Col Items element");
		}
	}
	
	public static void VerifyHRST_AboutHrTwoImageGallery(){
		String item="";
		int j = 0;
		int totalCount;
		try{
			totalCount=aboutObj.aboutHrTwoColImageGallery.size()-1;
				for(j=0; j<=totalCount; j++){
					item=aboutObj.aboutHrTwoColImageGallery.get(j).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("aboutHrTwoColImageGallery"));
					log.info("The Actual Module Box Wrapper item: "+ item);
					log.info("The Expected Module Box Wrapper item- "+expectedData.getProperty("aboutHrTwoColImageGallery"));
					log.info("TEST PASSED: The Actual and Expected Hope Restored About Hr Two Image Galleryitems are Same");
				}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored About Hr Two Image GalleryItems item- "+item);
			log.error("The Expected Hope Restored About Hr Two Image GalleryItems item- "+expectedData.getProperty("aboutHrTwoColImageGallery"));
			log.error("TEST FAILED: The Actual and Expected Hope Restored About Hr Two Image GalleryItems are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored About Hr Two Image GalleryItems element");
		}
	}
	
	public static void VerifyHRST_HrTwoShowHide(){
		String item="";
		int i = 0;
		int j = 0;
		int totalCount;
		try{
			totalCount=aboutObj.hrTwoShowHide.size()-1;
			for(i=0; i<=totalCount; i++){
				for(j=1; j<=3; j++){
					switch(j){
					case 1:
						item=Driver.findElement(By.xpath(".//*[@class='hr_showhide hr_showhide_close']["+ j +"]/span")).getAttribute("class");
						Assert.assertEquals(item, expectedData.getProperty("hrShowHide"+j));
					break;
					case 2:
						item=Driver.findElement(By.xpath(".//*[@class='hr_showhide hr_showhide_close']["+ j +"]/h5")).getAttribute("class");
						Assert.assertEquals(item, expectedData.getProperty("hrShowHide"+j));
					break;
					case 3:
						item=Driver.findElement(By.xpath(".//*[@class='hr_showhide hr_showhide_close']["+ j +"]/div")).getAttribute("class");
						Assert.assertEquals(item, expectedData.getProperty("hrShowHide"+j));
					break;
					default:
					break;
					}
					log.info("The Actual Module Box Wrapper item: "+ item);
					log.info("The Expected Module Box Wrapper item- "+expectedData.getProperty("aboutHrTwoColImageGallery"));
					log.info("TEST PASSED: The Actual and Expected Hope Restored About Hr Two Show Hide are Same");
				}
			}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored About Hr Two Show Hide item- "+item);
			log.error("The Expected Hope Restored About Hr Two Show Hide item- "+expectedData.getProperty("aboutHrTwoColImageGallery"));
			log.error("TEST FAILED: The Actual and Expected Hope Restored About Hr Two Show Hide are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored About Hr Two Show Hide element");
		}
	}
	
	public static void VerifyHRST_AboutBlockQuoteItems(){
		String item="";
		int j = 0;
		int totCount;
		try{
			totCount=aboutObj.aboutBlockQuoteItems.size()-1;
				for(j=0; j<=totCount; j++){
					item=aboutObj.aboutBlockQuoteItems.get(j).getTagName();
					Assert.assertEquals("<"+item+">", "<p>");
					log.info("The Actual Block Quote item: "+ "<"+item+">");
					log.info("The Expected Block Quote item- "+"<p>");
					log.info("TEST PASSED: The Actual and Expected Hope Restored About Block Quote items are Same");
				}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored About Block Quote item- "+"<"+item+">");
			log.error("The Expected Hope Restored About Block Quote item- "+"<p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored About Block Quote Items are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored About Block Quote Items element");
		}
	}
}
