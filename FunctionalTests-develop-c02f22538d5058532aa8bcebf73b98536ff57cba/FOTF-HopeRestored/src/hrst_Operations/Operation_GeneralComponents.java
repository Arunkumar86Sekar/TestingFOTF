package hrst_Operations;

import org.openqa.selenium.By;
import org.testng.Assert;

import hrst_Config.Basedriver;

public class Operation_GeneralComponents extends Basedriver {

	public static void VerifyHRST_GoogleAnalyticsScript(){
		log.info("<!-- Google Tag Manager -->");
		boolean actualGoogleAnalyticsNoScript=false;
		boolean actualGoogleAnalyticsScript=false;
		try{
			actualGoogleAnalyticsNoScript=generalObj.googleAnalyticsNoScript.isEnabled();
			actualGoogleAnalyticsScript=generalObj.googleAnalyticsScript.isEnabled();
			Assert.assertEquals(actualGoogleAnalyticsNoScript, true);
			Assert.assertEquals(actualGoogleAnalyticsScript, true);
			log.info("The Actual Hope Restored Google Analytics Script - "+generalObj.googleAnalyticsScript.getText());
			log.info("The Expected Hope Restored Google Analytics Script - Google Analyytics Script");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Google Analytics Script are Same");
		}catch(AssertionError e){
			log.info("The Actual Hope Restored Google Analytics Script - "+generalObj.googleAnalyticsScript.getText());
			log.info("The Expected Hope Restored Google Analytics Script - No Script");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Google Analytics Script are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Google Analytics Script element");
		}
	}
}
