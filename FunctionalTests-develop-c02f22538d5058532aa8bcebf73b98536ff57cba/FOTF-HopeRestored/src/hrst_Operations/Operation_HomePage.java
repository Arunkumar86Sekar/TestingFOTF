package hrst_Operations;

import java.util.List;

import hrst_Config.Basedriver;
import hrst_Objects.WebElements_Header;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Operation_HomePage extends Basedriver{
	
	public static void VerifyHRST_PromoTitle(){
		boolean actualpromoTitle = false;		
		try{
			actualpromoTitle=RM.IsDisplayedByWebelement(homeObj.promoTitle);
			Assert.assertEquals(actualpromoTitle, true);
			log.info("The Actual Hope Restored Promo Title - <" + RM.findAnElement(homeObj.promoTitle).getTagName() + ">");
			log.info("The Expected Hope Restored Promo Title - " + "Title - <h2>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Promo Title are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Promo Title - <" + RM.findAnElement(homeObj.promoTitle).getTagName() + ">");
			log.error("The Expected Hope Restored Promo Title - " + "Title - <h2>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Promo Title are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Promo Title element");
		}
	}
	
	public static void VerifyHRST_PromoDescription(){
		boolean actualpromoDescription = false;		
		try{
			actualpromoDescription=RM.IsDisplayedByWebelement(homeObj.promoDescription);
			Assert.assertEquals(actualpromoDescription, true);
			log.info("The Actual Hope Restored Promo Description - <" + RM.findAnElement(homeObj.promoDescription).getTagName() + ">");
			log.info("The Expected Hope Restored Promo Description - " + "Description - <p>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Promo Description are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Promo Description - <" + RM.findAnElement(homeObj.promoDescription).getTagName() + ">");
			log.error("The Expected Hope Restored Promo Description - " + "Description - <p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Promo Description are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Promo Description element");
		}
	}
	
	public static void VerifyHRST_PromoButton(){
		String actualPromoButton="";
		try{
			actualPromoButton=RM.GetAttributeByWebelement(homeObj.promoButton, "class");
			Assert.assertEquals(actualPromoButton, expectedData.getProperty("promoButton"));
			log.info("The Actual Hope Restored Home Promo Button - "+actualPromoButton);
			log.info("The Expected Hope Restored Home Promo Button - "+expectedData.getProperty("promoButton"));
			log.info("TEST PASSED: The Actual and Expected Hope Restored Home Promo Button are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Home Promo Button - "+actualPromoButton);
			log.error("The Expected Hope Restored Home Promo Button - "+expectedData.getProperty("promoButton"));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home Promo Button are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home Promo Button element");
		}
	}
	
	public static void VerifyHRST_contactTitle(){
		boolean actualcontactTitle = false;		
		try{
			actualcontactTitle=RM.IsDisplayedByWebelement(homeObj.contactTitle);
			Assert.assertEquals(actualcontactTitle, true);
			log.info("The Actual Hope Restored Contact Title - <" + RM.findAnElement(homeObj.contactTitle).getTagName() + ">");
			log.info("The Expected Hope Restored Contact Title - " + "Description - <p>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Title are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Title - <" + RM.findAnElement(homeObj.contactTitle).getTagName() + ">");
			log.error("The Expected Hope Restored Contact Title - " + "Description - <p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Title are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Title element");
		}
	}
	
	public static void VerifyHRST_contactDescription(){
		boolean actualcontactDescription = false;		
		try{
			actualcontactDescription=RM.IsDisplayedByWebelement(homeObj.contactDescription);
			Assert.assertEquals(actualcontactDescription, true);
			log.info("The Actual Hope Restored Contact Description - <" + RM.findAnElement(homeObj.contactDescription).getTagName() + ">");
			log.info("The Expected Hope Restored Contact Description - " + "Description - <p>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored Contact Description are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Contact Description - <" + RM.findAnElement(homeObj.contactDescription).getTagName() + ">");
			log.error("The Expected Hope Restored Contact Description - " + "Description - <p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Contact Description are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Contact Description element");
		}
	}
	
	public static void VerifyHRST_threeColumnItems(String threeComponentItems){
		String item="";
		int i;
		int j = 0;
		switch(threeComponentItems){
		case "4Items":
			try{
				for(i=1; i<=3; i++){
				List<WebElement> threeColumnItems = Driver.findElements(By.xpath(".//*[@class='hr_section'][1]//div[@class='hr_cnt_row'][2]/div[" + i + "]/div"));
					for(j=0; j<=3; j++){
						item=threeColumnItems.get(j).getAttribute("class");
						Assert.assertEquals(item, expectedData.getProperty("3columnItem"+j));
						log.info("The Actual Three Column item: "+ item);
						log.info("The Expected Three Column item- "+expectedData.getProperty("3columnItem"+j));
						log.info("TEST PASSED: The Actual and Expected Hope Restored Home Three Column items are Same");
					}
				}
			}catch(AssertionError e){
				log.error("The Actual Hope Restored Home Three Column item- "+item);
				log.error("The Expected Hope Restored Home Three Column item- "+expectedData.getProperty("3columnItem"+j));
				log.error("TEST FAILED: The Actual and Expected Hope Restored Home Three Column are NOT same");
			}catch(org.openqa.selenium.NoSuchElementException e){
				log.error("TEST FAILED: There is no Hope Restored Home Three Column element");
			}
		break;
		case "2Items":
			try{
				for(i=1; i<=3; i++){
				List<WebElement> threeColumnItems = Driver.findElements(By.xpath(".//*[@class='hr_section'][3]//div[@class='hr_cnt_row'][2]/div[" + i + "]/div"));
					for(j=0; j<=1; j++){
						item=threeColumnItems.get(j).getAttribute("class");
						if(j==1){j=2;}
						Assert.assertEquals(item, expectedData.getProperty("3columnItem"+j));
						log.info("The Actual Three Column item: "+ item);
						log.info("The Expected Three Column item- "+expectedData.getProperty("3columnItem"+j));
						log.info("TEST PASSED: The Actual and Expected Hope Restored Home Three Column items are Same");
						
					}
				}
			}catch(AssertionError e){
				log.error("The Actual Hope Restored Home Three Column item- "+item);
				log.error("The Expected Hope Restored Home Three Column item- "+expectedData.getProperty("3columnItem"+j));
				log.error("TEST FAILED: The Actual and Expected Hope Restored Home Three Column are NOT same");
			}catch(org.openqa.selenium.NoSuchElementException e){
				log.error("TEST FAILED: There is no Hope Restored Home Three Column element");
			}
		break;
	}
	}
	
	public static void VerifyHRST_moduleBoxWrapperItems(){
		String item="";
		int i = 1;
		int j = 0;
		try{
			for(i=1; i<=2; i++){
			List<WebElement> moduleBoxWrapperItems = Driver.findElements(By.xpath(".//*[@class='hr_two_col--module_box_wrapper'][" + i + "]/div"));
				for(j=0; j<=1; j++){
					item=moduleBoxWrapperItems.get(j).getAttribute("class");
					Assert.assertEquals(item, expectedData.getProperty("moduleBoxWrapperItems"+j));
					log.info("The Actual Module Box Wrapper item: "+ item);
					log.info("The Expected Module Box Wrapper item- "+expectedData.getProperty("moduleBoxWrapperItems"+j));
					log.info("TEST PASSED: The Actual and Expected Hope Restored Home Module Box Wrapper items are Same");
				}
			}
		}catch(AssertionError e){
			log.error("The Actual Hope Restored Home Module Box Wrapper item- "+item);
			log.error("The Expected Hope Restored Home Module Box Wrapper item- "+expectedData.getProperty("moduleBoxWrapperItems"+i));
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home Module Box Wrapper are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home Module Box Wrapper element");
		}
	}
	
	public static void VerifyContactInfoItems(){
		boolean item=false;
		int i = 1;
		try{
				for(i=0; i<=1; i++){
					item=RM.IsDisplayedByWebelement(contactUsObj.contactInfoItems.get(i));
					Assert.assertEquals(item, true);
					log.info("The Actual ContactInfo  item: <"+ contactUsObj.contactInfoItems.get(i).getTagName()+">");
					log.info("The Expected ContactInfo  item- <p>");
					log.info("TEST PASSED: The Actual and Expected Hope Restored Home ContactInfo  items are Same");
				}
			
		}catch(AssertionError e){
			log.error("The Actual ContactInfo  item: <"+ contactUsObj.contactInfoItems.get(i).getTagName()+">");
			log.error("The Expected ContactInfo  item- <p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored Home ContactInfo  are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored Home ContactInfo  element");
		}
	}
	
	public static void VerifyHrReportInfoTextItem(){
		boolean actualpromoTitle = false;		
		try{
			actualpromoTitle=RM.IsDisplayedByWebelement(homeObj.HrReportInfoTextItem);
			Assert.assertEquals(actualpromoTitle, true);
			log.info("The Actual Hope Restored HR Report Info Text - <" + RM.findAnElement(homeObj.HrReportInfoTextItem).getTagName() + ">");
			log.info("The Expected Hope Restored HR Report Info Text - " + "Title - <p>");
			log.info("TEST PASSED: The Actual and Expected Hope Restored HR Report Info Text are Same");
		}catch(AssertionError e){
			log.error("The Actual Hope Restored HR Report Info Text - <" + RM.findAnElement(homeObj.HrReportInfoTextItem).getTagName() + ">");
			log.error("The Expected Hope Restored HR Report Info Text - " + "Title - <p>");
			log.error("TEST FAILED: The Actual and Expected Hope Restored HR Report Info Text are NOT same");
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is no Hope Restored HR Report Info Text element");
		}
	}
}
