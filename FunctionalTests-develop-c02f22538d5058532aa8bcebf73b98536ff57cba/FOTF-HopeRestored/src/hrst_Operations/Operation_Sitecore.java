package hrst_Operations;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.SendKeysAction;

import hrst_Config.AboutusCommonClass;
import hrst_Config.Basedriver;

public class Operation_Sitecore extends Basedriver{
	// Login to the Sitecore
		public static void loginSitecore(){
		// Verify The Content Editor is Selected or not in Sitecore login page
			sitecoreObj.userName.clear();
			sitecoreObj.passWord.clear();
			
			RM.SendKeys(sitecoreObj.userName,AboutusCommonClass.SitecoreUserName());
			RM.SendKeys(sitecoreObj.passWord,AboutusCommonClass.SitecorePassword());			
			//sitecoreObj.loginButtion);
			RM.clickAnElement(sitecoreObj.loginButtion);
			
			RM.clickAnElement(sitecoreObj.contentEditor);
			}
				
		// Navigate to FOTF content of the Sitecore
		public static void navigateToHopecom(String pageToNavigate) throws InterruptedException{
			// Verify The "Content" is Displayed or not under the Sitecore content
			try{
				sitecoreObj.contentExtract.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.sitecoreExtract);
			}
			
			try{
				sitecoreObj.fotfCom.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.contentExtract);
			}
			
			try{
				sitecoreObj.hopeFirstItem.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.hopeExtract);
			}
			switch(pageToNavigate){
			
			case"About":
				RM.clickAnElement(sitecoreObj.hopeAbout);
				break;
			case"Programs":
				RM.clickAnElement(sitecoreObj.hopePrograms);
				break;
			case"Testimonials":
				RM.clickAnElement(sitecoreObj.hopeTestimonials);
				break;
			case"Home":
				RM.clickAnElement(sitecoreObj.hopeDotCom);
				break;
			//log.info("Click on FOTF.COM");			
			}
		}
		
		public static void navigateToPrimaryNavigation() throws InterruptedException{
			// Verify The "Content" is Displayed or not under the Sitecore content
			try{
				sitecoreObj.contentExtract.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.sitecoreExtract);
			}
			
			try{
				sitecoreObj.fotfCom.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.contentExtract);
			}
			
			try{
				sitecoreObj.hopeFirstItem.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.hopeExtract);
			}
			
			try{
				sitecoreObj.footerItem.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.standardExtract);
			}
			
			try{
				sitecoreObj.primaryNavigation.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.headerExtract);
			}
			
			try{
				sitecoreObj.primaryFirstItem.isDisplayed();
			}catch(org.openqa.selenium.NoSuchElementException e){
				RM.clickAnElement(sitecoreObj.primaryExtract);
			}
			
			RM.clickAnElement(sitecoreObj.aboutUnderPrimaryNavigation);
		}
		
		public static void navigateToPrivacy(String itemPath, String itemName){
			RM.SendKeys(sitecoreObj.treeSearch, itemPath);
			sitecoreObj.treeSearch.sendKeys(Keys.ENTER);
			Driver.findElement(By.xpath("//div[@id='SearchResult']//a[text()='"+itemName+"']")).click();
			sitecoreObj.treeSearch.clear();
			RM.clickAnElement(sitecoreObj.closeTreeSearch);
			
		}
		// Navigate to Create Layout for AutoAboutUs 
		public static void PublishPreview() throws InterruptedException{
			RM.clickAnElement(sitecoreObj.publishItem);
			//Thread.sleep(3000);
			//log.info("Clicked on Publish Item");
			RM.clickAnElement(sitecoreObj.previewMode);
			//log.info("Preview Mode opened for the AboutUs");
		}
		
		// Navigate to Child window for AutoAboutUs Preview page
		public static String GetChildWindow(String mode){
			Set<String> window=Driver.getWindowHandles();
			Iterator<String> itr = window.iterator();
			String parentName = itr.next();
			
			if (mode.equals("PARENT")){		
				return parentName;
			}
			
			try{
				return itr.next();
				}
			catch(Exception e){
				System.out.println(e);
			}
			
			return "Dummy";
		}	
}
