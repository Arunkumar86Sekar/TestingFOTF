package hrst_Assertion;

import hrst_Config.AboutusCommonClass;
import hrst_Config.Basedriver;
import hrst_Operations.Operation_AboutPage;
import hrst_Operations.Operation_ContactUsPage;
import hrst_Operations.Operation_Footer;
import hrst_Operations.Operation_GeneralComponents;
import hrst_Operations.Operation_Header;
import hrst_Operations.Operation_HomePage;
import hrst_Operations.Operation_Sitecore;

public class AssertFunctions extends Basedriver{
	public static void AssertHope(String toBeVerified) throws InterruptedException{
		if(!toBeVerified.contains("Static")){
		AboutusCommonClass.HomeSave();
		Operation_Sitecore.PublishPreview();
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("child"));
		}
		switch(toBeVerified){
		case "HeaderStickyBar":
			Operation_Header.VerifyHRST_HeaderPullLeft();
			Operation_Header.VerifyHRST_HeaderPullRight();
			break;
		case "HeaderNavigation":
			Operation_Header.VerifyHRST_HeaderMenuItemUrl();
			Operation_Header.VerifyHRST_HeaderMenuItemText();
			break;
		case "FooterNavigation":
			Operation_Header.VerifyHRST_FooterPrivacyText();
			Operation_Header.VerifyHRST_FooterPrivacyUrl();
			Operation_Header.VerifyHRST_FooterFocusLogoImg();
			Operation_Header.VerifyHRST_FooterhopeLogoImg();
			break;
		case "StaticHeader":
			Operation_Header.VerifyHRST_HeaderPullLeft();
			Operation_Header.VerifyHRST_HeaderPullRight();
			Operation_Header.VerifyHRST_HeaderLogo();
			Operation_Header.VerifyHRST_HeaderMenuItems();
			break;
		case "StaticFooter":
			Operation_Footer.VerifyHRST_FooterWidgetLeft();
			Operation_Footer.VerifyHRST_FooterWidgetRight();
			Operation_Footer.VerifyHRST_FooterWidgetInfo();
			Operation_Footer.VerifyHRST_FooterWidgetPullRight();
			break;
		case "StaticHome":
			Operation_HomePage.VerifyHRST_PromoTitle();
			Operation_HomePage.VerifyHRST_PromoDescription();
			Operation_HomePage.VerifyHRST_PromoButton();
			Operation_HomePage.VerifyContactInfoItems();
			Operation_HomePage.VerifyHRST_threeColumnItems("4Items");
			Operation_HomePage.VerifyHRST_moduleBoxWrapperItems();
			
			Operation_ContactUsPage.VerifyHRST_contactUsTitle();
			Operation_ContactUsPage.VerifyContactItems();
			Operation_ContactUsPage.VerifyHRST_contactUsMultiLineTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaLabel();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsSubmitButton();
			
			Operation_HomePage.VerifyHrReportInfoTextItem();
			Operation_HomePage.VerifyHRST_threeColumnItems("2Items");
			Operation_GeneralComponents.VerifyHRST_GoogleAnalyticsScript();
			Operation_ContactUsPage.VerifyContactItems();
			break;
		case "StaticAbout":
			Operation_AboutPage.VerifyHrParallaxItems();
			Operation_AboutPage.VerifyHrContentTextItems();
			Operation_AboutPage.VerifyHRST_AboutHrTwoColItemsItems();
			
			Operation_ContactUsPage.VerifyHRST_contactUsTitle();
			Operation_ContactUsPage.VerifyContactItems();
			Operation_ContactUsPage.VerifyHRST_contactUsMultiLineTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaLabel();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsSubmitButton();
			
			Operation_AboutPage.VerifyHRST_AboutBlockQuoteItems();
			Operation_HomePage.VerifyContactInfoItems();
			Operation_GeneralComponents.VerifyHRST_GoogleAnalyticsScript();
			break;
		case "StaticPrograms":
			Operation_AboutPage.VerifyHrParallaxItems();
			Operation_AboutPage.VerifyHrContentTextItems();
			
			Operation_ContactUsPage.VerifyHRST_contactUsTitle();
			Operation_ContactUsPage.VerifyContactItems();
			Operation_ContactUsPage.VerifyHRST_contactUsMultiLineTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaLabel();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsSubmitButton();
			
			Operation_AboutPage.VerifyHRST_AboutHrTwoColItemsItems();
			Operation_AboutPage.VerifyHRST_AboutHrTwoImageGallery();
			Operation_AboutPage.VerifyHRST_HrTwoShowHide();
			Operation_AboutPage.VerifyHRST_AboutBlockQuoteItems();
			Operation_HomePage.VerifyContactInfoItems();
			Operation_GeneralComponents.VerifyHRST_GoogleAnalyticsScript();
			break;
		case "StaticTestimonials":
			Operation_AboutPage.VerifyHrParallaxItems();
			Operation_AboutPage.VerifyHrContentTextItems();
			Operation_AboutPage.VerifyHRST_AboutHrTwoColItemsItems();
			
			Operation_ContactUsPage.VerifyHRST_contactUsTitle();
			Operation_ContactUsPage.VerifyContactItems();
			Operation_ContactUsPage.VerifyHRST_contactUsMultiLineTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaLabel();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsSubmitButton();
			
			Operation_AboutPage.VerifyHRST_AboutBlockQuoteItems();
			Operation_HomePage.VerifyContactInfoItems();
			Operation_GeneralComponents.VerifyHRST_GoogleAnalyticsScript();
			break;
		case "StaticContactUs":
			Operation_ContactUsPage.VerifyHRST_contactUsTitle();
			Operation_ContactUsPage.VerifyContactItems();
			Operation_ContactUsPage.VerifyHRST_contactUsMultiLineTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaLabel();
			Operation_ContactUsPage.VerifyHRST_contactUsCaptchaTextbox();
			Operation_ContactUsPage.VerifyHRST_contactUsSubmitButton();
			break;
		}
		if(!toBeVerified.contains("Static")){
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("child")).close();
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("PARENT"));
		}
	}
}
