package hrst_Objects;

import hrst_Operations.Operation_Footer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_ProgramsPage extends Operation_Footer{
	public WebElements_ProgramsPage(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@id='top-menu']//a[contains(text(),'Programs')]")
	public static WebElement programMenu;
}
