package hrst_Objects;

import java.util.List;

import hrst_Operations.Operation_Footer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_HomePage extends Operation_Footer{
	public WebElements_HomePage(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@class='hr_contact-cta--description']/h2")
	public static WebElement promoTitle;
	
	@FindBy(xpath=".//*[@class='hr_section'][3]//div[@class='hr_report_info--text']/p")
	public static WebElement HrReportInfoTextItem;
	
	@FindBy(xpath=".//*[@class='hr_contact-cta--description']/p")
	public static WebElement promoDescription;
	
	@FindBy(xpath=".//*[@class='hr_contact-cta--box_content torn_edge_white']/a")
	public static WebElement promoButton;
	
	@FindBy(xpath=".//*[@class='hr_contact_info--container']/div[1]/p")
	public static WebElement contactTitle;
	
	@FindBy(xpath=".//*[@class='hr_contact_info--container']/div[1]/p")
	public static WebElement contactDescription;
}
