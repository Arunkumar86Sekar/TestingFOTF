package hrst_Objects;

import java.util.List;

import hrst_Operations.Operation_Footer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_AboutPage extends Operation_Footer{
	public WebElements_AboutPage(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@id='top-menu']//a[contains(text(),'About')]")
	public static WebElement aboutMenu;
	
	@FindBy(xpath=".//*[@class='hr_parallax--container']/div")
	public static List <WebElement> hrParallax;
	
	@FindBy(xpath=".//*[@class='hr_content--text']")
	public static List <WebElement> hrContentText;
	
	@FindBy(xpath=".//*[@class='hr_two_col'][1]/div")
	public static List <WebElement> aboutHrTwoColItems;
	
	@FindBy(xpath=".//*[@class='hr_two_col'][2]/div[3]")
	public static List <WebElement> aboutHrTwoColImageGallery;
	
	@FindBy(xpath=".//*[@class='hr_showhide hr_showhide_close']")
	public static List <WebElement> hrTwoShowHide;
	
	@FindBy(xpath=".//*[blockquote]//p")
	public static List <WebElement> aboutBlockQuoteItems;
}
