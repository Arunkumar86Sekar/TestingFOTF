package hrst_Objects;



import hrst_Operations.Operation_Header;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_Header extends Operation_Header{
	public WebElements_Header(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@id='sticky-header']/div/div/span[1]")
	public static WebElement hPullLeft;
	
	@FindBy(xpath=".//*[@id='sticky-header']/div/div/span[2]")
	public static WebElement hPullRight;
	
	@FindBy(xpath=".//*[@id='logo']")
	public static WebElement hLogo;
	
	public static String hMenuItems="//ul[@id='top-menu']/li[";
	
	public static String hAboutMainMenu="//ul[@id='top-menu']/li[1]";
	
	public static String hMainMenuUrl="//ul[@id='top-menu']/li[1]/a";
	
	@FindBy(xpath="//ul[@class='footer--navigation']//a")
	public static WebElement FooterPrivacy;
	
	@FindBy(xpath="//div[@class='footer-widget'][1]/img")
	public static WebElement FooterFocusLogo;
	
	@FindBy(xpath="//div[@class='footer-widget'][2]/img")
	public static WebElement FooterhopeLogo;
}
