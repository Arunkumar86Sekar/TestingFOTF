package hrst_Objects;

import java.util.List;

import hrst_Operations.Operation_Footer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_ContactUsPage extends Operation_Footer{
	public WebElements_ContactUsPage(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@class='close']")
	public static WebElement contactUsCloseButton;
	
	@FindBy(xpath="//div[contains(@class,'scfSingleLineTextBorder')]/div[@class='scfSingleLineGeneralPanel']/span[1]")
	public static List<WebElement> contactUsItems;
	
	@FindBy(xpath=".//*[@class='hr_contact_info--container']/div/p")
	public static List<WebElement> contactInfoItems;
	
	@FindBy(xpath=".//*[@id='top-menu']//a[contains(text(),'Contact Us')]")
	public static WebElement contactUsMenu;
	
	@FindBy(xpath="//div[@class='scfSubmitButtonBorder']/input")
	public static WebElement contactUsSubmitButton;
	
	@FindBy(xpath=".//*[@class='modal-header']/h4")
	public static WebElement contactUsTitle;
	
	@FindBy(xpath="//div[@class='scfCaptchTop']/div/div/span")
	public static WebElement contactUsCaptchaLabel;
	
	@FindBy(xpath="//div[@class='scfCaptchTop']/following-sibling::div//input")
	public static WebElement contactUsCaptchaTextbox;
	
	@FindBy(xpath="//div[@class='scfMultipleLineGeneralPanel']/span[1]")
	public static WebElement contactUsMultiLineTextBox;
	
	@FindBy(xpath="//div[@class='scfCaptchTop']/following-sibling::div//input")
	public static List<WebElement> contactUsCaptchaText;
	
	@FindBy(xpath="//div[@id='main-content']//input[@value='SEND']")
	public static WebElement contactUsSubmit;
}
