package hrst_Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_Sitecore {
	public WebElements_Sitecore(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	// Login Page for Sitecore Sandbox 
		@FindBy(xpath=".//*[@id='UserName']")
		public static WebElement userName;
		
		@FindBy(xpath=".//*[@id='Password']")
		public static WebElement passWord;
		
		@FindBy(xpath=".//*[@id='login']/input")
		public static WebElement loginButtion;
		
		@FindBy(xpath="//span[contains(text(), 'Content Editor')]")
		public static WebElement contentEditor;
		
	// Navigation to AboutUs on Sitecore 		
		@FindBy(xpath="//span[text()='Content Source']/parent::a/parent::div/following-sibling::div[1]/a//span[text()='FOTF.com']")
		public static WebElement fotfCom;
		
		@FindBy(xpath="//span[text()='Hope.com']")
		public static WebElement hopeDotCom;
		
		@FindBy(xpath="//span[text()='Hope.com']/parent::a/following-sibling::div//span[text()='About']")
		public static WebElement hopeAbout;
		
		@FindBy(xpath="//span[text()='Hope.com']/parent::a/following-sibling::div//span[text()='Programs']")
		public static WebElement hopePrograms;
		
		@FindBy(xpath="//span[text()='Hope.com']/parent::a/following-sibling::div//span[text()='Testimonials']")
		public static WebElement hopeTestimonials;
		
		@FindBy(xpath="//span[text()='Hope.com']/parent::a/following-sibling::div/div[1]")
		public static WebElement hopeFirstItem;
		
		@FindBy(xpath="//span[text()='Standard Items']/parent::a/following-sibling::div/div//span[text()='Footer']")
		public static WebElement footerItem;
		
		@FindBy(xpath="//span[text()='Primary Navigation']/parent::a/following-sibling::div/div//span[text()='About']")
		public static WebElement primaryFirstItem;
		
		@FindBy(xpath=".//*[@id='TreeSearch']")
		public static WebElement treeSearch;
		
		@FindBy(xpath=".//*[@id='SearchHeader']/div[1]/a/img")
		public static WebElement closeTreeSearch;
		
		@FindBy(xpath=".//*[@id='Tree_Glyph_11111111111111111111111111111111']")
		public static WebElement sitecoreExtract;
		
		@FindBy(xpath=".//*[@id='Tree_Glyph_0DE95AE441AB4D019EB067441B7C2450']")
		public static WebElement contentExtract;
		
		@FindBy(xpath=".//*[@id='Tree_Glyph_AF3CAB1D6E914368A7E7683B0F49EEE0']")
		public static WebElement hopeExtract;
		
		@FindBy(xpath="//span[text()='Header']/../../img")
		public static WebElement headerExtract;
		
		@FindBy(xpath="//span[text()='Standard Items']/../../img")
		public static WebElement standardExtract;
		
		@FindBy(xpath="//span[text()='Primary Navigation']/../../img")
		public static WebElement primaryExtract;
		
		@FindBy(xpath="//span[text()='Primary Navigation']")
		public static WebElement primaryNavigation;
		
		@FindBy(xpath="//span[text()='Primary Navigation']/../../*//span[text()='About']")
		public static WebElement aboutUnderPrimaryNavigation;
		
		@FindBy(xpath=".//*[@id='Tree_Node_F8DE4B6E456B473792C2AEA89BC588FF']/span")
		public static WebElement aboutUs;
		
		@FindBy(xpath=".//*[@id='Ribbon_Nav_PublishStrip']")
		public static WebElement publishItem;
		
		@FindBy(xpath="html/body/form/div[5]/div[1]/div[3]/div/div[2]/div[5]/div[3]/div[1]/div[2]/a[2]//span[text()='Preview']")
		public static WebElement previewMode;
		
	// Navigation to LogOut on Sitecore
		@FindBy(xpath=".//*[@id='SystemMenu']")
		public static WebElement systemMenu;
		
		@FindBy(xpath="html/body/form/div[6]/table/tbody/tr[8]/td[2]")
		public static WebElement logOut;
		
		@FindBy(xpath="//td[text()='Delete']")
		public static WebElement deleteItem;
		
		@FindBy(xpath="html/body/form/div[2]/div[3]/div[1]/button[1]")
		public static WebElement deleteWithDatasoureItem;
}
