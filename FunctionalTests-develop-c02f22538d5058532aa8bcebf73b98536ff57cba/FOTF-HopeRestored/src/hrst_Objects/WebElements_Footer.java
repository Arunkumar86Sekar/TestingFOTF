package hrst_Objects;

import hrst_Operations.Operation_Footer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_Footer extends Operation_Footer{
	public WebElements_Footer(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath=".//*[@class='footer-widget'][1]/img")
	public static WebElement fWidgetLeft;
	
	@FindBy(xpath=".//*[@class='footer-widget'][2]/img")
	public static WebElement fWidgetRight;
	
	@FindBy(xpath=".//*[@id='footer-bottom']/div/p")
	public static WebElement fWidgetInfo;
	
	@FindBy(xpath=".//*[@id='footer-bottom']/div/span/a")
	public static WebElement fWidgetPullRight;
	
}
