package hrst_Objects;

import hrst_Operations.Operation_Footer;
import hrst_Operations.Operation_GeneralComponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElements_GeneralComponents extends Operation_GeneralComponents{
	public WebElements_GeneralComponents(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	@FindBy(xpath="//noscript[1]")
	public static WebElement googleAnalyticsNoScript;
	
	@FindBy(xpath="//noscript[1]//following-sibling::script[1]")
	public static WebElement googleAnalyticsScript;
}
