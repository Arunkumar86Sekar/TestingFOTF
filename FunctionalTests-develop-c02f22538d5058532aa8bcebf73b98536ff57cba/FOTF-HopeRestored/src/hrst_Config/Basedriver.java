package hrst_Config;

import hrst_Assertion.AssertFunctions;
import hrst_DDF.DraftInput;
import hrst_DDF.SitecoreDataSource;
import hrst_DDF.WebElementsDataSource;
import hrst_Objects.WebElements_AboutPage;
import hrst_Objects.WebElements_ContactUsPage;
import hrst_Objects.WebElements_Footer;
import hrst_Objects.WebElements_GeneralComponents;
import hrst_Objects.WebElements_Header;
import hrst_Objects.WebElements_HomePage;
import hrst_Objects.WebElements_ProgramsPage;
import hrst_Objects.WebElements_Sitecore;
import hrst_Objects.WebElements_TestimonialsPage;
import hrst_Operations.Operation_Sitecore;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class Basedriver {
public static WebDriver Driver=null;
public static ReusableMethods RM;
public static WebElements_Header headerObj;
public static WebElements_Footer footerObj;
public static WebElements_HomePage homeObj;
public static WebElements_AboutPage aboutObj;
public static WebElements_ProgramsPage programObj;
public static WebElements_TestimonialsPage testimonialsObj;
public static WebElements_ContactUsPage contactUsObj;
public static WebElements_Sitecore sitecoreObj;
public static WebElements_GeneralComponents generalObj;
public static WebElementsDataSource dataSourceObj;
public static DraftInput getData;
public static Logger log = Logger.getLogger(Basedriver.class.getName());
public static FileInputStream file=null;
public static Properties expectedData=null;
public static Properties dataSourceProvider=null;
static String testProp = "test.property";
static Properties sysProps;
public static String server;
public static String browser;

		@BeforeSuite
		public static void environmentSetup(){
			sysProps = System.getProperties();
			 server = sysProps.getProperty("ENVIRONMENT");
			 //server = "testpub.hoperestored.com:90";
			 browser = sysProps.getProperty("BROWSER");
			// browser = "chrome";
		}
		
		@BeforeTest
		public static void Configuration() throws Exception {	
			file=new FileInputStream(System.getProperty("user.dir")+"\\src\\hrst_DDF\\HopeRestored-ExpectedData.properties");
			expectedData=new Properties();
			expectedData.load(file);
			
			file=new FileInputStream(System.getProperty("user.dir")+"\\src\\hrst_DDF\\HopeRestored-DataSource.properties");
			dataSourceProvider=new Properties();
			dataSourceProvider.load(file);
			
		DOMConfigurator.configure("log4j.xml");
		
		if(browser.equalsIgnoreCase("Firefox")){
			 Driver=new FirefoxDriver();
			 log.info("***** Opening Firefox Browser *****");
		}
		else if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
			 Driver=new ChromeDriver();
			 log.info("***** Opening Chrome Browser *****");
		}
		else{
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver", "Drivers\\IEDriverServer.exe");
			Driver=new InternetExplorerDriver(capabilities);	
			log.info("***** Opening IE Browser *****");
		}
			Driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			Driver.manage().window().maximize();
			RM=new ReusableMethods();
			dataSourceObj=new WebElementsDataSource(Driver);
			headerObj=new WebElements_Header(Driver);
			footerObj=new WebElements_Footer(Driver);
			homeObj=new WebElements_HomePage(Driver);
			aboutObj=new WebElements_AboutPage(Driver);
			programObj=new WebElements_ProgramsPage(Driver);
			testimonialsObj=new WebElements_TestimonialsPage(Driver);
			contactUsObj=new WebElements_ContactUsPage(Driver);
			sitecoreObj=new WebElements_Sitecore(Driver);
			generalObj=new WebElements_GeneralComponents(Driver);
			getData=new DraftInput();
			log.info(" ");
			log.info("******************************************************* ");
			log.info("TestCase Execution Starts for Hope ReStored Project");
			log.info("******************************************************* ");
		}
			
		@AfterTest
		public static void closeDriver() throws InterruptedException{
			log.info(" ");
			log.info("TestCase Execution Finished for HRST Media Center Project");
			log.info("Closing Driver");
			Driver.quit();		
		}
			
		}