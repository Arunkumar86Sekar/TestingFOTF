package hrst_Config;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReusableMethods extends Basedriver{
	public static WebDriverWait wait;
	public static String attributeValue=null;
	public static boolean isDisplayedValue;
	public static String GetStringOfWebElement(WebElement xpath) {
		int strLen;
		int startIndex;
		
		String strXpath=xpath.toString();
		//System.out.println(xpath);
		 strLen=xpath.toString().length()-1;
		startIndex=strXpath.indexOf("-> ");
		strXpath=strXpath.substring(startIndex, strLen);
		//System.out.println(strXpath);
		
		strLen=strXpath.toString().length();
		startIndex=strXpath.indexOf(": ")+2;
		strXpath=strXpath.substring(startIndex, strLen);
		//System.out.println(strXpath);
		return strXpath;
}
	public static String RemoveSpace(String expectedValue){
		expectedValue=expectedValue.replaceAll("\\s+","");
		return expectedValue;
	}
	public static void clickAnElement(WebElement xpath) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    Driver.findElement(By.xpath(convertXpath)).click();
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
	}
	
	public static void clickAnElement(String xpath) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).click();
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
   // System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static void clearAnElement(String xpath) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).clear();
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
   // System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static void SendKeys(WebElement xpath, String sendKeys) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    Driver.findElement(By.xpath(convertXpath)).sendKeys(sendKeys);
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to send keys to the element: "+ e.getLocalizedMessage());	
    //System.out.println(("Unable to send keys to the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static void SendKeys(String xpath, String sendKeys) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).sendKeys(sendKeys);
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to send keys to the element: "+ e.getLocalizedMessage());	
    //System.out.println(("Unable to send keys to the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static WebElement FindAnElement(String xpath) {
		try{
			wait = new WebDriverWait(Driver, 30);
		    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		    }catch(Exception e){
			//log.fatal("Element Not found: " + e.getLocalizedMessage());
			//System.out.println(("Element Not found: "+ e.getLocalizedMessage()));
		}
		return Driver.findElement(By.xpath(xpath));
	}
	
	public static WebElement findAnElement(WebElement xpath) {
		WebElement findElement = null;
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    findElement=Driver.findElement(By.xpath(convertXpath));
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
		return findElement;
	}
	
	public static void clickAnElementByCss(WebElement css) {
		try{
		String convertXpath=GetStringOfWebElement(css);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    Driver.findElement(By.cssSelector(convertXpath)).click();
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
	}
	
	public static String GetAttributeByWebelement(WebElement xpath, String attributeName) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    attributeValue=Driver.findElement(By.xpath(convertXpath)).getAttribute(attributeName).replaceAll("\\s+","");
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
		return attributeValue;
	}
	
	public static String GetAttributeByString(String xpath, String attributeName) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    attributeValue= Driver.findElement(By.xpath(xpath)).getAttribute(attributeName).replaceAll("\\s+","");
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
	   // System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
		return attributeValue;
	}
	
	public static boolean IsDisplayedByWebelement(WebElement xpath) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    isDisplayedValue=Driver.findElement(By.xpath(convertXpath)).isDisplayed();
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
		return isDisplayedValue;
	}
	
	public static boolean IsDisplayedByString(String xpath) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    isDisplayedValue= Driver.findElement(By.xpath(xpath)).isDisplayed();
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
	   // System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
		return isDisplayedValue;
	}
}