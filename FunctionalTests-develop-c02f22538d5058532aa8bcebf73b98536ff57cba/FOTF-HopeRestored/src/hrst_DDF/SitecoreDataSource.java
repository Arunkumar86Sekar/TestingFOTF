package hrst_DDF;

import hrst_Config.Basedriver;

import java.util.Hashtable;

public class SitecoreDataSource extends Basedriver{
	static Hashtable hashTable = new Hashtable();
	
	public static void SetDefaultValueToHashTable(){
		hashTable.clear();		
		hashTable.put("HeaderStickyBar", false);
		hashTable.put("CreatePage", false);
	    hashTable.put("CreateHeaderNavigationItem", false);
	    hashTable.put("CreateFooterNavigationItem", false);
	    hashTable.put("UploadImage", false);
	    hashTable.put("SetPrimaryNavigation", false);
	}
	
	public static void DS_HopeRestored(String commonComponent) throws InterruptedException{
		SetDefaultValueToHashTable();
		
		switch(commonComponent){
			case"HeaderStickyBar":
				hashTable.replace("HeaderStickyBar", true);
			break;
			case"HeaderNavigation":
				//hashTable.replace("CreatePage", true);
				hashTable.replace("CreateHeaderNavigationItem", true);
				/*hashTable.replace("UploadImage", true);
				hashTable.replace("SetPrimaryNavigation", true);*/
			break;
			case"FooterNavigation":
				//hashTable.replace("CreatePage", true);
				hashTable.replace("CreateFooterNavigationItem", true);
				/*hashTable.replace("UploadImage", true);
				hashTable.replace("SetPrimaryNavigation", true);*/
			break;
			
			
		}
		
		CommonComponentDataSource.DataProvider(commonComponent, hashTable);
	}
	
	
}
