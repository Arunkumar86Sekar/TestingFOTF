package hrst_DDF;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import hrst_Config.AboutusCommonClass;
import hrst_Config.Basedriver;

public class DraftInput extends Basedriver{
	public static String mainUrl;
	public static String mainText;
	public static void RichTextEditor(String richTextTypeName) throws InterruptedException{
		
		RM.clickAnElement(dataSourceObj.editHtml.replaceAll("CONTROLTYPE", richTextTypeName));
		AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
		RM.clearAnElement(dataSourceObj.textEditor);
		RM.SendKeys(dataSourceObj.textEditor,dataSourceProvider.getProperty(RM.RemoveSpace(richTextTypeName)));
		RM.clickAnElement(dataSourceObj.itemTextAccept);
		Driver.switchTo().defaultContent();
	}
	
	public static void CreatePage(WebElement pageUnderitem, String pageText) throws InterruptedException{
		try{
			if(AboutusCommonClass.PageComponentContentItem().isDisplayed()){
				AboutusCommonClass.DeleteAboutUs();
		}}catch(org.openqa.selenium.NoSuchElementException e){}
		
		RightClickItem(pageUnderitem);
		RM.clickAnElement(dataSourceObj.insertMenu);
		RM.clickAnElement(dataSourceObj.pageMenu);
		AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
		RM.SendKeys(dataSourceObj.inputTextBox, pageText);
		RM.clickAnElement(dataSourceObj.itemTextAccept);
		Driver.switchTo().defaultContent();
		RM.clickAnElement(dataSourceObj.spanItem.replaceAll("ITEMNAME", pageText));
	}
	
	public static void CreateNavigationItem(WebElement navigationItem, String navigationName) throws InterruptedException{
		RM.clickAnElement(dataSourceObj.navigationButton);
		AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
		RM.SendKeys(dataSourceObj.inputTextBox, navigationName);
		RM.clickAnElement(dataSourceObj.itemTextAccept);
		Driver.switchTo().defaultContent();
		
	}
	
	public static String MainMenuUrlValue(){
		return mainUrl=dataSourceObj.mainMenuUrl.getAttribute("value");
	}
	public static String MainMenuTextValue(){
		return mainText=dataSourceObj.mainMenuText.getAttribute("value");
	}
	
	public static String GetPrivacySitecoreUrl(){
		return dataSourceObj.mainMenuUrl.getAttribute("value");
	}
	public static String GetPrivacySitecoreText(){
		return dataSourceObj.mainMenuText.getAttribute("value");
	}
	
	public static String GetSitecoreInputValue(String targetInput){
		return Driver.findElement(By.xpath("//div[contains(text(),'"+targetInput+"')]/following-sibling::div/input")).getAttribute("value");
	}
	public static void RightClickItem(WebElement rightClickOnItem) throws InterruptedException{
		RM.clickAnElement(rightClickOnItem);
		Actions item=new Actions(Driver);
		item.contextClick(rightClickOnItem).build().perform();
	}
	//span[text()='Pages']
}
