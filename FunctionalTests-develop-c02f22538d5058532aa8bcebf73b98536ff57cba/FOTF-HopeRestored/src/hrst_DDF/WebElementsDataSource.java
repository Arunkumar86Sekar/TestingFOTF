package hrst_DDF;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElementsDataSource {
	public WebElementsDataSource(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	static String editHtml="//div[contains(text(),'CONTROLTYPE')]/following-sibling::div[1]//a[text()='Edit HTML']";
	
	static String textEditor=".//*[@id='ctl00_ctl00_ctl05_Html']";
	
	public static String itemTextAccept=".//*[@id='OK']";
	
	@FindBy(xpath=".//*[@id='Ribbon_Nav_HomeStrip']")
	public static WebElement homeMenuStrip;
	
	public static String saveDataSource=".//*[@id='C3FCEBB9F38F4AB4857FCFC415B6A342_42F1FD8820E342BD9217A9468F115928']/div[1]/a/img";
	
	public static String insertMenu="//td[text()='Insert']";
	
	public static String pageMenu="//div[@id='Popup2']//td[text()='Pages']";
	
	public static String inputTextBox=".//*[@id='Value']";
	
	public static String spanItem="//span[text()='ITEMNAME']";
	
	@FindBy(xpath="//div[text()='Navigation Item']")
	public static WebElement navigationButton;
	
	@FindBy(xpath="//div[contains(text(),'URL')]/following-sibling::div[2]/input")
	public static WebElement mainMenuUrl;
	
	@FindBy(xpath="//div[contains(text(),'Text')]/following-sibling::div/input")
	public static WebElement mainMenuText;
}
