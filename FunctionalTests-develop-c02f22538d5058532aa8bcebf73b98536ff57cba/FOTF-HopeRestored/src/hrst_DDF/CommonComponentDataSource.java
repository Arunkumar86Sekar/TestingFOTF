package hrst_DDF;

import java.util.Hashtable;

import hrst_Config.AboutusCommonClass;
import hrst_Config.Basedriver;
import hrst_Operations.Operation_Sitecore;

public class CommonComponentDataSource extends DraftInput{
	public CommonComponentDataSource(String mainUrl, String mainText) {
		super();
		// TODO Auto-generated constructor stub
	}
	public static String privacySitecoreUrlValue;
	public static String privacySitecoreTextValue;
	public static String focusLogoSitecoreImgValue;
	public static String hopeLogoSitecoreImgValue;
	public static void DataProvider(String renderingDatasource, Hashtable hashTable) throws InterruptedException{
		
		if(hashTable.get("HeaderStickyBar").equals(true)){
			RichTextEditor("Program Info");
			RichTextEditor("Call Info");
		}
		
		if(hashTable.get("CreatePage").equals(true)){
			CreatePage(sitecoreObj.hopeDotCom, AboutusCommonClass.PageComponentContentItemName());
		}
		
		if(hashTable.get("CreateHeaderNavigationItem").equals(true)){
			Operation_Sitecore.navigateToPrimaryNavigation();
			MainMenuUrlValue();
			MainMenuTextValue();
			RM.clickAnElement(sitecoreObj.hopeDotCom);
		}
		
		if(hashTable.get("CreateFooterNavigationItem").equals(true)){
			Operation_Sitecore.navigateToPrivacy("/sitecore/content/HopeRestored/Standard Items/Footer/Primary Navigation/Privacy", "Privacy");
			privacySitecoreUrlValue=GetPrivacySitecoreUrl();
			privacySitecoreTextValue=GetPrivacySitecoreText();
			
			Operation_Sitecore.navigateToPrivacy("/sitecore/content/HopeRestored/Settings/Footer Settings", "Footer Settings");
			focusLogoSitecoreImgValue=GetSitecoreInputValue("Focus Logo");
			hopeLogoSitecoreImgValue=GetSitecoreInputValue("Hope Logo");
			
			RM.clickAnElement(sitecoreObj.hopeDotCom);
		}
		if(hashTable.get("UploadImage").equals(true)){
			
		}
		
		if(hashTable.get("SetPrimaryNavigation").equals(true)){
			
		}
	}
}
