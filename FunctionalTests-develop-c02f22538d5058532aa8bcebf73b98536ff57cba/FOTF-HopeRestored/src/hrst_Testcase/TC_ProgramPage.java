package hrst_Testcase;

import hrst_Assertion.AssertFunctions;
import hrst_Config.Basedriver;
import hrst_Operations.Operation_Footer;
import hrst_Operations.Operation_Header;
import hrst_Operations.Operation_Sitecore;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC_ProgramPage extends Basedriver{
	@BeforeMethod
		public static void NavigatetoHopeRestored() throws InterruptedException{
		Thread.sleep(5000);
		RM.clickAnElement(programObj.programMenu);
	} 
	
//Testcase for Hope Restored on Program Elements
	@Test
	public static void HopeRestored_Program() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Program Page***");
		log.info("__________________________________________________________________________________________________");
		Operation_Sitecore.navigateToHopecom("Programs");
		Operation_Sitecore.PublishPreview();
		AssertFunctions.AssertHope("Programs");
	}
	
	@AfterMethod
	public static void LogoutSitecore() throws InterruptedException{
		
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.systemMenu);
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.logOut);
	}	
}
