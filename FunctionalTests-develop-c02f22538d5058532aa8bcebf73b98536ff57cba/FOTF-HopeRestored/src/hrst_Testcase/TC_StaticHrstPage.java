package hrst_Testcase;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import hrst_Assertion.AssertFunctions;
import hrst_Config.Basedriver;
import hrst_Operations.Operation_Sitecore;

public class TC_StaticHrstPage extends Basedriver{

	@BeforeTest
	private static void SitecoreLogin() throws InterruptedException{
		Driver.get("http://"+server+"/sitecore/login");
		Operation_Sitecore.loginSitecore();
		Operation_Sitecore.navigateToHopecom("Home");
		Operation_Sitecore.PublishPreview();
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("child"));
	}
	
		//Testcase for Hope Restored on Header Elements
			@Test
			public static void HopeRestored_Header() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Header ***");
				log.info("__________________________________________________________________________________________________");
				AssertFunctions.AssertHope("StaticHeader");
			}	
			
		//Testcase for Hope Restored on Home Elements
			@Test
			public static void HopeRestored_Home() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Home ***");
				log.info("__________________________________________________________________________________________________");
				AssertFunctions.AssertHope("StaticHome");
			}
			
		//Testcase for Hope Restored on Footer Elements
			@Test
			public static void HopeRestored_Footer() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Footer ***");
				log.info("__________________________________________________________________________________________________");
				AssertFunctions.AssertHope("StaticFooter");
			}
			
			//Testcase for Hope Restored on About Elements
			@Test
			public static void HopeRestored_About() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: About Page***");
				log.info("__________________________________________________________________________________________________");
				aboutObj.aboutMenu.click();
				AssertFunctions.AssertHope("StaticAbout");
			}	
			
		//Testcase for Hope Restored on Program Elements
			@Test
			public static void HopeRestored_Program() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Program Page***");
				log.info("__________________________________________________________________________________________________");
				RM.clickAnElement(programObj.programMenu);
				AssertFunctions.AssertHope("StaticPrograms");
				
			}
			
		//Testcase for Hope Restored on testimonials Elements
			@Test
			public static void HopeRestored_testimonials() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: testimonials Page***");
				log.info("__________________________________________________________________________________________________");
				RM.clickAnElement(testimonialsObj.testimonialsMenu);
				AssertFunctions.AssertHope("StaticTestimonials");
			}	
			
		//Testcase for Hope Restored on ContactUs Elements
			@Test
			public static void HopeRestored_ContactUs() throws InterruptedException{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Contact Us Page***");
				log.info("__________________________________________________________________________________________________");
				RM.clickAnElement(contactUsObj.contactUsMenu);
				AssertFunctions.AssertHope("StaticContactUs");
			}	
	@AfterTest
	public static void LogoutSitecore() throws InterruptedException{
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("child")).close();
		Driver.switchTo().window(Operation_Sitecore.GetChildWindow("PARENT"));
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.systemMenu);
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.logOut);
	}
}
