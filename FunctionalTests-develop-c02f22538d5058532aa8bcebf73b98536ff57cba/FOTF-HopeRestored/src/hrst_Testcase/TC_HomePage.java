package hrst_Testcase;

import hrst_Assertion.AssertFunctions;
import hrst_Config.Basedriver;
import hrst_DDF.SitecoreDataSource;
import hrst_Operations.Operation_Footer;
import hrst_Operations.Operation_Header;
import hrst_Operations.Operation_HomePage;
import hrst_Operations.Operation_Sitecore;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC_HomePage extends Basedriver{
	@BeforeMethod
	private static void SitecoreLogin() throws InterruptedException{
		Driver.get("http://"+server+"/sitecore/login");
		Operation_Sitecore.loginSitecore();
		Operation_Sitecore.navigateToHopecom("Home");
	}
	//Testcase for Hope Restored on Header Sticky bar Elements
		@Test
		public static void HopeRestored_HeaderStickyBar() throws InterruptedException{
			log.info("");
			log.info("__________________________________________________________________________________________________");
			log.info("*** TestCase - PBI: B-01966 - FOTF: Hope Restored: Header Sticky Bar ***");
			log.info("__________________________________________________________________________________________________");
			SitecoreDataSource.DS_HopeRestored("HeaderStickyBar");
			AssertFunctions.AssertHope("HeaderStickyBar");
		}	
		
	//Testcase for Hope Restored on Header Elements
		@Test
		public static void HopeRestored_HeaderNavigation() throws InterruptedException{
			log.info("");
			log.info("__________________________________________________________________________________________________");
			log.info("*** TestCase - PBI: B-01967 - FOTF: Hope Restored: Header Sticky Bar ***");
			log.info("__________________________________________________________________________________________________");
			SitecoreDataSource.DS_HopeRestored("HeaderNavigation");
			AssertFunctions.AssertHope("HeaderNavigation");
		}	
		
	//Testcase for Hope Restored on Header Elements
		@Test
		public static void HopeRestored_FooterNavigation() throws InterruptedException{
			log.info("");
			log.info("__________________________________________________________________________________________________");
			log.info("*** TestCase - PBI: B-01965 - FOTF: Hope Restored: Header Sticky Bar ***");
			log.info("__________________________________________________________________________________________________");
			SitecoreDataSource.DS_HopeRestored("FooterNavigation");
			AssertFunctions.AssertHope("FooterNavigation");
		}	
	@AfterMethod
	public static void LogoutSitecore() throws InterruptedException{
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.systemMenu);
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.logOut);
	}	
}
