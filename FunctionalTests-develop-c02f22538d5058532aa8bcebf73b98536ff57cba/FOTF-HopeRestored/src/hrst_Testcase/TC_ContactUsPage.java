package hrst_Testcase;

import hrst_Assertion.AssertFunctions;
import hrst_Config.Basedriver;
import hrst_Operations.Operation_Footer;
import hrst_Operations.Operation_Header;
import hrst_Operations.Operation_ContactUsPage;





import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC_ContactUsPage extends Basedriver{
	
	@BeforeMethod
	public static void NavigatetoHopeRestored() throws InterruptedException{
		Thread.sleep(5000);
		RM.clickAnElement(contactUsObj.contactUsMenu);
	} 
	
	//Testcase for Hope Restored on ContactUs Elements
		@Test
		public static void HopeRestored_ContactUs() throws InterruptedException{
			log.info("");
			log.info("__________________________________________________________________________________________________");
			log.info("*** TestCase - PBI: B-01963 - FOTF: Hope Restored: Contact Us Page***");
			log.info("__________________________________________________________________________________________________");
			AssertFunctions.AssertHope("ContactUs");
		}	
}
