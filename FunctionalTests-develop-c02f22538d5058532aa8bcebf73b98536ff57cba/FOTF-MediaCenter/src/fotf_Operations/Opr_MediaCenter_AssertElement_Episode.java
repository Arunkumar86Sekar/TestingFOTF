package fotf_Operations;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import fotf_Config.Basedriver;
import fotf_Testcase.MediaCenter_ContentEditor_Episodes;

public class Opr_MediaCenter_AssertElement_Episode extends Basedriver{
	
	// Verify Unique Episode Image Element for MediaCenterEpisode Preview page
		public static void VerifyEpisodeUniqueImage(){
			String actualEpisodeUniqueImage="";
			try{
				actualEpisodeUniqueImage=sitecoreObj_Episode.episodeUniqueImage.getAttribute("style");
				boolean status=actualEpisodeUniqueImage.contains(MediaCenter_ContentEditor_Episodes.expectedUniqueImg.toLowerCase());
				Assert.assertTrue(status);
				log.info("The Actual  EpisodeUnique Image - "+actualEpisodeUniqueImage);
				log.info("The Expected  EpisodeUnique Image - "+MediaCenter_ContentEditor_Episodes.expectedUniqueImg);
				log.info("TEST PASSED: The Actual and Expected  EpisodeUnique Image are Same");
			}catch(AssertionError e){
				log.error("The Actual  EpisodeUnique Image - "+actualEpisodeUniqueImage);
				log.error("The Expected  EpisodeUnique Image - "+MediaCenter_ContentEditor_Episodes.expectedUniqueImg);
				log.error("TEST FAILED: The Actual and Expected  EpisodeUnique Image are NOT same");
			}catch(org.openqa.selenium.NoSuchElementException e){
				log.error("TEST FAILED: There is No EpisodeUnique Image element");
			}
			catch(Exception e){
				log.error("TEST CASE FAILED: Terminated Abruptly " + e.getLocalizedMessage());	
			}
		}

	// Verify  Episode Image Element for MediaCenterEpisode Preview page
		public static void VerifyEpisodeImage(){
			String actualEpisodeImage="";
			try{
				actualEpisodeImage=sitecoreObj_Episode.episodeUniqueImage.getAttribute("style");
				boolean status=actualEpisodeImage.contains(MediaCenter_ContentEditor_Episodes.expectedEpisodeImg.toLowerCase());
				Assert.assertTrue(status);
				log.info("The Actual  Episode Image - "+actualEpisodeImage);
				log.info("The Expected  Episode Image - "+MediaCenter_ContentEditor_Episodes.expectedEpisodeImg);
				log.info("TEST PASSED: The Actual and Expected  Episode Image are Same");
			}catch(AssertionError e){
				log.error("The Actual  Episode Image - "+actualEpisodeImage);
				log.error("The Expected  Episode Image - "+MediaCenter_ContentEditor_Episodes.expectedEpisodeImg);
				log.error("TEST FAILED: The Actual and Expected  Episode Image are NOT same");
			}catch(org.openqa.selenium.NoSuchElementException e){
				log.error("TEST FAILED: There is No Episode Image element");
			}
			catch(Exception e){
				log.error("TEST CASE FAILED: Terminated Abruptly " + e.getLocalizedMessage());	
			}
		}
		
	// Verify  Episode _SocialShareBarItems Element for MediaCenterEpisode Preview page
		public static void VerifyEpisode_SocialShareBarItems(){
			String[] expectedTitle={"Facebook", "Pinterest", "Import contacts and email to friends:", "Share a link on Twitter"};
			int i = 0;
			boolean actualEpisode_SocialShareBarItems=false;
			
				actualEpisode_SocialShareBarItems=sitecoreObj_Episode.episode_SocialShareBarItems.isDisplayed();
				Assert.assertTrue(actualEpisode_SocialShareBarItems);
				if(actualEpisode_SocialShareBarItems){
					List<WebElement> shareBarItems = Driver.findElements(By.xpath(sitecoreObj_Episode.episode_SocialShareBarItem));
					int sumShareBarTems=shareBarItems.size()-1;
					String actualTitle = null;
					try{
					for(i=0; i<=sumShareBarTems; i++){
						
						shareBarItems.get(i).click();
						if(i!=2){
							
							Driver.switchTo().window(Opr_Sitecore_AboutUs_Assert.GetChildWindow("GRANDCHILD"));
							actualTitle=Driver.getTitle();
							Driver.switchTo().window(Opr_Sitecore_AboutUs_Assert.GetChildWindow("GRANDCHILD")).close();
							Driver.switchTo().window(Opr_Sitecore_AboutUs_Assert.GetChildWindow("CHILD"));
						}else{
							actualTitle=sitecoreObj_Episode.episode_eMailItem.getText();
							RM.clickAnElement(sitecoreObj_Episode.episode_eMailClose);
						}
						Assert.assertTrue(actualTitle.contains(expectedTitle[i]));
						log.info("The Actual  Episode _SocialShareBarItems - "+actualTitle);
						log.info("The Expected  Episode _SocialShareBarItems - "+expectedTitle[i]);
						log.info("TEST PASSED: The Actual and Expected  Episode _SocialShareBarItems are Same");
					}
					}
					catch(AssertionError e){
						log.error("The Actual  Episode _SocialShareBarItems - "+actualTitle);
						log.error("The Expected  Episode _SocialShareBarItems - "+expectedTitle[i]);
						log.error("TEST FAILED: The Actual and Expected  Episode _SocialShareBarItems are NOT same");
					}catch(org.openqa.selenium.NoSuchElementException e){
						log.error("TEST FAILED: There is No Episode _SocialShareBarItems element");
					}
					catch(Exception e){
						log.error("TEST CASE FAILED: Terminated Abruptly " + e.getLocalizedMessage());	
					}
				}
				
		}
		
	// Verify  Episode _SocialShareBarOGImage Element for MediaCenterEpisode Preview page
		public static void VerifyEpisode_SocialShareBarOGImage(){
			String actualEpisode_SocialShareBarOGImage="";
			try{
				actualEpisode_SocialShareBarOGImage=sitecoreObj_Episode.episode_SocialShareBarOGImage.getAttribute("content");
				
				boolean status=actualEpisode_SocialShareBarOGImage.contains(MediaCenter_ContentEditor_Episodes.expectedEpisodesharebarImg.toLowerCase());
				Assert.assertTrue(status);
				log.info("The Actual  Episode _SocialShareBarOGImage - "+actualEpisode_SocialShareBarOGImage);
				log.info("The Expected  Episode _SocialShareBarOGImage - "+MediaCenter_ContentEditor_Episodes.expectedEpisodesharebarImg);
				log.info("TEST PASSED: The Actual and Expected  Episode _SocialShareBarOGImage are Same");
			}catch(AssertionError e){
				log.error("The Actual  Episode _SocialShareBarOGImage - "+actualEpisode_SocialShareBarOGImage);
				log.error("The Expected  Episode _SocialShareBarOGImage - "+MediaCenter_ContentEditor_Episodes.expectedEpisodesharebarImg);
				log.error("TEST FAILED: The Actual and Expected  Episode _SocialShareBarOGImage are NOT same");
			}catch(org.openqa.selenium.NoSuchElementException e){
				log.error("TEST FAILED: There is No Episode _SocialShareBarOGImage element");
			}
			catch(Exception e){
				log.error("TEST CASE FAILED: Terminated Abruptly " + e.getLocalizedMessage());	
			}
		}
}
