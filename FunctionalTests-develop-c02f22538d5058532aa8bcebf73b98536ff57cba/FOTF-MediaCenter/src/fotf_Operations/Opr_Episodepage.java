package fotf_Operations;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;

import fotf_Config.Basedriver;

public class Opr_Episodepage extends Basedriver{
	
	@BeforeMethod
	public static void baseUrl(){
		Driver.get("http://"+server);
	}
	
	public static void longLoadSearchEpisode(){
		episodeObj.episodeSearchTextBox.sendKeys(expectedData.getProperty("episodeText"));
		Driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		long start = System.currentTimeMillis();
		
		String urlText=episodeObj.pastEpisodeTitle.getText();
		long finish = System.currentTimeMillis();
		long totalTime = finish - start;
		long seconds=totalTime/1000;
		
		log.info("Total Time for searching the episode item - "+seconds+" Seconds for "+urlText);
		
		if(seconds<=10){
			log.info("The Searching for episodes has taken 10 seconds correctly");
		}
		else{
			log.error("The Searching for episodes has taken longer than 10 seconds");
		}
	}
	
	public static String GetSitecoreInputValue(String targetInput){
		return Driver.findElement(By.xpath("//div[contains(text(),'"+targetInput+"')]/following-sibling::div/input")).getAttribute("value");
	}
	
	public static void PutSitecoreInputValue(String targetInput, String inputData){
		 Driver.findElement(By.xpath("//div[contains(text(),'"+targetInput+"')]/following-sibling::div/input")).sendKeys(inputData);
	}
	
	public static void ClearSitecoreInputValue(String targetInput){
		 Driver.findElement(By.xpath("//div[contains(text(),'"+targetInput+"')]/following-sibling::div/input")).clear();
	}
	public static String GetSitecoreItemPath(){
		 return Driver.findElement(By.xpath("//td[contains(text(),'Item path')]/following-sibling::td/input")).getAttribute("value");
	}
	public static void RightClickItem(WebElement rightClickOnItem) throws InterruptedException{
		RM.clickAnElement(rightClickOnItem);
		Actions item=new Actions(Driver);
		item.contextClick(rightClickOnItem).build().perform();
	}
}
