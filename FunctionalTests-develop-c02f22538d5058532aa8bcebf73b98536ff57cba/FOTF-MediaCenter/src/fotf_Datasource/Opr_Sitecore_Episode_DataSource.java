package fotf_Datasource;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_Episodepage;
import fotf_Operations.Opr_PageEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;

public class Opr_Sitecore_Episode_DataSource extends Basedriver{
	public String StoreEpisodeUniqueImage() throws InterruptedException{
		String EpisodeUniqueImageValue="";
		EpisodeUniqueImageValue = Opr_Episodepage.GetSitecoreInputValue("Episode Image");
		if(EpisodeUniqueImageValue.equals("")){
			Opr_Episodepage.PutSitecoreInputValue("Episode Image", "/Images/articles/adopting-children-1063x597");
			EpisodeUniqueImageValue="/Images/articles/adopting-children-1063x597";
		}
		Opr_ContentEditor_AboutUs.HomeSave();
		Opr_ContentEditor_AboutUs.PublishPreview();
		return EpisodeUniqueImageValue;
	}
	
	public String StoreEpisodeImage() throws InterruptedException{
		String episodeImageValue="";
		Opr_Episodepage.ClearSitecoreInputValue("Episode Image");
		Opr_ContentEditor_AboutUs.HomeSave();
		episodeImageValue = Opr_Episodepage.GetSitecoreItemPath();
		String uniqueImagePath = episodeImageValue.substring(0, episodeImageValue.indexOf("/2"));
		Opr_Sitecore_AboutUs.navigateToItem(uniqueImagePath, uniqueImagePath.substring(uniqueImagePath.lastIndexOf("/") + 1));
		episodeImageValue = Opr_Episodepage.GetSitecoreInputValue("Image");
		
		return episodeImageValue;
	}
	
	public String StoreEpisodeImage_SocialBar(String itemPath, String itemName) throws InterruptedException{
		String EpisodeUniqueImageValue="";
		EpisodeUniqueImageValue = Opr_Episodepage.GetSitecoreInputValue("Episode Image");
		if(EpisodeUniqueImageValue.equals("")){
			EpisodeUniqueImageValue=StoreEpisodeImage();
			Opr_Sitecore_AboutUs.navigateToItem(itemPath, itemName);
		}
		
		Opr_ContentEditor_AboutUs.PublishPreview();
		return EpisodeUniqueImageValue;
	}
}