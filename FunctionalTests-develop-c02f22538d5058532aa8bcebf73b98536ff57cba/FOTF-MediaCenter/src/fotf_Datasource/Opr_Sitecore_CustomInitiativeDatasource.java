package fotf_Datasource;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_PageEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;

public class Opr_Sitecore_CustomInitiativeDatasource extends Basedriver{
	public static String dataSourcePath="";
	static Hashtable hashTable = new Hashtable();
	private static void SetDefaultValueToHashTable(){
		hashTable.clear();		
		hashTable.put("TitleText", false);
		hashTable.put("ItemText", false);
	    hashTable.put("Image", false);
	    hashTable.put("ContentType", false);
	   }    
    
	public static void CustomInitiativeComponentDatasource(String renderingDatasource) throws InterruptedException{	    
		SetDefaultValueToHashTable();
		switch(renderingDatasource)
		{			
			case "CustomInitiativeComponent":
				hashTable.replace("TitleText", true);
				hashTable.replace("ItemText", true);
				hashTable.replace("Image", true);
			break;
			case "InitiativeContentTypeLeftHeroImage":
				hashTable.replace("TitleText", true);
				hashTable.replace("ItemText", true);
				hashTable.replace("Image", true);
				hashTable.replace("ContentType", true);
			break;
			//case "PageComponentHeader": break;			
			default:
				break;			
		}		
		CustomInitiativeComponentDataSource(renderingDatasource, hashTable);
	}
	
	public static void SeletTopicInformation() throws InterruptedException{
		Opr_ContentEditor_AboutUs.HomeSave();
		RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
		RM.clickAnElement(AboutusCommonClass.InitiativeComponentContentItem());
		RM.clickAnElement(Obj_Sitecore_AboutusDatasource.topic_MarriageOpenBlock);
		RM.clickAnElement(Obj_Sitecore_AboutusDatasource.topicUnderMarriage);
		RM.clickAnElement(Obj_Sitecore_AboutusDatasource.topicToRight);
		Opr_ContentEditor_AboutUs.HomeSave();
	}
	public static String getComponentTitle;
	public static void CustomInitiativeComponentDataSource(String renderingDatasource, Hashtable hashTable)throws InterruptedException{
		if(hashTable.get("TitleText").equals(true)){
			RM.SendKeys(dataSourceObj.commonComponentTitle,renderingDatasource + " - Title");	
			getComponentTitle=dataSourceObj.commonComponentTitle.getAttribute("value");
		}
		if(hashTable.get("ItemText").equals(true)){
			AboutusCommonClass.UrlExternalLinkAndRichTextEditor_Click(dataSourceObj.commonComponentShowEditor);
			AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");
			RM.SendKeys(dataSourceObj.commonComponentTextEditor,customInitiativeDataSourceProvider.getProperty("InitiativeComponentText"));
			RM.clickAnElement(dataSourceObj.itemTextAccept);
			Driver.switchTo().defaultContent();
		}
		if(hashTable.get("Image").equals(true)){
			RM.SendKeys(dataSourceObj.commonComponentImage,customInitiativeDataSourceProvider.getProperty("InitiativeComponentImage"));
		}
		if(hashTable.get("ContentType").equals(true)){
			AboutusCommonClass.SelectDropdownListByvisibleText(dataSourceObj.custominitiativeContentType, "Initiative");
		}
		Opr_ContentEditor_AboutUs.HomeSave();
	}
}