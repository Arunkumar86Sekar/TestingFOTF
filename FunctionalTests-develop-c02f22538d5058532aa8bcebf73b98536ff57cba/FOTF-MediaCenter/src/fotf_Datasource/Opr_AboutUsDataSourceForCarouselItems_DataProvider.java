package fotf_Datasource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import fotf_Config.AboutusCommonClass;
import fotf_Config.Basedriver;
import fotf_Operations.Opr_AboutUs_AssertElement;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class Opr_AboutUsDataSourceForCarouselItems_DataProvider extends Basedriver
{	
	public static void CreateCarouselItemsWithFolder(String FolderName, String carouselLevel, String TemplateName) throws InterruptedException{
		Opr_ContentEditor_AboutUs.HomeFolderTemplates();							
		AboutusCommonClass.SwitchToFrame("jqueryModalDialogsFrame", "scContentIframeId0");			
		RM.SendKeys(dataSourceObj.childFolderName,(dataSourceProvider.getProperty(FolderName)));
		RM.clickAnElement(sitecoreObj.okButton);
		Driver.switchTo().defaultContent();
		RM.clickAnElement(sitecoreObj.insertFromTemplate);			
		AboutusCommonClass.SwitchToFrame();			
		AboutusCommonClass.NavigateToCommonComponentTemplate("MetaData");
		if(TemplateName.equals("CarouselTemplate")){RM.clickAnElement(dataSourceObj.carouselItemTemplate);	}
		else{RM.clickAnElement(dataSourceObj.childItemTemplate);	}	
		sitecoreObj.itemName.clear();
		RM.SendKeys(sitecoreObj.itemName,(dataSourceProvider.getProperty(carouselLevel)+"1"));
		RM.clickAnElement(sitecoreObj.insertButton);
		Driver.switchTo().defaultContent();	
	}
	
	public static void CarouselItemDataSource(String renderingDatasource) throws InterruptedException
	{
		RM.clickAnElement(dataSourceObj.commonComponentContentTab);
		RM.SendKeys(dataSourceObj.childItemTitle,("Carousel Item 1 - Title"));	
		RM.SendKeys(dataSourceObj.childItemImage,(dataSourceProvider.getProperty("ChildItemImage")));
		RM.clickAnElement(dataSourceObj.saveDataSource);
	}
	
	public static void DuplicatingCarouselItemDataSource(String renderingDatasource) throws InterruptedException
	{
		int numOfChildItemsToDuplicate = 4;
		for(int i = 2; i <= numOfChildItemsToDuplicate; i++){				
			Actions ref = new Actions(Driver);
			ref.contextClick(dataSourceObj.carouselItems_1).build().perform();
			RM.clickAnElement(dataSourceObj.duplicateChildItem);
			AboutusCommonClass.SwitchToFrame();
			RM.SendKeys(dataSourceObj.childFolderName,(dataSourceProvider.getProperty("CarouselItem") + i));
			RM.clickAnElement(sitecoreObj.okButton);
			Driver.switchTo().defaultContent();
		}			
	}
	
	public static void SelectCarouselItem_RenderingDatasource(String renderingDatasource) throws InterruptedException
	{		
		RM.clickAnElement("//span[text()='" + renderingDatasource+"_DataSource" +"']");
		int childItemsToDisplay = 3;		
		for(int i = 1; i <= childItemsToDisplay; i++){
			Actions ref = new Actions(Driver);
			WebElement item = RM.FindAnElement("//option[text()='" + dataSourceProvider.getProperty("CarouselItem")+Integer.toString(i) +"']");
			ref.doubleClick(item).build().perform();
		}
		RM.clickAnElement(dataSourceObj.saveDataSource);
	}
	
	// Select date for all the Marriage Article under three components for AutoAboutUs 	
	public static void ClonedArticlePublicationDate(String renderingDatasource, int articleStartPos, int articleCount) throws InterruptedException 
	{	
		String clonedArticle="//span[text()='" + renderingDatasource + "_DataSource']/parent::a/following-sibling::div/div[*]";
		for(int i=1; i<= articleCount; i++){
			RM.clickAnElement(clonedArticle.replace("*", Integer.toString(i)));
			if(i <= 3 && renderingDatasource.equals("marriage")){
				AboutusCommonClass.clonedRecentArticleTitle[i] = dataSourceObj.recentArticleTitleText.getAttribute("value");
			}
			RM.clickAnElement(dataSourceObj.clearPublicationDate);
			RM.SendKeys(dataSourceObj.publicationDate,(AboutusCommonClass.PublicationDate(articleStartPos + i- 2)));
			Opr_ContentEditor_AboutUs.HomeSave();
		}
	}	
}