package fotf_Testcase;

import org.openqa.selenium.Alert;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import fotf_Config.Basedriver;
import fotf_Datasource.Opr_Sitecore_AboutusDatasource;
import fotf_Operations.Opr_ContentEditor_AboutUs;
import fotf_Operations.Opr_PageEditor_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class MediaCenter_ContentEditor_Episodes extends Basedriver{

    //SiteCore Login
	@BeforeMethod
	public static void LoginSitecore(){
		Driver.get("http://"+server+"/sitecore/login");
		Opr_Sitecore_AboutUs.loginSitecore();
	}    
	public static String expectedUniqueImg;
	public static String expectedEpisodeImg;
	public static String expectedEpisodesharebarImg;
	//Testcase for Episode_UniqueImage
		@Test
		public static void Episode_UniqueImage() throws InterruptedException{
			String itemPath="/sitecore/content/FOTF/media-center/shows/daily-broadcast/2016/07/28/20/56/cultivating-a-kingdom-marriage-pt2";
			String itemName="cultivating-a-kingdom-marriage-pt2";
			try{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: 18396 - FOTF: Episode: Control Rendering: Unique Image ***");
				log.info("__________________________________________________________________________________________________");
				Opr_Sitecore_AboutUs.navigateToItem(itemPath, itemName);
				expectedUniqueImg=OprEpisode.StoreEpisodeUniqueImage();
				Opr_Sitecore_AboutUs_Assert.AssertAboutus("UniqueImage", "ContentEditor");
				System.out.println();
				expectedEpisodeImg=OprEpisode.StoreEpisodeImage();
				Opr_Sitecore_AboutUs.navigateToItem(itemPath, itemName);
				Opr_ContentEditor_AboutUs.PublishPreview();
				Opr_Sitecore_AboutUs_Assert.AssertAboutus("EpisodeImage", "ContentEditor");
			}catch(Exception e){
				Driver.navigate().refresh();
				log.fatal("TEST METHOD FAILED: Terminated Abruptly " + e.getLocalizedMessage());
			}
		}	
	
	//Testcase for Episode_SocialSharebar
		@Test
		public static void Episode_SocialSharebar() throws InterruptedException{
			String itemPath="/sitecore/content/FOTF/media-center/shows/daily-broadcast/2016/07/28/20/56/cultivating-a-kingdom-marriage-pt2";
			String itemName="cultivating-a-kingdom-marriage-pt2";
			try{
				log.info("");
				log.info("__________________________________________________________________________________________________");
				log.info("*** TestCase - PBI: 18396 - FOTF: Episode: Control Rendering: Social Share Bar ***");
				log.info("__________________________________________________________________________________________________");
				Opr_Sitecore_AboutUs.navigateToItem(itemPath, itemName);
				expectedEpisodesharebarImg=OprEpisode.StoreEpisodeImage_SocialBar(itemPath, itemName);
				Opr_Sitecore_AboutUs_Assert.AssertAboutus("SocialSharebar", "ContentEditor");
			}catch(Exception e){
				Driver.navigate().refresh();
				log.fatal("TEST METHOD FAILED: Terminated Abruptly " + e.getLocalizedMessage());
			}
		}	
	// Logout Sitecore
	@AfterMethod
	public static void LogoutSitecore() throws InterruptedException{
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.systemMenu);
		Thread.sleep(2000);
		RM.clickAnElement(sitecoreObj.logOut);
	}	
}
