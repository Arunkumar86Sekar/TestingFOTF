package fotf_Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import fotf_Operations.Opr_Sitecore_AboutUs;

public class Obj_Sitecore_MediaCenter_Episode extends Opr_Sitecore_AboutUs{
	public Obj_Sitecore_MediaCenter_Episode(WebDriver Driver){
		PageFactory.initElements(Driver, this);
	}
	
	
	
//FiveColumn
	@FindBy(xpath="//div[@class='oo_promo']")
	public static WebElement episodeUniqueImage;
	
	@FindBy(xpath="//div[@class='media_archive']")
	public static WebElement episodeImage;
	
	@FindBy(xpath="//div[@class='episode_player ng-scope']/parent::div/following-sibling::div//div[@id='social_sharing_top']")
	public static WebElement episode_SocialShareBarItems;
	
	public static String episode_SocialShareBarItem="//div[@class='episode_player ng-scope']/parent::div/following-sibling::div//div[@id='social_sharing_top']//td[@style='vertical-align:bottom; white-space:nowrap;zoom:1;']";
	
	@FindBy(xpath=".//*[contains(@id, 'showShareUI_emailCanvas')]/tbody/tr[2]//span")
	public static WebElement episode_eMailItem;
	
	@FindBy(xpath=".//*[contains(@id, 'showShareUI_emailCanvas')]/tbody/tr[1]//img")
	public static WebElement episode_eMailClose;
	
	@FindBy(xpath="//meta[@property='og:image']")
	public static WebElement episode_SocialShareBarOGImage;
}

