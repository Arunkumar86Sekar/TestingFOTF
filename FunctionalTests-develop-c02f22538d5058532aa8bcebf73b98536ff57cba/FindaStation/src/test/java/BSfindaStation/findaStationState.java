package com.FindaStation.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.testng.annotations.*;
import org.testng.Assert;
import org.testng.AssertJUnit;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FindaStationState {
  private WebDriver driver;
  private WebDriverWait wait;
  private String baseUrl = System.getenv("TEST_SERVER_HOST");
  private String selectStateXpath = "/html/body/div/div[2]/div[1]/select";
  private String selectCityXpath = "/html/body/div/div[2]/div[2]/select";
  private String componentURL = "/media-center/shows/daily-broadcast/find-a-station";
  private String rowXpath = "//*[@class='table-responsive']";

  public static final String USERNAME = System.getenv("BS_USERNAME");
  public static final String AUTOMATE_KEY = System.getenv("BS_KEY");
  public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

  @BeforeClass(alwaysRun = true)
  public void setUpBrowserStack() throws Exception {
    DesiredCapabilities capability = new DesiredCapabilities();
    capability.setPlatform(Platform.MAC);
    capability.setCapability("build", "Find a Station Test");
    capability.setCapability("browserstack.local", "true");
    capability.setCapability("browserstack.debug", "true");
    capability.setCapability("browser", "Chrome");
    capability.setCapability("browser_version", "51.0");
    capability.setCapability("os", "OS X");
    capability.setCapability("os_version", "El Capitan");
    capability.setCapability("resolution", "1280x960");

    driver = new RemoteWebDriver(
       new URL(URL),
       capability
     );

     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

     wait = new WebDriverWait(driver, 20);
  };

  //Test State Search
  @Test(priority = 0, invocationCount = 5)
  public void testFindaState() throws Exception {
    System.out.println("<----Testing State Search---->");
    System.out.println("<---Opening->"+baseUrl);

    //select state and search
    driver.get(baseUrl + componentURL);

    driver.switchTo().frame(0);

    String[] stateList = buildArray(selectStateXpath);

    int randomNum = randomWithRange(1,stateList.length-1);

    String state = stateList[randomNum];

    System.out.println("Testing -->" + state);
    Select select = new Select(driver.findElement(By.xpath(selectStateXpath)));
    select.selectByVisibleText(state);

    String[] cityList = buildArray(selectCityXpath);

    //test results
    int numRows = driver.findElements(By.xpath(rowXpath + "/table/tbody/tr")).size();

    for (int i = 1; i <= numRows; i++) {
      boolean contains;
      String cityResults = driver.findElement(By.xpath(rowXpath + "/table/tbody/tr["+i+"]/td[1]")).getText();

      contains = Arrays.asList(cityList).contains(cityResults);
      System.out.println("Does citylist contain " + cityResults + "?: " + contains);

      assertTrue(contains);
    }
  }

    //test state and city
    @Test(priority = 1, invocationCount = 5)
    public void testFindaCity() throws Exception {
      System.out.println("<----Testing State + City Search---->");
      System.out.println("<---Opening->"+baseUrl);

      //open site
      driver.get(baseUrl + componentURL);
      //switch to iframe
      driver.switchTo().frame(0);

      //randomly select a state from list
      String[] stateList = buildArray(selectStateXpath);

      int randomNum = randomWithRange(1,stateList.length-1);
      String state = stateList[randomNum];
      System.out.println("Testing -->" + state);
      Select selectedState = new Select(driver.findElement(By.xpath(selectStateXpath)));
      selectedState.selectByVisibleText(state);

      String[] cityList = buildArray(selectCityXpath);

      //randomly select city from list
      int randomNum2 = randomWithRange(1,cityList.length-1);
      String city = cityList[randomNum2];

      Select selectedCity = new Select(driver.findElement(By.xpath(selectCityXpath)));
      selectedCity.selectByVisibleText(city);

      //test results
      int numRows = driver.findElements(By.xpath(rowXpath + "/table/tbody/tr")).size();

      for (int i = 1; i <= numRows; i++) {
        // String stateResults = driver.findElement(By.xpath(rowXpath + "/tbody/tr["+i+"]/td[1]")).getText();
        // testValues(state, stateResults);

        String cityResults = driver.findElement(By.xpath(rowXpath + "/table/tbody/tr["+i+"]/td[1]")).getText();
        testValues(city, cityResults);

    }
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

private void testValues(String expected, String actual) {
  System.out.println("Expected ->" + expected + "\nActual --->" + actual);
  assertEquals(expected, actual);
}

private String[] buildArray(String xpath) {
  String[] list;
  String listXpath = xpath;
  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(listXpath + "/option")));
  int arrayLength = driver.findElements(By.xpath(listXpath + "/option")).size();
  System.out.println("Array length " + arrayLength);
  list = new String[arrayLength];

  for (int i = 1; i < arrayLength; i++) {
    int j = i + 1;
    String value = driver.findElement(By.xpath(listXpath + "/option["+ j +"]")).getAttribute("value");
    list[i] = value;
    System.out.println("Adding " + value + " to index " + i);
  };

  return list;
}

private int randomWithRange(int min, int max)
{
   int range = (max - min) + 1;
   return (int)(Math.random() * range) + min;
}
}
