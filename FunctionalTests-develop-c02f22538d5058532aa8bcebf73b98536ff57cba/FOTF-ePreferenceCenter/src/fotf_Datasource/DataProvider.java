package fotf_Datasource;

import java.util.Hashtable;

import fotf_Config.Basedriver;

public class DataProvider extends Basedriver{

	public static void ComponentDataProvider(Hashtable hashTable) throws InterruptedException{
		
		if(hashTable.get("Title").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Title", DSP.getProperty("Title"));
		}
		
		if(hashTable.get("RTETitle").equals(true)){
			siteComponents.typeRTEvalue("Title", DSP.getProperty("RTETitle"));
		}
		
		if(hashTable.get("ShortDescription").equals(true)){
			siteComponents.typeRTEvalue("Short Description", DSP.getProperty("ShortDescription"));
		}
		
		if(hashTable.get("MyAccountInformationText").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue","My Account Information Text", DSP.getProperty("MyAccountInformationText"));
		}
		
		if(hashTable.get("EmailSubscriptionsText").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue","Email Subscriptions Text", DSP.getProperty("EmailSubscriptionsText"));
		}
		
		if(hashTable.get("UnsubscribeText").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Unsubscribe Text", DSP.getProperty("UnsubscribeText"));
		}
		
		if(hashTable.get("VideoItems").equals(true)){
			
		}
		
		if(hashTable.get("OtherEmailSubscriptionsText").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Other Email Subscriptions Text", DSP.getProperty("OtherEmailsubscribeText"));
		}
		
		if(hashTable.get("SaveButtonText").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Save Button Text", DSP.getProperty("SaveButtonText"));
		}
		
		if(hashTable.get("EmailSubscriptionsList").equals(true)){
			siteComponents.SelectMultiListByName("Email Subscriptions List", DSP.getProperty("NewsLetterComponentItem"));
		}
		
		if(hashTable.get("EmailSubscriptionsLists").equals(true)){
			for(int i=1;i<=2;i++){
			siteComponents.SelectMultiListByName("Email Subscriptions List", DSP.getProperty("NewsLetterComponentItem")+i);
//			siteComponents.SelectMultiListByName("Email Subscriptions List", DSP.getProperty("NewsLetterComponentItem2"));
			}
		}
		
		if(hashTable.get("OtherEmailSubscriptionsList").equals(true)){
			siteComponents.SelectMultiListByName("Other Email Subscriptions List", DSP.getProperty("NewsLetterComponentItem"));
		}
		
		if(hashTable.get("ItemText").equals(true)){
			siteComponents.typeRTEvalue("Item Text", DSP.getProperty("ItemText"));
		}
		
		if(hashTable.get("Image").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Image", DSP.getProperty("Image"));
		}
		
		if(hashTable.get("TopicImage").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Image", DSP.getProperty("TopicImage"));
		}
		
		if(hashTable.get("PeriodicCycles").equals(true)){
			siteComponents.SelectByIndex("Periodic Cycles", 3);
		}
		
		if(hashTable.get("TargetAudiences").equals(true)){
			siteComponents.SelectMultiList("Target Audiences", 3);
		}
		
		if(hashTable.get("Hint").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Hint", DSP.getProperty("Hint"));
		}
		
		if(hashTable.get("PreviewImage").equals(true)){
			siteComponents.HandleTextBox("TypeTextValue", "Preview Image:", DSP.getProperty("PreviewImage"));
		}
	}
}
