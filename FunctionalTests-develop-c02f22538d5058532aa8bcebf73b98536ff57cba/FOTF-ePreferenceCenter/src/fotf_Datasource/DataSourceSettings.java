package fotf_Datasource;

import java.util.Hashtable;

import fotf_Config.Basedriver;

public class DataSourceSettings extends Basedriver{

	static Hashtable hashTable = new Hashtable();
	private static void SetDefaultValueToHashTable(){
		hashTable.clear();		
		hashTable.put("Title", false);
		hashTable.put("RTETitle", false);
	    hashTable.put("ShortDescription", false);
	    hashTable.put("MyAccountInformationText", false);
	    hashTable.put("EmailSubscriptionsText", false);
	    hashTable.put("UnsubscribeText", false);
	    hashTable.put("VideoItems", false);
	    hashTable.put("OtherEmailSubscriptionsText", false);
	    hashTable.put("SaveButtonText", false);
	    hashTable.put("EmailSubscriptionsList", false);
	    hashTable.put("EmailSubscriptionsLists", false);
	    hashTable.put("OtherEmailSubscriptionsList", false);
	    hashTable.put("ItemText", false);
	    hashTable.put("Image", false);	 
	    hashTable.put("TopicImage", false);
	    hashTable.put("PeriodicCycles", false);
	    hashTable.put("TargetAudiences", false);
	    hashTable.put("Hint", false);
	    hashTable.put("PreviewImage", false);
	} 
	
	public static void EPreferenceDatasource(String renderingDatasource) throws InterruptedException{	    
		SetDefaultValueToHashTable();
		switch(renderingDatasource)
		{			
			case "EPreferenceHeader":
				hashTable.replace("Title", true);
				hashTable.replace("ShortDescription", true);
				break;
			case "EPreferenceMyAccountInfo":
				hashTable.replace("MyAccountInformationText", true);
				break;
			case "EPreferenceENewsLetter":
				hashTable.replace("RTETitle", true);
				hashTable.replace("ItemText", true);
				hashTable.replace("Image", true);
				hashTable.replace("PeriodicCycles", true);
				hashTable.replace("TargetAudiences", true);
				hashTable.replace("Hint", true);
				hashTable.replace("PreviewImage", true);
				break;
			case "EmailSubscriptionsList":
				hashTable.replace("EmailSubscriptionsText", true);
				hashTable.replace("UnsubscribeText", true);
				hashTable.replace("EmailSubscriptionsList", true);
				break;
			case "EmailSubscriptionsLists":
				hashTable.replace("EmailSubscriptionsText", true);
				hashTable.replace("UnsubscribeText", true);
				hashTable.replace("EmailSubscriptionsLists", true);
				break;
			case "OtherEmailSubscription":
				hashTable.replace("RTETitle", true);
				hashTable.replace("ItemText", true);
				hashTable.replace("TopicImage", true);
				break;
			case "OtherEmailSubscriptionList":
				hashTable.replace("OtherEmailSubscriptionsText", true);
				hashTable.replace("SaveButtonText", true);
				hashTable.replace("OtherEmailSubscriptionsList", true);
				break;
				
		}
		dataSource.ComponentDataProvider(hashTable);
		siteUtil.HomeSave();
	}
}
