package fotf_DDF;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.testng.TestNG;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fotf_Config.Trigger;

public class XmlUtil {
	public static void createXml() throws Exception{
		int elementcounter;
		Trigger driver=new Trigger();
		try{
			// initialization of Drivers
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document= documentBuilder.newDocument();

			// Get the number of parameter to be created in XML
			int excelBrowserCount = driver.browserSize-1;
			int flaggedClassCount = driver.testCasesSize-1;

			// Type the suite Tag Element in the XML file
			Element rootElementSuite=document.createElement("suite");
			document.appendChild(rootElementSuite);
			rootElementSuite.setAttribute("name", "SeleniumJavaFramework");
			
			// Type the parameter set of lines in the XML file
			for(int browserCounter=0; browserCounter<= excelBrowserCount; browserCounter++){ 
				// Type the root elements in the XML file
				Element rootElementTest=document.createElement("test");
				Element rootElementParameter=document.createElement("parameter");
				Element rootElementClass=document.createElement("classes");	
				
				// Append values to the root elements
				rootElementSuite.appendChild(rootElementTest);		
			
				rootElementTest.setAttribute("name", driver.browserList[browserCounter]);
				rootElementParameter.setAttribute("name", "browser");
				rootElementParameter.setAttribute("value", driver.browserList[browserCounter]);
							
				rootElementTest.appendChild(rootElementParameter);
				rootElementParameter.appendChild(rootElementClass);	
				
				Element childElementClass;	
				
				for(int elementclasscounter=0; elementclasscounter<= flaggedClassCount; elementclasscounter++){
					childElementClass=document.createElement("class");
					String flagclsElement=driver.testCasesList[elementclasscounter]; 
					childElementClass.setAttribute("name", flagclsElement);
					rootElementClass.appendChild(childElementClass);
				}
		}
			// Generate the file.
			FileWriter fstream = new FileWriter("Switches\\testng.xml");
			BufferedWriter out = new BufferedWriter(fstream);
			
			// Generate the requried XML output file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer= transformerFactory.newTransformer();
			DOMSource source=new DOMSource(document);
			
			// Print all the Generated XML in the File Object
			StreamResult result=new StreamResult(fstream);
			transformer.transform(source, result);
			// Close the Generated file
			out.close();
		}catch(DOMException e){
			e.printStackTrace();
		}
	}
	public static void autoRunXml(){
		// Create a list
		List<String> files = new ArrayList<String>();
		// Add the required xml files to the list
		files.add("Switches\\testng.xml");
		// Create object for TestNG
		TestNG tng=new TestNG();
		// Add the list of the file to create a suite
		tng.setTestSuites(files);
		// Run the Suite
		tng.run();
	}
}