package fotf_Operations;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import fotf_Config.Basedriver;

public class Opr_Sitecore_AboutUs_Assert extends Basedriver{

	static Opr_AboutUs_AssertElement_Article objAssertAboutUsArticle=new Opr_AboutUs_AssertElement_Article();
	
// Verify Elements in Child window for AutoAboutUs Preview page
	public static void AssertAboutus(String imagePosition, String pageMode) throws InterruptedException{
		siteUtil.PreviewItem();
		
		if(!pageMode.equals("PageEditor")){
			Thread.sleep(3000);
			Driver.switchTo().window(siteActions.WindowHandle("CHILD"));
			try{
				if(coreUtil.skipImage.isEnabled()){
					siteActions.clickAnElement(coreUtil.skipImage);	}
			}catch(org.openqa.selenium.NoSuchElementException e){
				System.out.println(e);
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		switch(imagePosition)
		{		
			case "Header":
				objAssertAboutUsArticle.AssertTextTypeElement("//header[@class='eprefs_header'][1]//h1[@class='eprefs_header--title']", "The Actual FOTF EPreference - Header Title");
				objAssertAboutUsArticle.AssertTextTypeElement("//header[@class='eprefs_header'][1]//div[@class='eprefs_header--intro_info']", "The Actual FOTF EPreference - Header Description");
			break;
			
			case "MyAccountInfo":
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_account_info--container']//h2", "The Actual FOTF EPreference - My Account Info Title");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='form-group firstname']", "The Actual FOTF EPreference - My Account Info First Name ");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='form-group lastname']", "The Actual FOTF EPreference - My Account Info Last Name ");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='form-group address']", "The Actual FOTF EPreference - My Account Info Address");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='form-group state']", "The Actual FOTF EPreference - My Account Info State");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='row select-zipcode']", "The Actual FOTF EPreference - My Account Info ZipCode");
				objAssertAboutUsArticle.AssertObjTypeElement("//input[@id='contactemail']", "The Actual FOTF EPreference - My Account Info Contact Mail");
				objAssertAboutUsArticle.AssertObjTypeElement("//input[@id='contactphone']", "The Actual FOTF EPreference - My Account Info Contact Phone");
			break;
			case "ENewsLetter":
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_email_subscription--title default--layout']/h2", "The Actual FOTF EPreference - ENews Letter Title");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_newsitem--periodic_title']", "The Actual FOTF EPreference - ENews Letter Perodic Title");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_newsitem--news_title']/h3", "The Actual FOTF EPreference - ENews Letter Title");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_newsitem--audience']/em", "The Actual FOTF EPreference - ENews Letter Audience");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_newsitem--description']/p", "The Actual FOTF EPreference - ENews Letter Description");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[contains(@class, 'eprefs_newsitem--image')]", "The Actual FOTF EPreference - ENews Letter Image ");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='eprefs_newsitem--button']", "The Actual FOTF EPreference - ENews Letter Check Button ");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_unsubscribe--content']", "The Actual FOTF EPreference - ENews Letter UnSubscribe Label");
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='eprefs_unsubscribe--button']", "The Actual FOTF EPreference - ENews Letter UnSubscribe Button ");
				siteActions.MoveToClickItem("//div[contains(@class,'eprefs_newsitem--image')]", "//span[text()='View Sample']");
				objAssertAboutUsArticle.AssertContainsOf("//div[@id='overlay1']/img", DSP.getProperty("PreviewImage"), "src", "The Actual FOTF EPreference - ENews Letter Sample Image");
				break;
			case "EmailSubscriptions":
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='eprefs_newsitem--container'][2]", "The Actual FOTF EPreference - ENews Email Subsription");
				break;
			case "OtherEmailSubscriptions":
				objAssertAboutUsArticle.AssertObjTypeElement("//div[@class='eprefs_subscription_topic_item--image']/img", "The Actual FOTF EPreference - Image of Other Email Subsription");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_subscription_topic_item--title']/h3", "The Actual FOTF EPreference - Title of Other Email Subsription");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_subscription_topic_item--description']/p", "The Actual FOTF EPreference - Description of Other Email Subsription");
				objAssertAboutUsArticle.AssertTextTypeElement("//div[@class='eprefs_save_prefs_button--container']", "The Actual FOTF EPreference - Save Button of Other Email Subsription");
				break;
			default: break;
		}
		siteActions.CloseChildWindow();
	}
}
