package fotf_Operations;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import fotf_Config.Basedriver;
//import fotf_Datasource.Opr_AboutUsDataSource_DataProvider;

public class Opr_AboutUs_AssertElement_Article extends Basedriver{
	
	//Article Component		
	public static WebElement actualElement;
	public static boolean actualElementState=false;
	// Verify Article Share Bar Url for AboutUs
	public static void AssertTextTypeElement(String elementToVerify, String logMsg){
		
		try{
			actualElement=Driver.findElement(By.xpath(elementToVerify));
			actualElementState=actualElement.isDisplayed();
			Assert.assertEquals(actualElementState, true);
			log.info(logMsg +" - "+ actualElement.getText());
		}catch(AssertionError e){
			log.error(logMsg +" - "+ actualElement.getText());
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is No "+ logMsg +" element available");
		}
	}
	
	public static void AssertObjTypeElement(String elementToVerify, String logMsg){
		try{
			actualElement=Driver.findElement(By.xpath(elementToVerify));
			actualElementState=actualElement.isDisplayed();
			Assert.assertEquals(actualElementState, true);
			log.info(logMsg +" "+ actualElement);
		}catch(AssertionError e){
			log.error(logMsg +" "+ actualElement);
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is No "+ logMsg +" element available");
		}
	}
		
	public static void AssertContainsOf(String elementToVerify, String expectedValue, String attributeName, String logMsg){
		try{
			String actualContent = Driver.findElement(By.xpath(elementToVerify)).getAttribute(attributeName);
			actualElementState=actualContent.contains(expectedValue.toLowerCase());
			Assert.assertEquals(actualElementState, true);
			log.info(logMsg +" "+ actualElement);
		}catch(AssertionError e){
			log.error(logMsg +" "+ actualElement);
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("TEST FAILED: There is No "+ logMsg +" element available");
		}
	}
}
