package fotf_Config;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SitecoreActions extends Basedriver{
	public static WebDriverWait wait;
	public static String GetStringOfWebElement(WebElement xpath) {
		int strLen;
		int startIndex;
		
		String strXpath=xpath.toString();
		//System.out.println(xpath);
		 strLen=xpath.toString().length()-1;
		startIndex=strXpath.indexOf("-> ");
		strXpath=strXpath.substring(startIndex, strLen);
		//System.out.println(strXpath);
		
		strLen=strXpath.toString().length();
		startIndex=strXpath.indexOf(": ")+2;
		strXpath=strXpath.substring(startIndex, strLen);
		//System.out.println(strXpath);
		return strXpath;
}
	public static void clickAnElement(WebElement xpath) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    Driver.findElement(By.xpath(convertXpath)).click();
	    Thread.sleep(1000);
		}
	    catch(Exception e){
	    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());	
	    	//System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
	    }
	}
	
	public static void clickAnElement(String xpath) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).click();
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
   // System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static void clearAnElement(String xpath) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).clear();
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to click the element: "+ e.getLocalizedMessage());
    System.out.println(("Unable to click the element: "+ e.getLocalizedMessage()));
    }
	}
	public static void SendKeys(WebElement xpath, String sendKeys) {
		try{
		String convertXpath=GetStringOfWebElement(xpath);
		 wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(convertXpath)));
	    Driver.findElement(By.xpath(convertXpath)).sendKeys(sendKeys);
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to send keys to the element: "+ e.getLocalizedMessage());	
    //System.out.println(("Unable to send keys to the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static void SendKeys(String xpath, String sendKeys) {
		try{
		wait = new WebDriverWait(Driver, 30);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).sendKeys(sendKeys);
	    Thread.sleep(1000);
	}
    catch(Exception e){
    //log.fatal("Unable to send keys to the element: "+ e.getLocalizedMessage());	
    System.out.println(("Unable to send keys to the element: "+ e.getLocalizedMessage()));
    }
	}
	
	public static WebElement FindAnElement(String xpath) {
		try{
			wait = new WebDriverWait(Driver, 30);
		    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		    }catch(Exception e){
			//log.fatal("Element Not found: " + e.getLocalizedMessage());
			//System.out.println(("Element Not found: "+ e.getLocalizedMessage()));
		}
		return Driver.findElement(By.xpath(xpath));
	}
	
	// Navigate to Child window for AutoAboutUs Preview page
	public static String WindowHandle(String mode){
		Set<String> window=Driver.getWindowHandles();
		Iterator<String> itr = window.iterator();
		String parentName,childName, grandChildName;
		switch(mode){
		case"PARENT":
			return parentName=itr.next();
		
		case"CHILD":
			parentName=itr.next();
			return childName=itr.next();
			
		case"GRANDCHILD":
			parentName=itr.next();
			childName=itr.next();
			return grandChildName=itr.next();
		}
		return "Dummy";
	}	
	
	// Close the Child window and back to the Parent Window
	public static void CloseChildWindow(){
		Driver.switchTo().window(WindowHandle("CHILD")).close();
		Driver.switchTo().window(WindowHandle("PARENT"));
	}
	
	public static void RefreshItem(WebElement webElement) throws InterruptedException{
		//Thread.sleep(2000);			
		Actions ref=new Actions(Driver);
		ref.contextClick(webElement).build().perform();
		siteActions.clickAnElement(coreUtil.refreshItem);
		//Thread.sleep(2000);
	}
	
	public static void DoubleClickItem(WebElement webElement) throws InterruptedException{
		Thread.sleep(2000);			
		Actions ref=new Actions(Driver);
		ref.doubleClick(webElement).build().perform();
		Thread.sleep(2000);
	}
	
	public static void MoveToClickItem(String moveToElement, String clickElement) throws InterruptedException{
		Thread.sleep(2000);			
		Actions ref=new Actions(Driver);
		ref.moveToElement(Driver.findElement(By.xpath(moveToElement))).build().perform();
		siteActions.clickAnElement(moveToElement);
		Thread.sleep(2000);
	}
}