package fotf_Config;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SiteCoreComponents extends Basedriver{
	public static WebDriverWait wait=null;
	public static String GetQuickInfoValue(String quickInfoValue) {
		return Driver.findElement(By.xpath("//td[contains(text(), '"+ quickInfoValue +"')]/following-sibling::td/input")).getAttribute("value");
	}
// Textbox / Link - GetValue, Clear, Send Keys	
	public static String GetTextBoxValue(String TextBoxValue) {
		return Driver.findElement(By.xpath("//div[contains(text(), '"+ TextBoxValue +"')]/following-sibling::div/input")).getAttribute("value");
	}
	public static void TypeTextBox(String textBoxName, String sendKeys) {
		Driver.findElement(By.xpath("//div[contains(text(), '"+ textBoxName +"')]/following-sibling::div/input")).sendKeys(sendKeys);
	}
	public static void ClearTextBox(String TextBoxValue) {
		Driver.findElement(By.xpath("//div[contains(text(), '"+ TextBoxValue +"')]/following-sibling::div/input")).clear();
	}

	public static String HandleTextBox(String operation, String itemSection, String sendKeys){
		String retVal = null;
		switch(operation){
			case "GetTextValue":
				retVal= Driver.findElement(By.xpath("//div[contains(text(), '"+ itemSection +"')]/following-sibling::div/input")).getAttribute("value");
				break;
			case "TypeTextValue":
				Driver.findElement(By.xpath("//div[contains(text(), '"+ itemSection +"')]/following-sibling::div/input")).sendKeys(sendKeys);
				break;
			case "ClearTextValue":
				Driver.findElement(By.xpath("//div[contains(text(), '"+ itemSection +"')]/following-sibling::div/input")).clear();
				break;
		}
		return retVal;
	}
	
	//get text from Rich Text Editor
	  public static String getRTEvalue(String RTEBox) {
		String frameXpath = "//html/body";
	    String xpath = "//div[contains(text(), '"+ RTEBox +"')]/following-sibling::div[1]/a[1]";
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    Driver.findElement(By.xpath(xpath)).click();
	    siteUtil.SwitchToFrames("Editor_contentIframe");
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(frameXpath)));
	    String text = Driver.findElement(By.xpath(frameXpath)).getText();
	    System.out.println(RTEBox + "= " + text);
	    siteUtil.SwitchToFrames("default");
	    siteUtil.SwitchToFrames("scContentIframeId0");
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='CancelButton']")));
	    Driver.findElement(By.xpath("//*[@id='CancelButton']")).click();
	    siteUtil.SwitchToFrames("default");
	    return text;
	  }
	  
	//Type text in Rich Text Editor
	  public static void typeRTEvalue(String RTEBox, String RTEValue) {
		String frameXpath = "//html/body";
	    String xpath = "//div[contains(text(), '"+ RTEBox +"')]/following-sibling::div[1]/a[1]";
	   // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    siteActions.clickAnElement(xpath);
	    siteUtil.SwitchToFrames("scContentIframeId0");
	    siteActions.clickAnElement("//span[text()='HTML']");
	    siteUtil.SwitchToFrames("default");
	    siteUtil.SwitchToFrames("Editor_contentIframe");
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(frameXpath)));
	    siteActions.clearAnElement(frameXpath);
	    siteActions.SendKeys(frameXpath, RTEValue);
		siteUtil.SwitchToFrames("default");
	    siteUtil.SwitchToFrames("scContentIframeId0");
	   // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='CancelButton']")));
	    siteActions.clickAnElement("//*[@id='OkButton']");
	    siteUtil.SwitchToFrames("default");
	  }
	  
	  public static String SelectByIndex(String dropDownBox, int indexValue){
		  WebElement drpListBox=Driver.findElement(By.xpath("//div[contains(text(), '"+dropDownBox+"')]/following-sibling::div/select"));
		  Select list=new Select(drpListBox);
		  list.selectByIndex(indexValue);
		  return drpListBox.findElement(By.xpath("//option["+indexValue+"]")).getText();
	  }
	  
	  public static String SelectByVisibleText(String dropDownBox, String indexText){
		  WebElement drpListBox=Driver.findElement(By.xpath("//div[contains(text(), '"+dropDownBox+"')]/following-sibling::div/select"));
		  Select list=new Select(drpListBox);
		  list.selectByVisibleText(indexText);
		  return drpListBox.findElement(By.xpath("//option[@value='"+indexText+"']")).getText();
	  }
	  
	  public static void SelectMultiList(String multiListName, int listValue) throws InterruptedException{
		  siteActions.clickAnElement("//div[text()='"+multiListName+"']/following-sibling::div//a[contains(text(), 'Deselect all')]");
		  for(int i=1; i<=listValue; i++){
			  WebElement multiListContainer=Driver.findElement(By.xpath("//div[contains(text(),'"+multiListName+"')]/following-sibling::div/div/table/tbody/tr[2]/td[1]/select/option[1]"));
			  siteActions.DoubleClickItem(multiListContainer);
		  }
	  }
	  
	  public static void SelectMultiListByName(String multiListName, String listName) throws InterruptedException{
		  siteActions.SendKeys("//div[contains(text(),'"+multiListName+"')]/following-sibling::div/div/table//input", listName);
		  siteActions.clickAnElement("//div[text()='"+multiListName+"']/following-sibling::div//a[contains(text(), 'Deselect all')]");
		  WebElement multiListContainer=Driver.findElement(By.xpath("//div[contains(text(),'"+multiListName+"')]/following-sibling::div/div/table/tbody/tr[2]/td[1]/select/option[contains(text(),'"+listName+"')]"));
		  siteActions.DoubleClickItem(multiListContainer);
	  }
	  
	  public static void DuplicateItems(int numOfChildItemsToDuplicate){
		  WebElement itemToDuplicate = Driver.findElement(By.xpath("//span[text()='"+DSP.getProperty("NewsLetterComponentItem")+"1']"));
		  for(int i = 2; i <= numOfChildItemsToDuplicate; i++){				
			Actions ref = new Actions(Driver);
			ref.contextClick(itemToDuplicate).build().perform();
			siteActions.clickAnElement(coreUtil.duplicateChildItem);
			siteUtil.SwitchToFrames("scContentIframeId0");
			siteActions.SendKeys(coreUtil.childFolderName,DSP.getProperty("NewsLetterComponentItem") + i);
			siteActions.clickAnElement(coreUtil.okButton);
			Driver.switchTo().defaultContent();
		}	
	  }
}