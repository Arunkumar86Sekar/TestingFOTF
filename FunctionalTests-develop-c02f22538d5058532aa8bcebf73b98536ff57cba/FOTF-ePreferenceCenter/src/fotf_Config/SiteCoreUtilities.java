package fotf_Config;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SiteCoreUtilities extends Basedriver{
	public static String SitecoreUserName()
	{
		return DSP.getProperty("SitecoreUserName");
	}
	
	public static String SitecorePassword()
	{
		return DSP.getProperty("SitecorePassword");
	}
	
	public static String PageComponentContentItemName()
	{
		return DSP.getProperty("ItemName");
	}
	
	// Login to the Sitecore
	public static void loginSitecore(){
		// Verify The Content Editor is Selected or not in Sitecore login page
			coreUtil.userName.clear();
			coreUtil.passWord.clear();
			siteActions.SendKeys(coreUtil.userName,SitecoreUserName());
			siteActions.SendKeys(coreUtil.passWord,SitecorePassword());			
			siteActions.clickAnElement(coreUtil.loginButtion);
			siteActions.clickAnElement(coreUtil.contentEditor);
		}
	
	// Navigate to FOTF content of the Sitecore
	public static void navigateToFOTFcom() throws InterruptedException{
		// Verify The "Content" is Displayed or not under the Sitecore content
		try{
			coreUtil.contentExtract.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			siteActions.clickAnElement(coreUtil.sitecoreExtract);
		}
		
		try{
			coreUtil.fotfCom.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			siteActions.clickAnElement(coreUtil.contentExtract);
		}
		
		try{
			coreUtil.fotfComFirstItem.isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			siteActions.clickAnElement(coreUtil.fotfExtract);
		}

		siteActions.clickAnElement(coreUtil.fotfCom);
		//log.info("Click on FOTF.COM");						
	}
	
	public static void NavigateToItem(String itemPath, String itemName){
		siteActions.SendKeys(coreUtil.treeSearch, itemPath);
		coreUtil.treeSearch.sendKeys(Keys.ENTER);
		Driver.findElement(By.xpath("//div[@id='SearchResult']//a[text()='"+itemName+"']")).click();
		coreUtil.treeSearch.clear();
		siteActions.clickAnElement(coreUtil.closeTreeSearch);
	}
	
	// Creating a new Item from the Template in Sitecore
	public static void CreateItemFromTemplate(String templateName, String itemName) throws InterruptedException{		
		try{
			siteActions.clickAnElement(coreUtil.homeMenuStrip);
			siteActions.clickAnElement(coreUtil.insertFromTemplate);
			siteActions.clickAnElement(coreUtil.insertFromTemplate);
			SwitchToFrames("scContentIframeId0");
		}catch(Exception e){SwitchToFrames("scContentIframeId0");}
		
		
		siteActions.clickAnElement(coreUtil.searchButton);
		SwitchToFrames("default");
		SwitchToFrames("scStretch");
		siteActions.SendKeys(coreUtil.searchInput, templateName);
		siteActions.clickAnElement(coreUtil.searchIcon);
		siteActions.clickAnElement(coreUtil.searchedItem);
		SwitchToFrames("default");
		SwitchToFrames("scContentIframeId0");
		
		/*siteActions.clickAnElement(coreUtil.itemTemplate.replace("*", templateName));*/
		siteActions.clearAnElement(coreUtil.itemInput);
		
		siteActions.SendKeys(coreUtil.itemInput, itemName);
		siteActions.clickAnElement(coreUtil.insertButton);
		SwitchToFrames("default");
		siteActions.clickAnElement(coreUtil.contentTab);
	}
	
	// Switch to Multiple Frames in Sitecore
	public static void SwitchToFrames(String frame){
		Driver.switchTo().defaultContent();
		switch (frame){
		case "scContentIframeId0":
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");
			break;
		case "scContentIframeId1":
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");
			Driver.switchTo().frame("scContentIframeId1");
			break;
		case "scStretch":
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");
			Driver.switchTo().frame(Driver.findElement(By.xpath("//iframe[@class='scStretch']")));
			break;
		case "Editor_contentIframe":
			Driver.switchTo().frame("jqueryModalDialogsFrame");
		    Driver.switchTo().frame("scContentIframeId0");
		    Driver.switchTo().frame("Editor_contentIframe");
			break;
		default :
			Driver.switchTo().defaultContent();
			break;
			
		}
		
	}
	
	// Delete the Sitecore Items 
	public static void DeleteAboutUs(String itemToDelete) throws InterruptedException{
		WebElement elementToDelete=Driver.findElement(By.xpath("//span[contains(text(), '"+itemToDelete+"')]"));
		siteActions.clickAnElement(elementToDelete);
		////Thread.sleep(2000);
		Actions ref=new Actions(Driver);
		ref.contextClick(elementToDelete).build().perform();
		////Thread.sleep(2000);
		siteActions.clickAnElement(coreUtil.deleteItem);
		Thread.sleep(2000);
		SwitchToFrames("scContentIframeId0");	
		siteActions.clickAnElement(coreUtil.okButton);
		SwitchToFrames("default");
		Thread.sleep(2000);
		try{		
			SwitchToFrames("scContentIframeId0");
			//siteActions.clickAnElement(coreUtil.removeLinksForDeleteAboutUsItem);
			siteActions.clickAnElement(coreUtil.deleteWithDatasoureItem);
			siteActions.clickAnElement(coreUtil.deleteWithDatasoureItem);
			SwitchToFrames("default");
			Thread.sleep(3000);
		}catch(Exception e){
			SwitchToFrames("default");
			//log.fatal("TesiteActionsinated Abruptly - No Sub Items to Remove:  " + e.getLocalizedMessage());
		}
	}
		
	// Navigate to Create Layout for AutoAboutUs 
	public static void PreviewItem() throws InterruptedException{ //WebElement itemToPreview
		
		siteActions.clickAnElement(coreUtil.publishItem);
		//log.info("Clicked on Publish Item");
		siteActions.clickAnElement(coreUtil.previewMode);
		//log.info("Preview Mode opened for the AboutUs");
	}
	
	public static void HomeSave() throws InterruptedException{
		siteActions.clickAnElement(coreUtil.homeMenuStrip);
		siteActions.clickAnElement(coreUtil.saveDataSource);
		SaveAnyway();
		Thread.sleep(2000);
	}
	
	public static void SaveAnyway(){
		try{
			SwitchToFrames("scContentIframeId0");
			siteActions.clickAnElement(coreUtil.itemTextAccept);
			Driver.switchTo().defaultContent();
		}catch(org.openqa.selenium.NoSuchElementException e){
			Driver.switchTo().defaultContent();
		}catch(Exception e){
			Driver.switchTo().defaultContent();
		}
	}
	
	public static void HomeFolderTemplates() throws InterruptedException{
		siteActions.clickAnElement(coreUtil.homeMenuStrip);
		siteActions.clickAnElement(coreUtil.createFolder);
	}
	
	// LogOut From the Sitecore
	public static void LogoutSitecore() throws InterruptedException{
		Thread.sleep(2000);
		siteActions.clickAnElement(coreUtil.systemMenu);
		Thread.sleep(2000);
		siteActions.clickAnElement(coreUtil.logOut);
	}	
}