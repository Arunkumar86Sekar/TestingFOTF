package fotf_Testcase;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import fotf_Config.Basedriver;
import fotf_Config.SiteCoreComponents;
import fotf_Config.SiteCoreUtilities;
import fotf_Operations.Opr_Sitecore_AboutUs_Assert;

public class FOTF_EPreference extends Basedriver{

    //SiteCore Login
	@BeforeMethod
	public static void LoginSitecore(){
		Driver.get(baseUrl+"/sitecore");
		siteUtil.loginSitecore();
		siteUtil.NavigateToItem("/sitecore/content/FOTF", "FOTF.com");
	}    
	
	//Testcase for EPreference_Header
	@Test
	public static void EPreference_Header() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: S-02428 - FOTF: EPreference : ePreference Center Header ***");
		log.info("__________________________________________________________________________________________________");
		
		siteUtil.CreateItemFromTemplate("EPreference", DSP.getProperty("EPreferenceItem"));
		setData.EPreferenceDatasource("EPreferenceHeader");
		Opr_Sitecore_AboutUs_Assert.AssertAboutus("Header", "ContentEditor");
		siteUtil.DeleteAboutUs(DSP.getProperty("EPreferenceItem"));
	}	
		
	//Testcase for EPreference_My Accoutn Information
	@Test
	public static void EPreference_MyAccountInfo() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: S-02429 - FOTF: EPreference : ePreference My Account Information ***");
		log.info("__________________________________________________________________________________________________");
		
		siteUtil.CreateItemFromTemplate("EPreference", DSP.getProperty("EPreferenceItem"));
		setData.EPreferenceDatasource("EPreferenceMyAccountInfo");
		Opr_Sitecore_AboutUs_Assert.AssertAboutus("MyAccountInfo", "ContentEditor");
		siteUtil.DeleteAboutUs(DSP.getProperty("EPreferenceItem"));
	}	
		
	//Testcase for EPreference_ENews Letter
	@Test
	public static void EPreference_ENewsLetter() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: S-02430 - FOTF: EPreference : ePreference E News Letter ***");
		log.info("__________________________________________________________________________________________________");
		
		siteUtil.CreateItemFromTemplate("EPreference", DSP.getProperty("EPreferenceItem"));
		siteUtil.CreateItemFromTemplate("NewsLetter Component", DSP.getProperty("NewsLetterComponentItem"));
		setData.EPreferenceDatasource("EPreferenceENewsLetter");
		siteActions.DoubleClickItem(Driver.findElement(By.xpath("//span[contains(text(), '"+DSP.getProperty("EPreferenceItem")+"')]")));
		setData.EPreferenceDatasource("EmailSubscriptionsList");
		
		Opr_Sitecore_AboutUs_Assert.AssertAboutus("ENewsLetter", "ContentEditor");
		siteUtil.DeleteAboutUs(DSP.getProperty("EPreferenceItem"));
	}	
		
	//Testcase for EPreference_ENews Letter
	@Test
	public static void EPreference_TwoENewsLetter() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: S-02431 - FOTF: EPreference : ePreference Email Subscriptions ***");
		log.info("__________________________________________________________________________________________________");
		
		siteUtil.CreateItemFromTemplate("EPreference", DSP.getProperty("EPreferenceItem"));
		siteUtil.CreateItemFromTemplate("NewsLetter Component", DSP.getProperty("NewsLetterComponentItem")+1);
		setData.EPreferenceDatasource("EPreferenceENewsLetter");
		siteComponents.DuplicateItems(2);
		siteActions.DoubleClickItem(Driver.findElement(By.xpath("//span[contains(text(), '"+DSP.getProperty("EPreferenceItem")+"')]")));
		setData.EPreferenceDatasource("EmailSubscriptionsLists");
		Opr_Sitecore_AboutUs_Assert.AssertAboutus("EmailSubscriptions", "ContentEditor");
		siteUtil.DeleteAboutUs(DSP.getProperty("EPreferenceItem"));
	}	
	
	//Testcase for EPreference_ENews Letter
	@Test
	public static void EPreference_OtherEmailSubscription() throws InterruptedException{
		log.info("");
		log.info("__________________________________________________________________________________________________");
		log.info("*** TestCase - PBI: S-02430 - FOTF: EPreference : ePreference Other Email Subscription  ***");
		log.info("__________________________________________________________________________________________________");
		
		siteUtil.CreateItemFromTemplate("EPreference", DSP.getProperty("EPreferenceItem"));
		siteUtil.CreateItemFromTemplate("NewsLetter Component", DSP.getProperty("NewsLetterComponentItem"));
		setData.EPreferenceDatasource("OtherEmailSubscription");
		siteActions.DoubleClickItem(Driver.findElement(By.xpath("//span[contains(text(), '"+DSP.getProperty("EPreferenceItem")+"')]")));
		setData.EPreferenceDatasource("OtherEmailSubscriptionList");
		
		Opr_Sitecore_AboutUs_Assert.AssertAboutus("OtherEmailSubscriptions", "ContentEditor");
		siteUtil.DeleteAboutUs(DSP.getProperty("EPreferenceItem"));
	}	
		
	// Logout Sitecore
	@AfterMethod
	public static void LogoutSitecore() throws InterruptedException{
		Thread.sleep(2000);
		siteActions.clickAnElement(coreUtil.systemMenu);
		Thread.sleep(2000);
		siteActions.clickAnElement(coreUtil.logOut);
	}	
}
